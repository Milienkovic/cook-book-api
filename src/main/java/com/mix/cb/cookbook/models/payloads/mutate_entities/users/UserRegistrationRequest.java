package com.mix.cb.cookbook.models.payloads.mutate_entities.users;

import com.mix.cb.cookbook.services.validation.ValidEmail;
import com.mix.cb.cookbook.services.validation.FieldValuesMatch;
import com.mix.cb.cookbook.services.validation.ValidPassword;
import lombok.Data;

@Data
@FieldValuesMatch(field = "password", fieldMatch = "confirmPassword",message = "{passwords.dontMatch}")
public class UserRegistrationRequest {
    @ValidEmail
    private String email;
    @ValidPassword
    private String password;
    @ValidPassword
    private String confirmPassword;
}
