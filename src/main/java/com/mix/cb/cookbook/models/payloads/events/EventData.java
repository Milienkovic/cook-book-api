package com.mix.cb.cookbook.models.payloads.events;

import com.mix.cb.cookbook.services.validation.ValidUrl;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Locale;

@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class EventData {
    private Locale locale;
    @ValidUrl(message = "{url.invalid}")
    private String appUrl;
}
