package com.mix.cb.cookbook.models.payloads.mutate_entities.steps;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class MutateStep {
    @NotEmpty(message = "{field.notEmpty}")
    private String stepNo;
    @NotEmpty(message = "{field.notEmpty}")
    private String description;
    private boolean mandatory;
}
