package com.mix.cb.cookbook.models.payloads.mutate_entities.comments;

import com.mix.cb.cookbook.models.enums.ReportReason;
import lombok.Data;

@Data
public class ReportCommentRequest {
    private ReportReason reason;
}
