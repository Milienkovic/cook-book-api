package com.mix.cb.cookbook.models.enums;

public enum Complexity {
    HARD(5), MEDIUM(3), EASY(1), EASY_MEDIUM(2), MEDIUM_HARD(4), VERY_HARD(6), VERY_EASY(0);

    private int value;

    Complexity(int value) {
        this.value = value;
    }
}
