package com.mix.cb.cookbook.models.payloads.mutate_entities.users.address.location;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.address.AddressDTO;
import com.mix.cb.cookbook.services.validation.ValidLocation;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ValidLocation
public class LocationDTO {
    int id;
    private double lat;
    private double lng;
    @JsonBackReference
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private AddressDTO address;
}
