package com.mix.cb.cookbook.models.payloads.mutate_entities.users;

import com.mix.cb.cookbook.models.payloads.mutate_entities.users.address.MutateAddress;
import com.mix.cb.cookbook.services.validation.ValidEmail;
import com.mix.cb.cookbook.services.validation.ValidPersonName;
import com.mix.cb.cookbook.services.validation.ValidPhone;
import lombok.Data;

@Data
public class MutateUser {
    @ValidPersonName(message = "{firstName.invalid}")
    private String firstName;
    @ValidPersonName(message = "{lastName.invalid}")
    private String lastName;
    @ValidEmail
    private String email;
    @ValidPhone
    private String phone;
    private MutateAddress address;
}
