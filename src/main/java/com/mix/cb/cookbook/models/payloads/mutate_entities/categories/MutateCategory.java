package com.mix.cb.cookbook.models.payloads.mutate_entities.categories;

import com.mix.cb.cookbook.services.validation.ValidDescription;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;

@Data
public class MutateCategory {
    @NotEmpty(message = "{field.notEmpty}")
    private String name;
    @ValidDescription
    private String description;
    private String imageUrl;
    private MultipartFile image;
}
