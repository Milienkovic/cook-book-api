package com.mix.cb.cookbook.models.enums;

public enum ReportReason {
    INSULTING_COMMENT("insulting comment"),
    SUBJECT_NON_RELATED_COMMENT("not related to the subject");


    private String reason;

    ReportReason(String reason) {
        this.reason = reason;
    }

    public String reason() {
        return this.reason;
    }
}
