package com.mix.cb.cookbook.models.payloads.events;

import com.mix.cb.cookbook.models.payloads.mutate_entities.users.CurrentUserPayload;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Locale;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class PasswordResetEventData extends EventData {
    private CurrentUserPayload user;

    public PasswordResetEventData(Locale locale, String appUrl, CurrentUserPayload user) {
        super(locale, appUrl);
        this.user = user;
    }
}
