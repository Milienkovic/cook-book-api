package com.mix.cb.cookbook.models.payloads.mutate_entities.recipes;

import com.mix.cb.cookbook.models.enums.Complexity;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class MutateRecipe {
    @NotEmpty(message = "{field.notEmpty}")
    private String name;
    private Complexity complexity;
    private String imageUrl;
    private boolean visible;

}
