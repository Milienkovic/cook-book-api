package com.mix.cb.cookbook.models.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Ban {
    @Id
    private int id;
    @Column
    private int nob;
    @Column
    private boolean perma;
    @Column
    private String IP;
    @Column
    private Timestamp bannedUntil;
    @Column
    @CreatedDate
    private Timestamp createdAt;
    @Column
    @LastModifiedDate
    private Timestamp lastModified;
    @OneToMany(mappedBy = "ban",cascade = CascadeType.ALL)
    private List<Reason> reasons;
    @MapsId
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToOne(targetEntity = User.class)
    private User user;

    public boolean isBanned() {
        return perma || (bannedUntil != null && bannedUntil.after(new Date()));
    }
}
