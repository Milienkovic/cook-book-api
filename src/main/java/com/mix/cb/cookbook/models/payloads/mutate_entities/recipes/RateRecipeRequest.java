package com.mix.cb.cookbook.models.payloads.mutate_entities.recipes;

import com.mix.cb.cookbook.services.validation.ValidRating;
import lombok.Data;

@Data
public class RateRecipeRequest {
    @ValidRating
    private int rated;
}
