package com.mix.cb.cookbook.models.entities;

import com.mix.cb.cookbook.models.enums.MeasurementUnit;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

@Data
@Entity
public class Ingredient {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column
    private String name;
    @Column
    double amount;
    @Enumerated
    @Column(name = "uom")
    private MeasurementUnit unitOfMeasurement;
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JoinColumn(name = "recipe_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Recipe recipe;
}
