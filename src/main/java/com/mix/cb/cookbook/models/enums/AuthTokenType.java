package com.mix.cb.cookbook.models.enums;

public enum AuthTokenType {
    BASIC("Basic "), DIGEST("Digest "), BEARER("Bearer ");
    private String value;

    AuthTokenType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
