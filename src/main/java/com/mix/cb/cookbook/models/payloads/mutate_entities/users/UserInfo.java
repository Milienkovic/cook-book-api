package com.mix.cb.cookbook.models.payloads.mutate_entities.users;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.address.AddressDTO;
import com.mix.cb.cookbook.services.validation.ValidEmail;
import com.mix.cb.cookbook.services.validation.ValidPersonName;
import com.mix.cb.cookbook.services.validation.ValidPhone;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.Past;
import java.sql.Timestamp;

@Data
public class UserInfo {
    private int id;
    @ValidPersonName(message = "{firstName.invalid}")
    private String firstName;
    @ValidPersonName(message = "{lastName.invalid}")
    private String lastName;
    @ValidEmail
    private String email;
    @ValidPhone
    private String phone;

    @JsonManagedReference
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private AddressDTO address;
    @Past
    private Timestamp createdAt;
    @Past
    private Timestamp lastModified;
    @Past
    private Timestamp lastVisited;
    @Past
    private Timestamp validatedAt;
}
