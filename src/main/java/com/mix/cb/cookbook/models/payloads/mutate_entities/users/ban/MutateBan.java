package com.mix.cb.cookbook.models.payloads.mutate_entities.users.ban;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class MutateBan {
    private int nob;
    private boolean perma;
    @NotEmpty(message = "{field.notEmpty}")
    private String reason;
    private String IP;
}
