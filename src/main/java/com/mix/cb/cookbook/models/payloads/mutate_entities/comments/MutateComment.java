package com.mix.cb.cookbook.models.payloads.mutate_entities.comments;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class MutateComment {
    @NotEmpty(message = "{field.notEmpty}")
    private String text;
    private boolean reported;
}
