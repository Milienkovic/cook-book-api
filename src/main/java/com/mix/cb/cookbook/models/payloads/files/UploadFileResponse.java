package com.mix.cb.cookbook.models.payloads.files;

import com.mix.cb.cookbook.services.validation.ValidUrl;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
@Builder
public class UploadFileResponse {
    @NotEmpty(message = "{field.notEmpty}")
    private String fileName;
    @ValidUrl
    private String downloadUrl;
    private long fileSize;
    private String fileType;
}
