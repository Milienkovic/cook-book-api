package com.mix.cb.cookbook.models.payloads.mutate_entities.users.ban;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.mix.cb.cookbook.models.entities.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Past;
import java.sql.Timestamp;
@Data
public class BanDTO {
    private int id;
    private int nob;
    private boolean perma;
    @NotEmpty(message = "{field.notEmpty}")
    private String reason;
    private String IP;
    private Timestamp bannedUntil;
    @Past
    private Timestamp createdAt;
    @Past
    private Timestamp lastModified;
    @JsonBackReference
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private User user;
}
