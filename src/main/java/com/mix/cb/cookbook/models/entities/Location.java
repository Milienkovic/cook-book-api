package com.mix.cb.cookbook.models.entities;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

@Data
@Entity
public class Location {
    @Id
    int id;
    @Column
    private double lat;
    @Column
    private double lng;

    @MapsId
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JoinColumn(name = "address_id")
    @OneToOne(cascade = {CascadeType.MERGE})
    private Address address;



}
