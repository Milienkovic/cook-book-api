package com.mix.cb.cookbook.models.payloads.files;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.core.io.Resource;

@Getter
@EqualsAndHashCode(callSuper = true)
public class UploadPdfFileResponse extends UploadFileResponse {
    private Resource resource;


    public UploadPdfFileResponse(String fileName, String downloadUrl, long fileSize, String fileType, Resource resource) {
        super(fileName, downloadUrl, fileSize, fileType);
        this.resource = resource;
    }
}
