package com.mix.cb.cookbook.models.payloads.mutate_entities.comments;

import lombok.Data;

@Data
public class LikeCommentRequest {
    private boolean like;
}
