package com.mix.cb.cookbook.models.payloads.mutate_entities.users.address;

import com.mix.cb.cookbook.models.payloads.mutate_entities.users.address.location.MutateLocation;
import com.mix.cb.cookbook.services.validation.ValidAddress;
import lombok.Data;

@Data
@ValidAddress
public class MutateAddress {
    private String street;
    private String city;
    private String country;
    private MutateLocation location;
}
