package com.mix.cb.cookbook.models.payloads.auth.data;

import com.mix.cb.cookbook.services.validation.ValidEmail;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.servlet.http.Cookie;

@Data
@AllArgsConstructor
public class AuthToken {
    @ValidEmail(message = "{email.invalid}")
    private String email;
    private String authToken;
    private Cookie refreshTokenCookie;

}
