package com.mix.cb.cookbook.models.enums;

public enum BanReason {
    TOO_MANY_FAILED_ATTEMPTS("Too many failed login attempts"),
    INSULTING_COMMENT("Posting insulting comments");

    private String value;

    BanReason(String value) {
        this.value = value;
    }

    public String reason(){
        return value;
    }
}
