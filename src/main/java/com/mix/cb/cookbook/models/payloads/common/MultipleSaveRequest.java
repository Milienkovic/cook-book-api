package com.mix.cb.cookbook.models.payloads.common;

import com.mix.cb.cookbook.services.validation.CollectionNotEmpty;
import lombok.Data;

import java.util.Collection;

@Data
public class MultipleSaveRequest<T> {
    @CollectionNotEmpty
    private Collection<T> items;
}
