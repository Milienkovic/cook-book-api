package com.mix.cb.cookbook.models.payloads.mutate_entities.comments;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.mix.cb.cookbook.models.payloads.mutate_entities.recipes.RecipeDTO;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.UserDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Past;
import java.sql.Timestamp;
import java.util.LinkedHashSet;
import java.util.Set;

@Data
public class CommentDTO {
    private int id;
    @NotEmpty(message = "{field.notEmpty}")
    private String text;
    private int likes;
    private int dislikes;
    private boolean reported;
    @Past
    private Timestamp createdAt;
    @Past
    private Timestamp lastModified;

    private String reason;

    @JsonBackReference
    private CommentDTO comment;

    @JsonManagedReference
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Set<CommentDTO> replies = new LinkedHashSet<>();

    @JsonBackReference
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private UserDTO user;

    @JsonBackReference
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private RecipeDTO recipe;
}
