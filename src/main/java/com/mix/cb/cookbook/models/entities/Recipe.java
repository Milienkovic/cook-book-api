package com.mix.cb.cookbook.models.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.mix.cb.cookbook.models.enums.Complexity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Data
@EntityListeners(AuditingEntityListener.class)
public class Recipe {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(unique = true)
    private String name;
    @Enumerated
    private Complexity complexity;

    @ToString.Exclude
    @JsonBackReference
    @EqualsAndHashCode.Exclude
    @JoinColumn(name = "categoryId")
    @ManyToOne
    private Category category;

    @Column(name = "image_url")
    private String imageUrl;

    @Column
    private double rating;

    @Column
    private int votes;

    @Column
    private boolean visible;
    @EqualsAndHashCode.Exclude
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "recipe", cascade = {CascadeType.ALL})
    private Set<Ingredient> ingredients;

    @CreatedDate
    @Column(updatable = false, name = "created_at")
    private Timestamp createdAt;

    @LastModifiedDate
    @Column(name = "last_modified")
    private Timestamp lastModified;

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "recipe", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    private Set<Step> steps = new LinkedHashSet<>();

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "recipe", cascade = CascadeType.ALL)
    private Set<Comment> comments = new LinkedHashSet<>();

    @ManyToMany(mappedBy = "favorites")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Set<User> usersInFavor;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private User user;

    public void addUser(User user){
        this.usersInFavor.add(user);
        user.getFavorites().add(this);
    }

    public void removeUser(User user){
        this.usersInFavor.remove(user);
        user.getFavorites().remove(this);
    }
}
