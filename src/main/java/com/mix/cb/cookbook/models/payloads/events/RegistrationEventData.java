package com.mix.cb.cookbook.models.payloads.events;

import com.mix.cb.cookbook.models.payloads.mutate_entities.users.CurrentUserPayload;
import lombok.*;

import java.util.Locale;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class RegistrationEventData extends EventData {
    private CurrentUserPayload user;

    public RegistrationEventData(CurrentUserPayload user, String appUrl, Locale locale) {
        super(locale, appUrl);
        this.user = user;
    }
}
