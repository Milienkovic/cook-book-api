package com.mix.cb.cookbook.models.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private int likes;

    @Column
    private int dislikes;

    @Column
    private boolean reported;

    @Column
    private String text;

    @Column
    @CreatedDate
    private Timestamp createdAt;

    @Column
    @LastModifiedDate
    private Timestamp lastModified;

    @Column
    private String reason;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private User user;

    @ManyToOne
    @JoinColumn(name = "recipe_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Recipe recipe;


    @JoinColumn(name = "comment_id")
    @ManyToOne(fetch = FetchType.EAGER)

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Comment comment;

    @OneToMany(mappedBy = "comment",cascade = CascadeType.ALL)
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Set<Comment> replies;
}
