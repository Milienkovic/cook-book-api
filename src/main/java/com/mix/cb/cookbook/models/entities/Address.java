package com.mix.cb.cookbook.models.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

@Data
@Entity
public class Address {
    @Id
    private int id;
    @Column
    private String street;
    @Column
    private String city;
    @Column
    private String country;

    @EqualsAndHashCode.Exclude
    @OneToOne(mappedBy = "address", fetch = FetchType.LAZY, cascade = CascadeType.ALL,targetEntity = Location.class)
    private Location location;

    @MapsId
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JoinColumn(name = "user_Id")
    @OneToOne(cascade = CascadeType.MERGE)
    private User user;
}
