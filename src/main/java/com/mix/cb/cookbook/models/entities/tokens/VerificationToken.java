package com.mix.cb.cookbook.models.entities.tokens;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class VerificationToken extends Token{

}
