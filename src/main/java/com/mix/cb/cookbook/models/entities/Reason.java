package com.mix.cb.cookbook.models.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Reason {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column
    private String reason;
    @Column
    @CreatedDate
    private Timestamp createdAt;
    @Column
    private int banTimeInMinutes;
    @ManyToOne
    @JoinColumn(name = "ban_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    Ban ban;
}
