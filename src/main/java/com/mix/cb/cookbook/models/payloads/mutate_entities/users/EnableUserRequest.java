package com.mix.cb.cookbook.models.payloads.mutate_entities.users;

import lombok.Data;

@Data
public class EnableUserRequest {
    private boolean enabled;
}
