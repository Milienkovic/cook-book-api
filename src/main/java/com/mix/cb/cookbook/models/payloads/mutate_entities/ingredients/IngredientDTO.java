package com.mix.cb.cookbook.models.payloads.mutate_entities.ingredients;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.mix.cb.cookbook.models.enums.MeasurementUnit;
import com.mix.cb.cookbook.models.payloads.mutate_entities.recipes.RecipeDTO;
import com.mix.cb.cookbook.services.validation.ValidAmount;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;

@Data
public class IngredientDTO {
    private int id;
    @NotEmpty(message = "{field.notEmpty}")
    private String name;
    @ValidAmount
    double amount;
    private MeasurementUnit unitOfMeasurement;
    @JsonBackReference
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private RecipeDTO recipe;
}
