package com.mix.cb.cookbook.models.payloads.events;

import lombok.*;

import java.util.Locale;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class NewVerificationTokenEventData extends EventData {
    private String email;

    public NewVerificationTokenEventData(Locale locale, String appUrl, String email) {
        super(locale, appUrl);
        this.email = email;
    }
}