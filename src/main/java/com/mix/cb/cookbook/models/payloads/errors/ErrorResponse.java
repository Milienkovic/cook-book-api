package com.mix.cb.cookbook.models.payloads.errors;

import lombok.Data;

import java.sql.Timestamp;
import java.util.Map;

@Data
public class ErrorResponse {
    private Map<?, ?> errors;
    private String message;
    private Timestamp timestamp;
    private String error;
    private int status;
    private String path;
}
