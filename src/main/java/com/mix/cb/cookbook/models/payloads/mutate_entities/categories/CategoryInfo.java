package com.mix.cb.cookbook.models.payloads.mutate_entities.categories;

import com.mix.cb.cookbook.services.validation.ValidDescription;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Past;
import java.sql.Timestamp;

@Data
public class CategoryInfo {
    private int id;
    @NotEmpty(message = "{field.notEmpty}")
    private String name;
    private String imageUrl;
    @ValidDescription
    private String description;
    @Past
    private Timestamp createdAt;
    @Past
    private Timestamp lastModified;

    private int recipesCount;

}
