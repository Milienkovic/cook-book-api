package com.mix.cb.cookbook.models.enums;

public enum Coordinates {
    LATITUDE("latitude"), LONGITUDE("longitude");

    private String value;

    Coordinates(String value) {
        this.value = value;
    }
}
