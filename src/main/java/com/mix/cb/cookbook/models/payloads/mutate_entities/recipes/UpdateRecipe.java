package com.mix.cb.cookbook.models.payloads.mutate_entities.recipes;

import com.mix.cb.cookbook.models.enums.Complexity;
import com.mix.cb.cookbook.models.payloads.mutate_entities.ingredients.UpdateIngredient;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import java.util.Set;

@Data
public class UpdateRecipe {
    @NotEmpty(message = "{field.notEmpty}")
    private String name;
    private Complexity complexity;
    private String imageUrl;
    boolean visible;
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Set<UpdateIngredient> ingredients;
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Set<UpdateIngredient> steps;
}
