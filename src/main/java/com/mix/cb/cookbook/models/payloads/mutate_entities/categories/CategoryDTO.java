package com.mix.cb.cookbook.models.payloads.mutate_entities.categories;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.mix.cb.cookbook.models.payloads.mutate_entities.recipes.RecipeDTO;
import com.mix.cb.cookbook.services.validation.ValidDescription;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Past;
import java.sql.Timestamp;
import java.util.List;

@Data
public class CategoryDTO {
    private int id;
    @NotEmpty(message = "{field.notEmpty}")
    private String name;
    private String imageUrl;
    @Past
    private Timestamp createdAt;
    @Past
    private Timestamp lastModified;
    @ValidDescription
    private String description;
    @JsonManagedReference
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<RecipeDTO> recipes;



}
