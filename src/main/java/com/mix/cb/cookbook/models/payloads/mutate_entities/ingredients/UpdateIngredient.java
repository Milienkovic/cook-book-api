package com.mix.cb.cookbook.models.payloads.mutate_entities.ingredients;

import com.mix.cb.cookbook.models.enums.MeasurementUnit;
import com.mix.cb.cookbook.services.validation.ValidAmount;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class UpdateIngredient {
    private int id;
    @NotEmpty(message = "{field.notEmpty}")
    private String name;
    @ValidAmount
    private double amount;
    private MeasurementUnit unitOfMeasurement;
}
