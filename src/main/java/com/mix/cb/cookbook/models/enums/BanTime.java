package com.mix.cb.cookbook.models.enums;

public enum BanTime {
    HALF_HOUR(30), HOUR(60),DAY(1440), PERMANENT(Integer.MAX_VALUE), WEEK(10080);

    private int value;

    BanTime(int value) {
        this.value = value;
    }
     public int time(){
        return value;
     }
}
