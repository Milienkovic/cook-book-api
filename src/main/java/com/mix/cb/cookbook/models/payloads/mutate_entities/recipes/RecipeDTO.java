package com.mix.cb.cookbook.models.payloads.mutate_entities.recipes;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.mix.cb.cookbook.models.entities.Category;
import com.mix.cb.cookbook.models.entities.User;
import com.mix.cb.cookbook.models.enums.Complexity;
import com.mix.cb.cookbook.models.payloads.mutate_entities.comments.CommentDTO;
import com.mix.cb.cookbook.models.payloads.mutate_entities.ingredients.IngredientDTO;
import com.mix.cb.cookbook.models.payloads.mutate_entities.steps.StepDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Past;
import java.sql.Timestamp;
import java.util.LinkedHashSet;
import java.util.Set;

@Data
public class RecipeDTO {
    private int id;

    @NotEmpty(message = "{field.notEmpty}")
    private String name;

    private Complexity complexity;

    private String imageUrl;

    private int votes;

    private double rating;

    private boolean visible;

    @Past
    private Timestamp createdAt;

    @Past
    private Timestamp lastModified;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonManagedReference
    private Set<IngredientDTO> ingredients = new LinkedHashSet<>();

    @JsonBackReference
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Category category;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonManagedReference
    private Set<StepDTO> steps = new LinkedHashSet<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonManagedReference
    private Set<CommentDTO> comments = new LinkedHashSet<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonBackReference
    private Set<User> usersInFavor;
}
