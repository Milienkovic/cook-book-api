package com.mix.cb.cookbook.models.payloads.common;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ResponseMessage {
    private String message;
    private boolean isSuccess;
}
