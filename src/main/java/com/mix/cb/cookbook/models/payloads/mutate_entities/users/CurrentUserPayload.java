package com.mix.cb.cookbook.models.payloads.mutate_entities.users;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.mix.cb.cookbook.models.entities.Role;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.address.AddressDTO;
import com.mix.cb.cookbook.services.validation.ValidEmail;
import com.mix.cb.cookbook.services.validation.ValidPersonName;
import com.mix.cb.cookbook.services.validation.ValidPhone;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.Past;
import java.sql.Timestamp;

@Data
public class CurrentUserPayload {

    private int id;

    @ValidPersonName(message = "{firstName.invalid}")
    private String firstName;
    @ValidPersonName(message = "{lastName.invalid}")
    private String lastName;
    @ValidEmail
    private String email;
    @ValidPhone
    private String phone;
    @Past
    private Timestamp createdAt;
    @Past
    private Timestamp lastModified;
    @Past
    private Timestamp lastVisited;
    @Past
    private Timestamp validatedAt;
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonManagedReference
    private AddressDTO address;

    private Role role;
}
