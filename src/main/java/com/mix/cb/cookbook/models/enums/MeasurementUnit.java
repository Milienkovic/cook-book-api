package com.mix.cb.cookbook.models.enums;

public enum MeasurementUnit {
    GRAM("gr"), KILOGRAM("kg"), UNIT("unit"), POUND("lb"), OUNCE("oz"),
    LITER("l"), DECILITER("dl"), MILLILITER("ml"), PINT("pt"), GALLON("gal"),
    SMALL_SPOON("small spoon"), LARGE_SPOON("large spoon"), GLASS("glass");

    private String value;

    MeasurementUnit(String value) {
        this.value = value;
    }
}
