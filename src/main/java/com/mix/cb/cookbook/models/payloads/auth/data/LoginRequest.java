package com.mix.cb.cookbook.models.payloads.auth.data;

import com.mix.cb.cookbook.services.validation.ValidEmail;
import com.mix.cb.cookbook.services.validation.ValidPassword;
import lombok.Data;

@Data
public class LoginRequest {
    @ValidEmail(message = "{email.invalid}")
    private String email;
    @ValidPassword(message = "{password.invalid}")
    private String password;
}