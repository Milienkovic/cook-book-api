package com.mix.cb.cookbook.models.payloads.mutate_entities.recipes;

import com.mix.cb.cookbook.models.enums.Complexity;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Past;
import java.sql.Timestamp;

@Data
public class RecipeInfo {
    private int id;

    @NotEmpty(message = "{field.notEmpty}")
    private String name;

    private Complexity complexity;

    private String imageUrl;

    private int votes;

    private double rating;

    @Past
    private Timestamp createdAt;

    @Past
    private Timestamp lastModified;

    private Boolean visible;
}
