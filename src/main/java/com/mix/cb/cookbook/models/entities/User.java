package com.mix.cb.cookbook.models.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

@Data
@Entity
@EntityListeners({AuditingEntityListener.class})
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;
    @Column
    private String firstName;
    @Column
    private String lastName;

    @Column(unique = true)
    private String email;

    @Column
    private boolean enabled;

    @Column
    @JsonIgnore
    private String password;

    @CreatedDate
    @Column(updatable = false, name = "created_at")
    private Timestamp createdAt;

    @Column(name = "validated_at")
    private Timestamp validatedAt;

    @LastModifiedDate
    @Column(name = "last_modified")
    @EqualsAndHashCode.Exclude
    private Timestamp lastModified;

    @Column(name = "last_visited")
    @EqualsAndHashCode.Exclude
    private Timestamp lastVisited;
    @Column
    private String phone;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "user")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Address address;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "role_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Role role;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "user",cascade = CascadeType.ALL)
    private List<Recipe> recipes;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "user", cascade = {CascadeType.MERGE,CascadeType.DETACH})
    private Set<Comment> comments;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToOne(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Ban ban;

    @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinTable(name = "favorite",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "recipe_id"))
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Set<Recipe> favorites;

    public void addFavoriteRecipe(Recipe recipe) {
        this.favorites.add(recipe);
        recipe.getUsersInFavor().add(this);
    }

    public void removeFromFavorites(Recipe recipe) {
        this.favorites.remove(recipe);
        recipe.getUsersInFavor().remove(this);
    }
}
