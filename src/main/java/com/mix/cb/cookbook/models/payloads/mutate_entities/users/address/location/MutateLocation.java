package com.mix.cb.cookbook.models.payloads.mutate_entities.users.address.location;

import com.mix.cb.cookbook.services.validation.ValidLocation;
import lombok.Data;

@Data
@ValidLocation
public class MutateLocation {
    private double lat;
    private double lng;
}
