package com.mix.cb.cookbook.models.payloads.mutate_entities.steps;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.mix.cb.cookbook.models.payloads.mutate_entities.recipes.RecipeDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Past;
import java.sql.Timestamp;

@Data
public class StepDTO {
    private int id;
    @NotEmpty(message = "{field.notEmpty}")
    private String stepNo;
    @NotEmpty(message = "{field.notEmpty}")
    private String description;
    @Past
    private Timestamp createdAt;
    @Past
    private Timestamp lastModified;
    private boolean mandatory;
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonBackReference
    private RecipeDTO recipe;
}
