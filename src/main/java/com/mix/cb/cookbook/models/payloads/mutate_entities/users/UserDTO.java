package com.mix.cb.cookbook.models.payloads.mutate_entities.users;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.mix.cb.cookbook.models.entities.Recipe;
import com.mix.cb.cookbook.models.entities.Role;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.address.AddressDTO;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.ban.BanDTO;
import com.mix.cb.cookbook.services.validation.ValidEmail;
import com.mix.cb.cookbook.services.validation.ValidPersonName;
import com.mix.cb.cookbook.services.validation.ValidPhone;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.Past;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

@Data
public class UserDTO {
    private int id;
    @ValidPersonName(message = "{firstName.invalid}")
    private String firstName;
    @ValidPersonName(message = "{lastName.invalid}")
    private String lastName;
    @ValidEmail
    private String email;
    private boolean enabled;
    @Past
    private Timestamp createdAt;
    @Past
    private Timestamp lastModified;
    @Past
    private Timestamp lastVisited;
    @Past
    private Timestamp validatedAt;

    private Role role;
    @ValidPhone
    private String phone;

    @ToString.Exclude
    @JsonManagedReference
    @EqualsAndHashCode.Exclude
    private AddressDTO address;

    @ToString.Exclude
    @JsonManagedReference
    @EqualsAndHashCode.Exclude
    private List<Recipe> recipes;

    @JsonManagedReference
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private BanDTO ban;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonManagedReference
    private Set<Recipe> favorites;
}
