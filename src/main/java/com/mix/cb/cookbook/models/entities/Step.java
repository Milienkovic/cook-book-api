package com.mix.cb.cookbook.models.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Step {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "step_no")
    private String stepNo;
    @Column
    private String description;
    @CreatedDate
    @Column(name = "created_at",updatable = false)
    private Timestamp createdAt;
    @LastModifiedDate
    @Column(name = "last_modified")
    private Timestamp lastModified;
    @Column
    private boolean mandatory;
    @ManyToOne
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JoinColumn(name = "recipe_id")
    private Recipe recipe;
}
