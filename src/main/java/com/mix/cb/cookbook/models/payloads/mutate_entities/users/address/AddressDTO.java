package com.mix.cb.cookbook.models.payloads.mutate_entities.users.address;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.address.location.LocationDTO;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.UserDTO;
import com.mix.cb.cookbook.services.validation.ValidAddress;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ValidAddress
public class AddressDTO {
    private int id;
    private String street;
    private String city;
    private String country;
    @JsonManagedReference
    @EqualsAndHashCode.Exclude
    private LocationDTO location;
    @JsonBackReference
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private UserDTO user;
}
