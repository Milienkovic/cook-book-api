package com.mix.cb.cookbook.models.payloads.mutate_entities.users;

import com.mix.cb.cookbook.services.validation.ValidEmail;
import lombok.Data;

@Data
public class UserEmail {
    @ValidEmail
    private String email;
}
