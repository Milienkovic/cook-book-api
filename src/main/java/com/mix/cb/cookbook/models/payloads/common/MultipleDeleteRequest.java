package com.mix.cb.cookbook.models.payloads.common;

import com.mix.cb.cookbook.services.validation.ArrayNotEmpty;
import lombok.Data;

@Data
public class MultipleDeleteRequest {
    @ArrayNotEmpty
    private int[] indices;
}
