package com.mix.cb.cookbook.utils;

import com.mix.cb.cookbook.models.payloads.mutate_entities.comments.CommentDTO;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.CurrentUserPayload;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;

public final class URLs {
    public static final String BASE_URL = "http://localhost:8001";
    public static final String CONFIRM_REGISTRATION_URL_FRAGMENT = "/registration/confirm?token=";
    public static final String CHANGE_PASSWORD_URL_FRAGMENT = "/changePassword?id=";
    public static final String REFRESH_TOKEN_URL = "/refreshToken";
    public static final String AUTO_LOGIN_TOKEN_FRAGMENT = "/autoLogin?token=";


    public static String composeAppUrl(HttpServletRequest request) {
        return "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
    }

    public static String composeFEEntryPoint(){
        return null;
    }

    public static String composePasswordResetUrl(String userId, String token, HttpServletRequest request) {
        return "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath()
                + "/users/changePassword?id=" + userId + "&token=" + token;
    }


    public static URI composeUserLocationUri(CurrentUserPayload persistedUser, HttpServletRequest request) {
        String path = "http://" + request.getServerName() + ":" + request.getServerPort() + "/users";
        return ServletUriComponentsBuilder
                .fromHttpUrl(path)
                .queryParam("id", persistedUser.getId())
                .buildAndExpand(persistedUser.getId())
                .toUri();
    }

    public static URI composeReplyLocationUri(CommentDTO reply, HttpServletRequest request) {
        String path = "http://" + request.getServerName() + ":" + request.getServerPort() + "/reply";
        return ServletUriComponentsBuilder
                .fromHttpUrl(path)
                .queryParam("id", reply.getId())
                .buildAndExpand(reply.getId())
                .toUri();
    }

    public static String composeRecipePdfDownloadUrl(int recipeId, HttpServletRequest request) {
        return composeAppUrl(request) + "/recipes/pdf?recipe=" + recipeId;
    }
}
