package com.mix.cb.cookbook.utils;

import org.springframework.util.SerializationUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Base64;
import java.util.Date;
import java.util.Optional;

public final class Cookies {

    private Cookies() {
    }

    public static Optional<Cookie> getCookie(HttpServletRequest request, String name) {
        Cookie[] cookies = request.getCookies();
        return getCookie(name, cookies);
    }

    public static void addCookie(HttpServletResponse response, String name, String value, int maxAge) {
        Cookie cookie = new Cookie(name, value);
        cookie.setMaxAge(maxAge);
        cookie.setHttpOnly(true);
        cookie.setPath("/");
        response.addCookie(cookie);
    }

    public static Cookie createCookie(String name, String value, int maxAgeSeconds, boolean isHttpOnly) {
        Cookie cookie = new Cookie(name, value);
        cookie.setMaxAge(maxAgeSeconds);
        cookie.setHttpOnly(isHttpOnly);
        cookie.setPath("/");
        return cookie;
    }

    public static void deleteCookie(HttpServletRequest request, HttpServletResponse response, String name) {
        Cookie[] cookies = request.getCookies();
        invalidateCookie(name, response, cookies);
    }

    public static int calculateMaxAgeSec(long expirationTime){
        long now = new Date().getTime();
        return Math.toIntExact((expirationTime - now) / 1000);
    }

    public static String encode(Object o) {
        return Base64.getEncoder().encodeToString(SerializationUtils.serialize(o));
    }

    public static <T> T decode(Cookie cookie, Class<T> clz) {
        return clz.cast(SerializationUtils.deserialize(Base64.getDecoder().decode(cookie.getValue())));
    }

    private static Optional<Cookie> getCookie(String name, Cookie... cookies) {
        if (cookies != null && cookies.length > 0) {
            for (Cookie cookie :
                    cookies) {
                if (cookie.getName().equals(name)) {
                    return Optional.of(cookie);
                }
            }
        }
        return Optional.empty();
    }

    private static void invalidateCookie(String name, HttpServletResponse response, Cookie... cookies) {
        if (cookies != null && cookies.length > 0) {
            for (Cookie cookie :
                    cookies) {
                if (cookie.getName().equals(name)) {
                    cookie.setMaxAge(0);
                    cookie.setPath("/");
                    cookie.setValue("");
                    response.addCookie(cookie);
                }
            }
        }
    }
}
