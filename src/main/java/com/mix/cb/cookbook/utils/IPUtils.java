package com.mix.cb.cookbook.utils;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;

@Slf4j
public final class IPUtils {

    private static final String[] IP_HEADER_CANDIDATES = {
            "X-Forwarded-For",
            "Proxy-Client-IP",
            "WL-Proxy-Client-IP",
            "HTTP_X_FORWARDED_FOR",
            "HTTP_X_FORWARDED",
            "HTTP_X_CLUSTER_CLIENT_IP",
            "HTTP_CLIENT_IP",
            "HTTP_FORWARDED_FOR",
            "HTTP_FORWARDED",
            "HTTP_VIA",
            "REMOTE_ADDR"};

    private IPUtils() {
    }

    public static String getIP(HttpServletRequest request) {
        String xff = request.getHeader("X-Forwarded-For");
        log.error("XFF " + xff);
        if (xff == null) {
            log.error("IP: " + request.getRemoteAddr());
            return request.getRemoteAddr();
        }
        return xff.split(",")[0];
    }


    public static String getClientIpAddress(HttpServletRequest request) {
        //TODO not working correctly
        for (String header : IP_HEADER_CANDIDATES) {
            String ip = request.getHeader(header);
            if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
                log.error(" IP: " + ip);
                return ip;
            }
        }

        log.error("remote add" + request.getRemoteAddr());
        return request.getRemoteAddr();
    }
}
