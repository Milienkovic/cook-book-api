package com.mix.cb.cookbook.utils;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "allowed-front-end")
public class UrlProps {
    private String url;
}
