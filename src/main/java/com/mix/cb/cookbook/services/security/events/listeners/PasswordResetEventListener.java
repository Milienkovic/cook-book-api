package com.mix.cb.cookbook.services.security.events.listeners;

import com.mix.cb.cookbook.services.mail.MailService;
import com.mix.cb.cookbook.services.security.events.OnPasswordResetRequestEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class PasswordResetEventListener implements ApplicationListener<OnPasswordResetRequestEvent> {

    private final MailService mailService;

    public PasswordResetEventListener(MailService mailService) {
        this.mailService = mailService;
    }

    @Override
    public void onApplicationEvent(OnPasswordResetRequestEvent onPasswordResetRequestEvent) {
        mailService.resetPasswordEmail(onPasswordResetRequestEvent);
    }
}
