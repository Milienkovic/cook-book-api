package com.mix.cb.cookbook.services.data.impl;

import com.mix.cb.cookbook.models.entities.Recipe;
import com.mix.cb.cookbook.models.entities.Step;
import com.mix.cb.cookbook.models.entities.User;
import com.mix.cb.cookbook.models.payloads.common.MultipleDeleteRequest;
import com.mix.cb.cookbook.models.payloads.common.MultipleSaveRequest;
import com.mix.cb.cookbook.models.payloads.mutate_entities.steps.MutateStep;
import com.mix.cb.cookbook.models.payloads.mutate_entities.steps.StepDTO;
import com.mix.cb.cookbook.models.payloads.mutate_entities.steps.UpdateStep;
import com.mix.cb.cookbook.repos.RecipeRepo;
import com.mix.cb.cookbook.repos.StepRepo;
import com.mix.cb.cookbook.repos.UserRepo;
import com.mix.cb.cookbook.services.data.StepService;
import com.mix.cb.cookbook.services.exceptions.ResourceExistsException;
import com.mix.cb.cookbook.services.exceptions.ResourceNotFoundException;
import com.mix.cb.cookbook.services.exceptions.UnauthorizedOperationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class StepServiceImpl implements StepService {

    private final StepRepo stepRepo;
    private final RecipeRepo recipeRepo;
    private ModelMapper modelMapper;
    private UserRepo userRepo;

    public StepServiceImpl(StepRepo stepRepo, RecipeRepo recipeRepo) {
        this.stepRepo = stepRepo;
        this.recipeRepo = recipeRepo;
    }

    @Autowired
    public void setUserRepo(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Autowired
    public void setModelMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public List<StepDTO> findAllByRecipeId(int id) {
        return stepRepo.findAllByRecipeId(id).stream()
                .map(step -> modelMapper.map(step, StepDTO.class)).collect(Collectors.toList());
    }

    @Override
    public StepDTO findByRecipeIdAndStepNo(int recipeId, String stepNumber) {
        return modelMapper.map(getStep(recipeId, stepNumber), StepDTO.class);
    }

    @Override
    public StepDTO findById(int id) {
        return modelMapper.map(getStep(id), StepDTO.class);
    }

    @Override
    @Transactional
    public void deleteById(int id, String email) {
        User user = getUser(email);
        Step step = getStep(id);
        isMineRecipe(user, step);
        stepRepo.deleteById(id);
    }

    private void isMineRecipe(User user, Step step) {
        if (!step.getRecipe().getUser().equals(user)) {
            throw new UnauthorizedOperationException("You dont have permission to perform this operation");
        }
    }

    private User getUser(String email) {
        return userRepo.findByEmail(email).orElseThrow(() -> new ResourceNotFoundException(User.class.getSimpleName(), "email", email));
    }


    @Override
    @Transactional
    public StepDTO createStep(int recipeId, MutateStep step) {
        if (stepRepo.existsByRecipeIdAndStepNo(recipeId, step.getStepNo())) {
            throw new ResourceExistsException(Step.class.getSimpleName(), "step number", step.getStepNo());
        }
        Step s = new Step();
        modelMapper.map(step, s);
        return modelMapper.map(saveStep(recipeId, s), StepDTO.class);
    }

    public boolean isRecipeAccessible(int recipeId, Principal principal) {
        Recipe recipe = getRecipe(recipeId);
        if (principal == null) {
            return recipe.isVisible();
        }
        return recipe.getUser().getEmail().equals(principal.getName());
    }

    public boolean isStepMine(int stepId, Principal principal) {
        Step step = getStep(stepId);
        if (principal == null) {
            return step.getRecipe().isVisible();
        }
        return step.getRecipe().getUser().getEmail().equals(principal.getName());
    }

    @Override
    @Transactional
    public StepDTO updateStep(int stepId, MutateStep step) {
        Step s = getStep(stepId);
        modelMapper.map(step, s);
        return modelMapper.map(saveStep(s.getRecipe().getId(), s), StepDTO.class);
    }


    @Override
    @Transactional
    public void saveRecipeSteps(int recipeId, MultipleSaveRequest<MutateStep> saveRequest) {
       saveSteps(recipeId, saveRequest.getItems());
    }

    @Override
    @Transactional
    public void saveRecipeSteps(int recipeId, List<MutateStep> steps) {
       saveSteps(recipeId, steps);
    }

    private void saveSteps(int recipeId, Collection<MutateStep> steps){
        Recipe recipe = getRecipe(recipeId);
        List<Step> s = steps.stream()
                .map(step -> modelMapper.map(step, Step.class))
                .peek(step -> step.setRecipe(recipe))
                .collect(Collectors.toList());
        stepRepo.saveAll(s);
    }

    @Override
    @Transactional
    public void deleteRecipeSteps(MultipleDeleteRequest deleteRequest) {
        stepRepo.deleteAllWithIds(deleteRequest.getIndices());
    }

    @Override
    public void updateSteps(int recipeId, MultipleSaveRequest<UpdateStep> steps) {
        updateRecipeSteps(recipeId, steps.getItems());
    }

    @Override
    public void updateSteps(int recipeId, List<UpdateStep> steps) {
        updateRecipeSteps(recipeId, steps);
    }

    private void updateRecipeSteps(int recipeId, Collection<UpdateStep> steps) {
        Recipe recipe = getRecipe(recipeId);
        List<Step> s = steps.stream()
                .map(step -> modelMapper.map(step, Step.class))
                .peek(step -> step.setRecipe(recipe))
                .collect(Collectors.toList());
        stepRepo.saveAll(s);
    }

    public boolean isRecipeMine(int recipeId, Principal principal) {
        Recipe recipe = getRecipe(recipeId);
        return recipe.getUser().getEmail().equals(principal.getName());
    }

    public boolean areStepsMine(int recipeId, Collection<UpdateStep> steps, Principal principal) {
        Recipe recipe = getRecipe(recipeId);

        if (recipe.getUser().getEmail().equals(principal.getName())) {
            List<Integer> s = recipe.getSteps().stream()
                    .map(Step::getId)
                    .collect(Collectors.toList());
            return s.containsAll(steps.stream().map(UpdateStep::getId).collect(Collectors.toList()));
        }

        return false;
    }

    public boolean areStepsMine(int recipeId, int[] indices, Principal principal) {
        Recipe recipe = getRecipe(recipeId);
        if (recipe.getUser().getEmail().equals(principal.getName())) {
            List<Integer> recipeSteps = recipe.getSteps().stream()
                    .map(Step::getId)
                    .collect(Collectors.toList());
            List<Integer> stepIndices = Arrays.stream(indices).boxed().collect(Collectors.toList());
            return recipeSteps.containsAll(stepIndices);
        }
        return false;
    }

    private Step saveStep(int recipeId, Step step) {
        Recipe recipe = getRecipe(recipeId);
        step.setRecipe(recipe);
        return stepRepo.save(step);
    }

    private Recipe getRecipe(int recipeId) {
        return recipeRepo.findById(recipeId)
                .orElseThrow(() -> new ResourceNotFoundException(Recipe.class.getSimpleName(), "ID", recipeId));
    }

    private Step getStep(int stepId) {
        return stepRepo.findById(stepId)
                .orElseThrow(() -> new ResourceNotFoundException(Step.class.getSimpleName(), "ID", stepId));
    }

    private Step getStep(int recipeId, String stepNo) {
        return stepRepo.findByRecipeIdAndStepNo(recipeId, stepNo)
                .orElseThrow(() -> new ResourceNotFoundException(Step.class.getSimpleName(), "stepNo", stepNo));
    }
}
