package com.mix.cb.cookbook.services.data;

import com.mix.cb.cookbook.models.entities.Role;

public interface RoleService {
    Role findByRole(String role);
}
