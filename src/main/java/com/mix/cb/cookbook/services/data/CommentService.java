package com.mix.cb.cookbook.services.data;

import com.mix.cb.cookbook.models.payloads.mutate_entities.comments.LikeCommentRequest;
import com.mix.cb.cookbook.models.payloads.mutate_entities.comments.ReportCommentRequest;
import com.mix.cb.cookbook.models.payloads.mutate_entities.comments.MutateComment;
import com.mix.cb.cookbook.models.payloads.mutate_entities.comments.CommentDTO;

import java.util.List;

public interface CommentService {
    List<CommentDTO> findAllByReportedAndUserId(boolean reported, int userId);

    List<CommentDTO> findAll();

    List<CommentDTO> findAllReported(boolean reported);

    List<CommentDTO> findAllByRecipeId(boolean reported, int recipeId);

    List<CommentDTO> findAllReplies(boolean reported, int commentId);

    CommentDTO findById(int commentId);

    CommentDTO createComment(String email, int recipeId, MutateComment mutateComment);

    CommentDTO createReply(String email, int commentId, MutateComment mutateComment);

    void deleteComment(int commentId, String email);

    void deleteAllReported();

    CommentDTO updateComment(int commentId, MutateComment mutateComment, String email);

    void likeComment(int commentId, LikeCommentRequest likeCommentRequest);

    void reportComment(int commentId, ReportCommentRequest reportCommentRequest);
}
