package com.mix.cb.cookbook.services.validation;

import com.mix.cb.cookbook.services.validation.validators.DescriptionValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Constraint(validatedBy = DescriptionValidator.class)
public @interface ValidDescription {
    String message() default "{description.invalid}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
