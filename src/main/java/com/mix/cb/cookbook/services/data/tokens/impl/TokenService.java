package com.mix.cb.cookbook.services.data.tokens.impl;

import com.mix.cb.cookbook.models.entities.User;
import com.mix.cb.cookbook.models.entities.tokens.JwtResetToken;
import com.mix.cb.cookbook.models.entities.tokens.PasswordResetToken;
import com.mix.cb.cookbook.models.entities.tokens.VerificationToken;
import com.mix.cb.cookbook.services.data.tokens.JwtResetTokenService;
import com.mix.cb.cookbook.services.data.tokens.PasswordResetTokenService;
import com.mix.cb.cookbook.services.data.tokens.VerificationTokenService;
import com.mix.cb.cookbook.services.exceptions.UnauthorizedOperationException;
import org.springframework.stereotype.Service;

@Service
public class TokenService {

    private final PasswordResetTokenService passwordResetTokenService;
    private final JwtResetTokenService jwtResetTokenService;
    private final VerificationTokenService verificationTokenService;

    public TokenService(PasswordResetTokenService passwordResetTokenService, JwtResetTokenService jwtResetTokenService, VerificationTokenService verificationTokenService) {
        this.passwordResetTokenService = passwordResetTokenService;
        this.jwtResetTokenService = jwtResetTokenService;
        this.verificationTokenService = verificationTokenService;
    }

    public void deleteTokens(User user) {
        passwordResetTokenService.deleteByUser(user);
        jwtResetTokenService.deleteByUser(user);
        verificationTokenService.deleteByUser(user);
    }

    public void invalidateToken(User user, Class<?> c) {

        if (c.getName().equals(PasswordResetToken.class.getName())) {
            passwordResetTokenService.invalidateToken(passwordResetTokenService.findByUser(user));
        } else if (c.getName().equals(JwtResetToken.class.getName())) {
            jwtResetTokenService.invalidateToken(jwtResetTokenService.findByUser(user));
        } else if (c.getName().equals(VerificationToken.class.getName())) {
            verificationTokenService.invalidateToken(verificationTokenService.findByUser(user));
        } else {
            throw new UnauthorizedOperationException("Operation is not supported");
        }
    }

}
