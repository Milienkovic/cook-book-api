package com.mix.cb.cookbook.services.data.impl;

import com.mix.cb.cookbook.models.entities.Role;
import com.mix.cb.cookbook.repos.RoleRepo;
import com.mix.cb.cookbook.services.data.RoleService;
import com.mix.cb.cookbook.services.exceptions.ResourceNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {
    private final RoleRepo roleRepo;

    public RoleServiceImpl(RoleRepo roleRepo) {
        this.roleRepo = roleRepo;
    }

    @Override
    public Role findByRole(String role) {
        return roleRepo.findByRole(role)
                .orElseThrow(() -> new ResourceNotFoundException(Role.class.getSimpleName(), "role", role));
    }
}
