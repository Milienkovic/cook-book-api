package com.mix.cb.cookbook.services.data;

import com.mix.cb.cookbook.models.entities.Ban;

public interface BanService {

    void banUserOnTooManyFailedLoginAttempts(String email, String userIP);

    Ban findByUserEmail(String email);

    boolean isBanExpired(Ban ban);

}
