package com.mix.cb.cookbook.services.schedule.tasks;

import com.mix.cb.cookbook.services.data.CommentService;
import org.springframework.scheduling.annotation.Scheduled;

public class ReportedCommentsPurgeTask {
    private final CommentService commentService;

    public ReportedCommentsPurgeTask(CommentService commentService) {
        this.commentService = commentService;
    }

    @Scheduled(cron = "${purge.cron.expression}")
    public void purgeReportedComments(){
        commentService.deleteAllReported();
    }
}
