package com.mix.cb.cookbook.services.data;

import com.mix.cb.cookbook.models.payloads.mutate_entities.users.address.location.MutateLocation;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.address.location.LocationDTO;

public interface LocationService {
    LocationDTO findByLatitudeAndLongitude(double latitude, double longitude);

    LocationDTO findById(Integer locationId);

    LocationDTO createLocation(MutateLocation location, String email);

    LocationDTO updateLocation(MutateLocation location, String email);

    void deleteLocation(String email);

}
