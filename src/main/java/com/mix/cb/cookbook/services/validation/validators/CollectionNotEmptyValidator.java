package com.mix.cb.cookbook.services.validation.validators;

import com.mix.cb.cookbook.services.validation.CollectionNotEmpty;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Collection;

public class CollectionNotEmptyValidator implements ConstraintValidator<CollectionNotEmpty, Collection> {
    public void initialize(CollectionNotEmpty constraint) {
    }

    public boolean isValid(Collection collection, ConstraintValidatorContext context) {
        return collection.size() > 0;
    }
}
