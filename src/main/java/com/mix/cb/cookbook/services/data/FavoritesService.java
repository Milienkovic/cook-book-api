package com.mix.cb.cookbook.services.data;

public interface FavoritesService {
    void addFavoriteRecipe(String email, int recipeId);
    void removeRecipe(String email, int recipeId);
}
