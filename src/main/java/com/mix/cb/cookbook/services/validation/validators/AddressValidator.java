package com.mix.cb.cookbook.services.validation.validators;

import com.mix.cb.cookbook.models.payloads.mutate_entities.users.address.MutateAddress;
import com.mix.cb.cookbook.services.validation.ValidAddress;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AddressValidator implements ConstraintValidator<ValidAddress, MutateAddress> {
    private String defaultMessage = "";
    private String message = "";

    public void initialize(ValidAddress constraint) {
        this.defaultMessage = constraint.message();
    }

    public boolean isValid(MutateAddress address, ConstraintValidatorContext context) {
        String city = address.getCity();
        String street = address.getStreet();
        String country = address.getCountry();

        if (isInvalidAddressProperty(city)) {
            formMessage("city");
            buildValidatorContext(context, message);
            return false;
        }
        if (isInvalidAddressProperty(street)) {
            formMessage("street");
            buildValidatorContext(context, message);
            return false;
        }
        if (isInvalidAddressProperty(country)) {
            formMessage("country");
            buildValidatorContext(context, message);
            return false;
        }
        return true;
    }

    private boolean isLowerCase(char c) {
        return Character.isLowerCase(c);
    }

    private boolean isInvalidAddressProperty(String prop) {
        return prop.isEmpty() || prop.isBlank() || isLowerCase(prop.charAt(0));
    }

    private void buildValidatorContext(ConstraintValidatorContext context, String message) {
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(message).addConstraintViolation();
    }

    private void formMessage(String propName) {
        message = defaultMessage + propName + " name is not valid; Cannot be empty or start with lower case ";
    }
}
