package com.mix.cb.cookbook.services.exceptions;

import lombok.Getter;

@Getter
public class ResourceExistsException extends RuntimeException {
    private static final String  message = "Resource already exists";
    private Object[] args;
    public ResourceExistsException(String message) {
        super(message);
    }

    public ResourceExistsException(Object... args) {
        super(message);
        this.args = args;
    }
}
