package com.mix.cb.cookbook.services.validation.validators;

import com.mix.cb.cookbook.services.validation.ValidPersonName;
import lombok.extern.slf4j.Slf4j;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class PersonNameValidator implements ConstraintValidator<ValidPersonName, String> {
    private String message = "";
    private String defaultMessage = "";
    private int min;
    private int max;
    private String regex;

    public void initialize(ValidPersonName constraint) {
        this.min = constraint.min();
        this.max = constraint.max();
        checkMinConstraints(min);
        checkMaxConstraints(max);
        this.regex = "^[A-Z][a-z]{min,max}$";
        this.regex = regex.replaceFirst("min", min + "");
        this.regex = regex.replaceFirst("max", max + "");
        this.message = defaultMessage + "must start with uppercase and be between " + min + " and " + max + " characters";
    }

    public boolean isValid(String name, ConstraintValidatorContext context) {
        if (!isValidPersonName(name)) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(message).addConstraintViolation();
            return false;
        }
        return true;
    }

    private boolean isValidPersonName(String name) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(name);
        return matcher.matches() && name.length() >= min + 1 && name.length() <= max + 1;
    }

    private void checkMinConstraints(int min) {
        if (min < 2) {
            this.min = 2;
        }
    }

    private void checkMaxConstraints(int min) {
        if (max > 50) {
            this.max = 49;
        }
    }
}
