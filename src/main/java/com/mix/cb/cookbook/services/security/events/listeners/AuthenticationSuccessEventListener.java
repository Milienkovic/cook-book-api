package com.mix.cb.cookbook.services.security.events.listeners;

import com.mix.cb.cookbook.services.data.UserService;
import com.mix.cb.cookbook.services.security.LoginAttemptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

import static com.mix.cb.cookbook.utils.IPUtils.getClientIpAddress;

@Component
public class AuthenticationSuccessEventListener implements ApplicationListener<AuthenticationSuccessEvent> {
    private HttpServletRequest request;
    private LoginAttemptService loginAttemptService;
    private final UserService userService;

    public AuthenticationSuccessEventListener(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    @Autowired
    public void setLoginAttemptService(LoginAttemptService loginAttemptService) {
        this.loginAttemptService = loginAttemptService;
    }

    @Override
    public void onApplicationEvent(AuthenticationSuccessEvent event) {
        String userIP = getClientIpAddress(request);
        loginAttemptService.loginSucceeded(userIP);
        userService.updateLastVisited(event.getAuthentication().getName());
    }
}
