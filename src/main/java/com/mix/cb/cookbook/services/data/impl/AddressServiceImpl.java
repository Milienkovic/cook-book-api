package com.mix.cb.cookbook.services.data.impl;

import com.mix.cb.cookbook.models.entities.Address;
import com.mix.cb.cookbook.models.entities.User;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.address.MutateAddress;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.address.AddressDTO;
import com.mix.cb.cookbook.repos.AddressRepo;
import com.mix.cb.cookbook.repos.UserRepo;
import com.mix.cb.cookbook.services.data.AddressService;
import com.mix.cb.cookbook.services.exceptions.ResourceExistsException;
import com.mix.cb.cookbook.services.exceptions.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Slf4j
@Service
public class AddressServiceImpl implements AddressService {

    private final AddressRepo addressRepo;
    private final UserRepo userRepo;
    private ModelMapper modelMapper;

    public AddressServiceImpl(AddressRepo addressRepo, UserRepo userRepo) {
        this.addressRepo = addressRepo;
        this.userRepo = userRepo;
    }

    @Autowired
    public void setModelMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public AddressDTO findById(Integer id) {
        return modelMapper.map(getAddress(id), AddressDTO.class);
    }

    @Override
    @Transactional
    public void deleteAddress(String email) {
        User user = getUser(email);
        user.setAddress(null);
        addressRepo.deleteById(user.getId());
    }

    @Override
    @Transactional
    public AddressDTO updateAddress(MutateAddress address, String email) {
        User user = getUser(email);
        Address a = user.getAddress();
        modelMapper.map(address, a);
        return modelMapper.map(saveAddress(a, user), AddressDTO.class);
    }

    @Override
    @Transactional
    public AddressDTO createAddress(MutateAddress address, String email) {
        User user = getUser(email);
        if (user.getAddress() != null)
            throw new ResourceExistsException(Address.class.getSimpleName());
        Address a = new Address();
        modelMapper.map(address, a);
        a.setId(user.getId());
        return modelMapper.map(saveAddress(a, user), AddressDTO.class);
    }

    private Address saveAddress(Address address, User user) {
        address.setUser(user);
        if (address.getLocation() != null) {
            address.getLocation().setAddress(address);
        }
        return addressRepo.save(address);
    }

    private Address getAddress(int addressId) {
        return addressRepo.findById(addressId)
                .orElseThrow(() -> new ResourceNotFoundException(Address.class.getSimpleName(), "ID", addressId));
    }

    private User getUser(String email) {
        return userRepo.findByEmail(email)
                .orElseThrow(() -> new ResourceNotFoundException(User.class.getSimpleName(), "email", email));
    }

}
