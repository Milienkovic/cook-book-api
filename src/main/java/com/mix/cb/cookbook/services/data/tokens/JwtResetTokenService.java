package com.mix.cb.cookbook.services.data.tokens;

import com.mix.cb.cookbook.models.entities.User;
import com.mix.cb.cookbook.models.entities.tokens.JwtResetToken;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.UserDTO;

public interface JwtResetTokenService extends TokenService<JwtResetToken>{
    JwtResetToken createToken(User user);

    JwtResetToken generateNewJwtResetToken(String oldRefreshToken);
    boolean validateJwtResetToken(String refreshToken);
    boolean validateJwtResetToken(JwtResetToken  refreshToken);
    JwtResetToken findByUser(UserDTO user);
}
