package com.mix.cb.cookbook.services.security.events.listeners;

import com.mix.cb.cookbook.services.mail.MailService;
import com.mix.cb.cookbook.services.security.events.OnNewVerificationTokenRequestEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class NewVerificationTokenEventListener implements ApplicationListener<OnNewVerificationTokenRequestEvent> {

    private final MailService mailService;

    public NewVerificationTokenEventListener(MailService mailService) {
        this.mailService = mailService;
    }

    @Override
    public void onApplicationEvent(OnNewVerificationTokenRequestEvent onNewVerificationTokenRequestEvent) {
        mailService.resendVerificationTokenEmail(onNewVerificationTokenRequestEvent);
    }
}
