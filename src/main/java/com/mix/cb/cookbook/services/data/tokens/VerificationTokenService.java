package com.mix.cb.cookbook.services.data.tokens;

import com.mix.cb.cookbook.models.entities.tokens.VerificationToken;

public interface VerificationTokenService extends TokenService<VerificationToken> {
    VerificationToken generateNewVerificationToken(String existingToken);
    VerificationToken findByUserEmail(String email);

}
