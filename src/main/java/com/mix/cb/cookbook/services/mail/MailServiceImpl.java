package com.mix.cb.cookbook.services.mail;

import com.mix.cb.cookbook.models.entities.User;
import com.mix.cb.cookbook.models.entities.tokens.PasswordResetToken;
import com.mix.cb.cookbook.models.entities.tokens.VerificationToken;
import com.mix.cb.cookbook.models.payloads.events.NewVerificationTokenEventData;
import com.mix.cb.cookbook.models.payloads.events.PasswordResetEventData;
import com.mix.cb.cookbook.models.payloads.events.RegistrationEventData;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.CurrentUserPayload;
import com.mix.cb.cookbook.repos.UserRepo;
import com.mix.cb.cookbook.services.data.tokens.PasswordResetTokenService;
import com.mix.cb.cookbook.services.data.tokens.VerificationTokenService;
import com.mix.cb.cookbook.services.exceptions.ResourceNotFoundException;
import com.mix.cb.cookbook.services.security.events.OnNewVerificationTokenRequestEvent;
import com.mix.cb.cookbook.services.security.events.OnPasswordResetRequestEvent;
import com.mix.cb.cookbook.services.security.events.OnRegistrationCompleteEvent;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Locale;

import static com.mix.cb.cookbook.utils.URLs.CHANGE_PASSWORD_URL_FRAGMENT;
import static com.mix.cb.cookbook.utils.URLs.CONFIRM_REGISTRATION_URL_FRAGMENT;

@Service
@Slf4j
public class MailServiceImpl implements MailService {
    private final JavaMailSender mailSender;
    private final Environment environment;
    private final VerificationTokenService verificationTokenService;
    private final PasswordResetTokenService passwordResetTokenService;
    private UserRepo userRepo;
    private ModelMapper mapper;
    private MessageSource messageSource;

    public MailServiceImpl(JavaMailSender mailSender, Environment environment, VerificationTokenService verificationTokenService, PasswordResetTokenService passwordResetTokenService) {
        this.mailSender = mailSender;
        this.environment = environment;
        this.verificationTokenService = verificationTokenService;
        this.passwordResetTokenService = passwordResetTokenService;
    }

    @Autowired
    public void setMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Autowired
    public void setUserRepo(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    @Async
    public void confirmRegistrationEmail(OnRegistrationCompleteEvent event) {
        composeRegistrationEmail(event);
    }

    @Override
    public void resendVerificationTokenEmail(OnNewVerificationTokenRequestEvent event) {
        NewVerificationTokenEventData data = (NewVerificationTokenEventData) event.getSource();
        User user = userRepo.findByEmail(data.getEmail()).orElseThrow(() -> new ResourceNotFoundException(User.class, "email", data.getEmail()));

        if (!user.isEnabled()) {
            if (verificationTokenService.existsByUserEmail(data.getEmail())) {
                VerificationToken oldToken = verificationTokenService.findByUserEmail(data.getEmail());
                if (verificationTokenService.isTokenExpired(oldToken.getToken())) {
                    VerificationToken verificationToken = verificationTokenService.generateNewVerificationToken(oldToken.getToken());
                    composeResendVerificationTokenEmail(data.getAppUrl(), data.getLocale(), verificationToken, user);
                } else {
                    composeTokenValidEmail(data.getAppUrl(), data.getLocale(), oldToken, oldToken.getUser());
                }
            } else {
                VerificationToken verificationToken = verificationTokenService.createToken(data.getEmail());
                composeResendVerificationTokenEmail(data.getAppUrl(), data.getLocale(), verificationToken, user);
            }
        } else {
            composeAccountEnabledAlreadyEmail(data.getLocale(), user);
        }
    }


    @Override
    public void resetPasswordEmail(OnPasswordResetRequestEvent event) {
        PasswordResetEventData data = (PasswordResetEventData) event.getSource();
        CurrentUserPayload user = data.getUser();
        PasswordResetToken resetToken = passwordResetTokenService.createToken(user);
        composerPasswordResetTokenEmail(user, resetToken, data.getAppUrl(), data.getLocale());
    }

    private void composerPasswordResetTokenEmail(CurrentUserPayload user, PasswordResetToken resetToken, String contextPath, Locale locale) {
        String appUrl = contextPath + CHANGE_PASSWORD_URL_FRAGMENT + user.getId() + "&token=" + resetToken.getToken();
        String subject = "Password Reset";
        String message = messageSource.getMessage("message.resetPasswordEmail", null, locale);
        String text = message + "\r\n" + appUrl;
        SimpleMailMessage email = composeTokenEmail(subject, text, user);
        mailSender.send(email);

    }

    private void composeTokenValidEmail(String appUrl, Locale locale, VerificationToken verificationToken, User user) {
        String confirmationUrl = appUrl + CONFIRM_REGISTRATION_URL_FRAGMENT + verificationToken.getToken();
        String message = messageSource.getMessage("auth.message.validToken", null, locale);
        String subject = "Your token is valid still";
        String text = message + " is valid until: " + verificationToken.getExpiryDate() + "\r\n" + confirmationUrl;
        SimpleMailMessage email = composeTokenEmail(subject, text, mapper.map(user, CurrentUserPayload.class));
        mailSender.send(email);
    }

    private void composeAccountEnabledAlreadyEmail(Locale locale, User user) {
        String message = messageSource.getMessage("auth.message.enabled", null, locale);
        SimpleMailMessage email = composeTokenEmail(
                "Your account is activated already",
                message,
                mapper.map(user, CurrentUserPayload.class));
        mailSender.send(email);
    }

    private void composeResendVerificationTokenEmail(String contextPath, Locale locale, VerificationToken newToken, User user) {
        String confirmationUrl = contextPath + CONFIRM_REGISTRATION_URL_FRAGMENT + newToken.getToken();
        String message = messageSource.getMessage("message.resendToken", null, locale) + "\r\n" + confirmationUrl;
        SimpleMailMessage email = composeTokenEmail(
                "Resend Registration Token",
                message,
                mapper.map(user, CurrentUserPayload.class));
        mailSender.send(email);
    }

    private void composeRegistrationEmail(OnRegistrationCompleteEvent event) {
        RegistrationEventData data = (RegistrationEventData) event.getSource();
        VerificationToken verificationToken = verificationTokenService.createToken(data.getUser());
        String confirmationUrl = data.getAppUrl().concat(CONFIRM_REGISTRATION_URL_FRAGMENT).concat(verificationToken.getToken());
        String message = messageSource.getMessage("message.regSuccess", null, data.getLocale()) + " \r\n" + confirmationUrl;
        SimpleMailMessage email = composeTokenEmail("Confirm registration", message, data.getUser());
        mailSender.send(email);
    }

    private SimpleMailMessage composeTokenEmail(String subject, String text, CurrentUserPayload user) {
        String recipientAddress = user.getEmail();
        String from = environment.getProperty("support.email");
        return composeEmail(recipientAddress, from, subject, text);

    }

    private SimpleMailMessage composeEmail(String to, String from, String subject, String text) {
        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(to);
        email.setFrom(from);
        email.setText(text);
        email.setSubject(subject);
        return email;
    }
}
