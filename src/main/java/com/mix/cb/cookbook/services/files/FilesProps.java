package com.mix.cb.cookbook.services.files;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "file")
public class FilesProps {
    private String uploadDir;
}
