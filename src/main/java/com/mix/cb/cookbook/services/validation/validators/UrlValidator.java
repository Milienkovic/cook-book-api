package com.mix.cb.cookbook.services.validation.validators;

import com.mix.cb.cookbook.services.validation.ValidUrl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UrlValidator implements ConstraintValidator<ValidUrl, String> {
   private Pattern pattern;

    public void initialize(ValidUrl constraint) {
       String regex = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
       pattern = Pattern.compile(regex);
    }

    public boolean isValid(String url, ConstraintValidatorContext context) {
        Matcher matcher = pattern.matcher(url);
        return matcher.matches();
    }
}
