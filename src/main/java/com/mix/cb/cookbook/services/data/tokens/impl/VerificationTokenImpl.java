package com.mix.cb.cookbook.services.data.tokens.impl;

import com.mix.cb.cookbook.models.entities.User;
import com.mix.cb.cookbook.models.entities.tokens.Token;
import com.mix.cb.cookbook.models.entities.tokens.VerificationToken;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.CurrentUserPayload;
import com.mix.cb.cookbook.repos.UserRepo;
import com.mix.cb.cookbook.repos.tokens.VerificationTokenRepo;
import com.mix.cb.cookbook.services.data.tokens.VerificationTokenService;
import com.mix.cb.cookbook.services.exceptions.ResourceNotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.UUID;

@Service
public class VerificationTokenImpl implements VerificationTokenService {

    private final VerificationTokenRepo tokenRepo;
    private UserRepo userRepo;

    private ModelMapper mapper;

    public VerificationTokenImpl(VerificationTokenRepo tokenRepo) {
        this.tokenRepo = tokenRepo;
    }

    @Autowired
    public void setMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    @Autowired
    public void setUserRepo(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public VerificationToken findByToken(String token) {
        return getToken(token);
    }

    @Override
    public VerificationToken findByUser(User user) {
        return getToken(user);
    }

    @Override
    public VerificationToken createToken(CurrentUserPayload user) {
        return saveVerificationToken(mapper.map(user, User.class));
    }

    @Override
    public VerificationToken createToken(String email) {
        User user = userRepo.findByEmail(email).orElseThrow(() -> new ResourceNotFoundException(User.class, "email", email));
        return saveVerificationToken(user);
    }

    private VerificationToken saveVerificationToken(User user) {
        String token = createTokenString();

        VerificationToken verificationToken;
        if (tokenRepo.existsByUserEmail(user.getEmail())) {
            verificationToken = getToken(user);
            verificationToken.updateToken(token);
        } else {
            verificationToken = new VerificationToken();
            verificationToken.setToken(token);
            verificationToken.setUser(user);
        }
        return tokenRepo.save(verificationToken);
    }

    @Override
    public boolean existsByUserEmail(String email) {
        return tokenRepo.existsByUserEmail(email);
    }

    @Override
    public boolean existsByToken(String token) {
        return tokenRepo.existsByToken(token);
    }


    @Override
    public VerificationToken generateNewVerificationToken(String existingToken) {
        VerificationToken verificationToken = getToken(existingToken);
        verificationToken.updateToken(UUID.randomUUID().toString());
        return tokenRepo.save(verificationToken);
    }

    @Override
    public VerificationToken findByUserEmail(String email) {
        return getTokenByUserEmail(email);
    }

    @Override
    public void invalidateToken(Token token) {
        if (token instanceof VerificationToken) {
            token.setExpiryDate(new Timestamp(System.currentTimeMillis() - 1000));
            tokenRepo.save((VerificationToken) token);
        } else {
            throw new UnsupportedOperationException("Operation is not supported");
        }
    }

    @Override
    public void deleteByUser(User user) {
        tokenRepo.deleteByUser(user);
    }


    private VerificationToken getToken(String token) {
        return tokenRepo.findByToken(token)
                .orElseThrow(() -> new ResourceNotFoundException(VerificationToken.class.getSimpleName(), "token", token));
    }

    private VerificationToken getToken(User user) {
        return tokenRepo.findByUser(user).orElseThrow(() -> new ResourceNotFoundException("token not found"));
    }

    private VerificationToken getTokenByUserEmail(String email) {
        return tokenRepo.findByUserEmail(email).orElseThrow(() -> new ResourceNotFoundException("token not found"));
    }

}
