package com.mix.cb.cookbook.services.data.impl;

import com.mix.cb.cookbook.models.entities.Address;
import com.mix.cb.cookbook.models.entities.Location;
import com.mix.cb.cookbook.models.entities.User;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.address.location.LocationDTO;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.address.location.MutateLocation;
import com.mix.cb.cookbook.repos.LocationRepo;
import com.mix.cb.cookbook.repos.UserRepo;
import com.mix.cb.cookbook.services.data.LocationService;
import com.mix.cb.cookbook.services.exceptions.ResourceExistsException;
import com.mix.cb.cookbook.services.exceptions.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Slf4j
@Service
public class LocationServiceImpl implements LocationService {

    private final LocationRepo locationRepo;
    private final UserRepo userRepo;
    private ModelMapper modelMapper;

    public LocationServiceImpl(LocationRepo locationRepo, UserRepo userRepo) {
        this.locationRepo = locationRepo;
        this.userRepo = userRepo;
    }

    @Autowired
    public void setModelMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public LocationDTO findByLatitudeAndLongitude(double latitude, double longitude) {
        return modelMapper.map(getLocation(latitude, longitude), LocationDTO.class);
    }

    @Override
    public LocationDTO findById(Integer locationId) {
        return modelMapper.map(getLocation(locationId), LocationDTO.class);
    }

    @Override
    @Transactional
    public LocationDTO createLocation(MutateLocation location, String email) {
        User user = getUser(email);
        if (locationRepo.existsById(user.getId())) {
            throw new ResourceExistsException(Location.class.getSimpleName());
        }
        Location l = new Location();
        modelMapper.map(location, l);
        l.setId(user.getId());
        return modelMapper.map(saveLocation(user, l), LocationDTO.class);
    }

    @Override
    public LocationDTO updateLocation(MutateLocation location, String email) {
        User user = getUser(email);
        Location l = getLocation(user.getId());
        modelMapper.map(location, l);
        return modelMapper.map(saveLocation(user, l), LocationDTO.class);
    }

    @Override
    public void deleteLocation(String email) {
        User user = getUser(email);
        Address address = user.getAddress();
        address.setLocation(null);
        locationRepo.deleteById(user.getId());
    }

    private Location saveLocation(User user, Location location) {
        Address address = user.getAddress();
        location.setAddress(address);
        return locationRepo.save(location);
    }

    private Location getLocation(int locationId) {
        return locationRepo.findById(locationId)
                .orElseThrow(() -> new ResourceNotFoundException(Location.class.getSimpleName(), "ID", locationId));
    }

    private Location getLocation(double latitude, double longitude) {
        return locationRepo.findByLatAndLng(latitude, longitude)
                .orElseThrow(() -> new ResourceNotFoundException("Location with latitude:" + latitude + " and longitude:" + longitude + " not found"));
    }

    private User getUser(String email) {
        return userRepo.findByEmail(email)
                .orElseThrow(() -> new ResourceNotFoundException(User.class.getSimpleName(), "email", email));
    }
}
