package com.mix.cb.cookbook.services.security.events.listeners;

import com.mix.cb.cookbook.services.mail.MailService;
import com.mix.cb.cookbook.services.security.events.OnRegistrationCompleteEvent;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class RegistrationEventListener implements ApplicationListener<OnRegistrationCompleteEvent> {

    private final MailService mailService;


    public RegistrationEventListener(MailService mailService) {
        this.mailService = mailService;
    }

    @Override
    public void onApplicationEvent(@NotNull OnRegistrationCompleteEvent onRegistrationCompleteEvent) {
        mailService.confirmRegistrationEmail(onRegistrationCompleteEvent);
    }
}
