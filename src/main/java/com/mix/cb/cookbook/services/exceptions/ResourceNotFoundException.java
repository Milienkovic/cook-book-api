package com.mix.cb.cookbook.services.exceptions;

import lombok.Getter;

@Getter
public class ResourceNotFoundException extends RuntimeException {
    private static final String message = "Resource not found";
    private Object[] args;
    public ResourceNotFoundException(String message) {
        super(message);
    }

    public ResourceNotFoundException(Object... args) {
        super(message);
        this.args = args;
    }
}
