package com.mix.cb.cookbook.services.validation;

import com.mix.cb.cookbook.services.validation.validators.PersonNameValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PersonNameValidator.class)
public @interface ValidPersonName {
    String message() default "Invalid name: ";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    int min() default 2;
    int max() default 30;
}
