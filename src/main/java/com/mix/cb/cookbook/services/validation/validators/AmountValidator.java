package com.mix.cb.cookbook.services.validation.validators;

import com.mix.cb.cookbook.services.validation.ValidAmount;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AmountValidator implements ConstraintValidator<ValidAmount, Double> {
    public void initialize(ValidAmount constraint) {
    }

    public boolean isValid(Double amount, ConstraintValidatorContext context) {
        return amount > 0.0;
    }
}
