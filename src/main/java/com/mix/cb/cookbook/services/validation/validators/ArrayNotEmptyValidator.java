package com.mix.cb.cookbook.services.validation.validators;

import com.mix.cb.cookbook.services.validation.ArrayNotEmpty;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ArrayNotEmptyValidator implements ConstraintValidator<ArrayNotEmpty, int[]> {


    @Override
    public boolean isValid(int[] value, ConstraintValidatorContext context) {
        return value.length > 0;
    }
}
