package com.mix.cb.cookbook.services.data;

import com.mix.cb.cookbook.models.payloads.common.MultipleDeleteRequest;
import com.mix.cb.cookbook.models.payloads.common.MultipleSaveRequest;
import com.mix.cb.cookbook.models.payloads.mutate_entities.steps.MutateStep;
import com.mix.cb.cookbook.models.payloads.mutate_entities.steps.StepDTO;
import com.mix.cb.cookbook.models.payloads.mutate_entities.steps.UpdateStep;

import java.util.List;

public interface StepService {
    List<StepDTO> findAllByRecipeId(int id);

    StepDTO findByRecipeIdAndStepNo(int recipeId, String stepNumber);

    StepDTO findById(int id);

    void deleteById(int id, String email);

    StepDTO createStep(int recipeId, MutateStep step);

    StepDTO updateStep(int stepId, MutateStep step);

    void saveRecipeSteps(int recipeId, MultipleSaveRequest<MutateStep> saveRequest);
    void saveRecipeSteps(int recipeId, List<MutateStep> saveRequest);

    void deleteRecipeSteps(MultipleDeleteRequest deleteRequest);

    void updateSteps(int recipeId, MultipleSaveRequest<UpdateStep> steps);
    void updateSteps(int recipeId, List<UpdateStep> steps);
}
