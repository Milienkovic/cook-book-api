package com.mix.cb.cookbook.services.files.pdf.impl;

import com.itextpdf.text.DocumentException;
import com.mix.cb.cookbook.models.payloads.files.UploadPdfFileResponse;
import com.mix.cb.cookbook.models.payloads.mutate_entities.recipes.RecipeInfo;
import com.mix.cb.cookbook.services.data.RecipeService;
import com.mix.cb.cookbook.services.exceptions.FileStorageException;
import com.mix.cb.cookbook.services.files.pdf.PDFService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

@Slf4j
@Service
public class PdfServiceImpl implements PDFService {
    //todo can extend file service impl
    private static final Path path = FileSystems.getDefault()
            .getPath("src", "main", "resources", "temp", "uploads", "pdf");
    private RecipeService recipeService;
    private SpringTemplateEngine templateEngine;

    @Autowired
    public void setRecipeService(RecipeService recipeService) {
        this.recipeService = recipeService;
    }

    @Autowired
    public void setTemplateEngine(SpringTemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
    }

    @Override
    public String createRecipePdf(int recipeId, String template) {
        try {
            RecipeInfo recipe = recipeService.findById(recipeId);
            String filename = recipe.getName().concat(".pdf");
            Context context = new Context();
            context.setVariable("recipe", recipe);

            String htmlContent = templateEngine.process(template, context);
            ITextRenderer renderer = new ITextRenderer();

            renderer.setDocumentFromString(htmlContent);
            renderer.layout();
            OutputStream outputStream = new FileOutputStream(createDirectories() + "\\" + filename);
            renderer.createPDF(outputStream);
            outputStream.close();
            return filename;
        } catch (DocumentException | IOException e) {
            throw new FileStorageException("Error while creating pdf file");
        }
    }

    @Override
    public Resource loadPdfAsResource(String filename) {
        try {
            Path pdfPath = path.resolve(filename);
            return new ByteArrayResource(Files.readAllBytes(pdfPath));
        } catch (IOException e) {
            throw new FileStorageException("Error while reading file " + filename);
        }
    }

    @Override
    public UploadPdfFileResponse downloadRecipeAsPdf(int recipeId, String recipeTemplate, String downloadUrl) {
        String pdfName = createRecipePdf(recipeId, recipeTemplate);
        try {
            Resource pdf = loadPdfAsResource(pdfName);

            return new UploadPdfFileResponse(pdfName, downloadUrl, pdf.contentLength(), MediaType.APPLICATION_PDF_VALUE, pdf);
        } catch (IOException e) {
            throw new FileStorageException("Error while downloading pdf");
        } finally {
            deletePdf(path.resolve(pdfName));
            log.error("pdf deleted " + pdfName);
        }
    }

    private void deletePdf(Path path) {
        try {
            Files.deleteIfExists(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String createDirectories() {
        try {
            Files.createDirectories(path);
            return path.toString();
        } catch (IOException e) {
            throw new FileStorageException("Error while creating subdirectory");
        }
    }
}
