package com.mix.cb.cookbook.services.validation;

import com.mix.cb.cookbook.services.validation.validators.AmountValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = AmountValidator.class)
public @interface ValidAmount {
    String message() default "{amount.invalid}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
