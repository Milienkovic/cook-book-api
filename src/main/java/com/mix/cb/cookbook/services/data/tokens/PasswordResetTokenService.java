package com.mix.cb.cookbook.services.data.tokens;

import com.mix.cb.cookbook.models.entities.tokens.PasswordResetToken;

public interface PasswordResetTokenService extends TokenService<PasswordResetToken>{
    PasswordResetToken validatePasswordResetToken(int userId, String token);
    void invalidateToken(String token);
}
