package com.mix.cb.cookbook.services.validation;

import com.mix.cb.cookbook.services.validation.validators.CollectionNotEmptyValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Constraint(validatedBy = CollectionNotEmptyValidator.class)
public @interface CollectionNotEmpty {
    String message() default "{collection.invalid}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
