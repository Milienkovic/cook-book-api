package com.mix.cb.cookbook.services.security.jwt;

import com.mix.cb.cookbook.models.enums.AuthTokenType;
import com.mix.cb.cookbook.services.security.CustomUserDetailsService;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.*;

@Slf4j
@Component
public class JwtTokenProvider {

    private static final String BEARER_PREFIX = "Bearer ";
    private static final String PRIVILEGES_KEY = "roles";

    private final JwtTokenProps tokenProps;
    private final CustomUserDetailsService userDetailsService;

    public JwtTokenProvider(JwtTokenProps tokenProps, CustomUserDetailsService userDetailsService) {
        this.tokenProps = tokenProps;
        this.userDetailsService = userDetailsService;
    }

    public String createToken(String email, List<String> roles) {
        Claims claims = Jwts.claims().setSubject(email);
        claims.put(PRIVILEGES_KEY, roles);
        Date now = new Date();
        long expiryTime = now.getTime() + tokenProps.getTokenExpirationMsec();
        Date expiryDate = new Date(expiryTime);
        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS256, tokenProps.getSecretKey())
                .compact();
    }

    public String createBearerToken(String email, List<String> roles) {
        return AuthTokenType.BEARER.toString() + createToken(email, roles);
    }

    public boolean validateToken(String token) {
        try {
            Jwts.parser().setSigningKey(tokenProps.getSecretKey())
                    .parseClaimsJws(token);
            return true;
        } catch (SignatureException | MalformedJwtException | ExpiredJwtException | UnsupportedJwtException | IllegalArgumentException ex) {
//            ex.printStackTrace();
            return false;
        }
    }


    public String resolveToken(String autHeader) {
        if (autHeader != null && !autHeader.isEmpty() && autHeader.startsWith(BEARER_PREFIX)) {
            return autHeader.substring(7);
        }
        return null;
    }

    public Authentication getAuthentication(String token) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(getEmail(token));
        Set<GrantedAuthority> authorities = getAuthorities(token, PRIVILEGES_KEY);
        authorities.addAll(userDetails.getAuthorities());
        log.error(authorities.toString());
        return new UsernamePasswordAuthenticationToken(userDetails, null, authorities);
    }

    private String getEmail(String token) {
        try {
            return Jwts.parser().setSigningKey(tokenProps.getSecretKey())
                    .parseClaimsJws(token).getBody()
                    .getSubject();
        } catch (ExpiredJwtException | UnsupportedJwtException | MalformedJwtException | SignatureException | IllegalArgumentException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Optional<Object> getPrivileges(String token, String key) {
        Object o = Jwts.parser()
                .setSigningKey(tokenProps.getSecretKey())
                .parseClaimsJws(token)
                .getBody()
                .get(key);
        return Optional.of(o);
    }

    private Set<GrantedAuthority> getAuthorities(String token, String key) {
        Set<GrantedAuthority> authorities = new LinkedHashSet<>();
        getPrivileges(token, key).ifPresent(o -> {
            String[] arrayOfRoles = o.toString().replaceAll("\\[", "").replaceAll("]", "").split(",");
            for (String role : arrayOfRoles) {
                authorities.add(new SimpleGrantedAuthority(role));
            }
        });
        return authorities;
    }

}
