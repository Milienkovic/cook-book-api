package com.mix.cb.cookbook.services.validation;

import com.mix.cb.cookbook.services.validation.validators.FieldValuesMatchValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = FieldValuesMatchValidator.class)
public @interface FieldValuesMatch {
    String message() default "Field values don't match";
    String field();
    String fieldMatch();
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    @interface List{
        FieldValuesMatch[] value();
    }
}
