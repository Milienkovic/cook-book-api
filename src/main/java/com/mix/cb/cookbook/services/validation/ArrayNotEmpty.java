package com.mix.cb.cookbook.services.validation;

import com.mix.cb.cookbook.services.validation.validators.ArrayNotEmptyValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Constraint(validatedBy = ArrayNotEmptyValidator.class)
public @interface ArrayNotEmpty {
    String message() default "Array is not valid";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
