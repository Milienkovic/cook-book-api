package com.mix.cb.cookbook.services.validation;

import com.mix.cb.cookbook.services.validation.validators.AddressValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Constraint(validatedBy = AddressValidator.class)
public @interface ValidAddress {
    String message() default "Invalid address ";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
