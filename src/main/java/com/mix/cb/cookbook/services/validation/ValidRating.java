package com.mix.cb.cookbook.services.validation;

import com.mix.cb.cookbook.services.validation.validators.ValidIntegerValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Retention(value = RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD,ElementType.TYPE})
@Constraint(validatedBy = ValidIntegerValidator.class)
public @interface ValidRating {
    String message() default "{rating.notValid}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
