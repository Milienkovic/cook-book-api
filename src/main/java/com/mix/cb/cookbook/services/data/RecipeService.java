package com.mix.cb.cookbook.services.data;

import com.mix.cb.cookbook.models.enums.Complexity;
import com.mix.cb.cookbook.models.payloads.common.MultipleDeleteRequest;
import com.mix.cb.cookbook.models.payloads.mutate_entities.recipes.MutateRecipe;
import com.mix.cb.cookbook.models.payloads.mutate_entities.recipes.RateRecipeRequest;
import com.mix.cb.cookbook.models.payloads.mutate_entities.recipes.RecipeInfo;

import java.util.List;

public interface RecipeService {
    List<RecipeInfo> findAllByCategoryId(int categoryId, String email);

    List<RecipeInfo> findAll();

    List<RecipeInfo> findAllByCategoryIdAndComplexity(int categoryId, Complexity complexity, String email);

    List<RecipeInfo> findAllByName(String recipeName, String email);

    List<RecipeInfo> findAllByUser(String email);

    List<RecipeInfo> findAllByUserAndCategory(int categoryId, String email);

    RecipeInfo findById(int recipeId, String email);

    RecipeInfo findById(int recipeId);

    void deleteById(int id, String email);

    void deleteAll(String email, MultipleDeleteRequest deleteRequest);

    RecipeInfo createRecipe(MutateRecipe recipe, int categoryId, String email);

    RecipeInfo updateRecipe(int recipeId, MutateRecipe recipe, String email);

    void rateRecipe(int recipeId, RateRecipeRequest rateRecipe);

    void updateRecipeImageUrl(int entityId, String imageUrl);

}
