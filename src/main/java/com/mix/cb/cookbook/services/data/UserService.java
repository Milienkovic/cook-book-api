package com.mix.cb.cookbook.services.data;

import com.mix.cb.cookbook.models.entities.tokens.VerificationToken;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.*;

import java.util.List;

public interface UserService {
    CurrentUserPayload findByEmail(String email);

    CurrentUserPayload findById(int userId);

    UserInfo findUserById(int userId);
    UserInfo findUserByEmail(String email);

    void deleteById(int id);

    void deleteByEmail(String email);

    CurrentUserPayload createUser(UserRegistrationRequest user);

    void updateUser(MutateUser user, int id);

    List<AdminUserPayload> findAll();

    void enableUser(VerificationToken verificationToken);

//    String confirmRegistration(String token, Locale locale);

    void updatePassword(String email, ChangePasswordRequest passwordRequest);

    void updateLastVisited(String email);
}
