package com.mix.cb.cookbook.services.security.events.listeners;

import com.mix.cb.cookbook.services.data.BanService;
import com.mix.cb.cookbook.services.security.LoginAttemptService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

import static com.mix.cb.cookbook.utils.IPUtils.getClientIpAddress;

@Slf4j
@Component
public class AuthenticationFailureEventListener implements ApplicationListener<AuthenticationFailureBadCredentialsEvent> {
    private LoginAttemptService loginAttemptService;
    private HttpServletRequest request;
    private final BanService banService;

    public AuthenticationFailureEventListener(BanService banService) {
        this.banService = banService;
    }

    @Autowired
    public void setLoginAttemptService(LoginAttemptService loginAttemptService) {
        this.loginAttemptService = loginAttemptService;
    }

    @Autowired
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public void onApplicationEvent(AuthenticationFailureBadCredentialsEvent event) {
        String userIP = getClientIpAddress(request);
        loginAttemptService.loginFailed(userIP);
        if (loginAttemptService.isBlocked(userIP)) {
            banService.banUserOnTooManyFailedLoginAttempts(event.getAuthentication().getName(), userIP);
            loginAttemptService.resetKey(userIP);
        }
    }
}
