package com.mix.cb.cookbook.services.security.events;

import com.mix.cb.cookbook.models.payloads.events.RegistrationEventData;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

@Getter
@Setter
public class OnRegistrationCompleteEvent extends ApplicationEvent {

    public OnRegistrationCompleteEvent(RegistrationEventData data) {
        super(data);
    }
}
