package com.mix.cb.cookbook.services.data.impl;

import com.mix.cb.cookbook.models.entities.Ingredient;
import com.mix.cb.cookbook.models.entities.Recipe;
import com.mix.cb.cookbook.models.entities.User;
import com.mix.cb.cookbook.models.payloads.common.MultipleDeleteRequest;
import com.mix.cb.cookbook.models.payloads.common.MultipleSaveRequest;
import com.mix.cb.cookbook.models.payloads.mutate_entities.ingredients.IngredientDTO;
import com.mix.cb.cookbook.models.payloads.mutate_entities.ingredients.MutateIngredient;
import com.mix.cb.cookbook.models.payloads.mutate_entities.ingredients.UpdateIngredient;
import com.mix.cb.cookbook.repos.IngredientRepo;
import com.mix.cb.cookbook.repos.RecipeRepo;
import com.mix.cb.cookbook.repos.UserRepo;
import com.mix.cb.cookbook.services.data.IngredientService;
import com.mix.cb.cookbook.services.exceptions.ResourceExistsException;
import com.mix.cb.cookbook.services.exceptions.ResourceNotFoundException;
import com.mix.cb.cookbook.services.exceptions.UnauthorizedOperationException;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class IngredientServiceImpl implements IngredientService {
    private final IngredientRepo ingredientRepo;
    private final RecipeRepo recipeRepo;
    private UserRepo userRepo;
    private ModelMapper modelMapper;

    public IngredientServiceImpl(IngredientRepo ingredientRepo, RecipeRepo recipeRepo) {
        this.ingredientRepo = ingredientRepo;
        this.recipeRepo = recipeRepo;
    }

    @Autowired
    public void setUserRepo(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Autowired
    public void setModelMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public List<IngredientDTO> findAllByRecipeId(int recipeId) {
        return ingredientRepo.findAllByRecipeId(recipeId).stream()
                .map(ingredient -> modelMapper.map(ingredient, IngredientDTO.class)).collect(Collectors.toList());
    }

    @Override
    public IngredientDTO findById(int id) {
        return modelMapper.map(getIngredient(id), IngredientDTO.class);
    }

    @Override
    @Transactional
    public void deleteById(int id, String email) {
        User user = getUser(email);
        Ingredient ingredient = getIngredient(id);
        isMineRecipe(user, ingredient);
        ingredientRepo.deleteById(id);
    }

    private User getUser(String email) {
        return userRepo.findByEmail(email)
                .orElseThrow(() -> new ResourceNotFoundException(User.class.getSimpleName(), "email", email));
    }

    @Override
    @Transactional
    public IngredientDTO createIngredient(int recipeId, MutateIngredient ingredient, String email) {
        User user = getUser(email);
        isMineRecipe(recipeId, user);
        if (ingredientRepo.existsByRecipeIdAndName(recipeId, ingredient.getName())) {
            throw new ResourceExistsException(Ingredient.class.getSimpleName(), "name", ingredient.getName());
        }
        Ingredient i = new Ingredient();
        modelMapper.map(ingredient, i);
        return modelMapper.map(saveIngredient(recipeId, i), IngredientDTO.class);
    }

    private void isMineRecipe(int recipeId, User user) {
        if (user.getRecipes().stream()
                .map(Recipe::getId)
                .noneMatch(id -> id == recipeId)) {
            throw new UnauthorizedOperationException("You dont have permission to perform this operation");
        }
    }

    @Override
    @Transactional
    public IngredientDTO updateIngredient(int ingredientId, MutateIngredient ingredient, String email) {
        User user = getUser(email);
        Ingredient i = getIngredient(ingredientId);
        isMineRecipe(user, i);
        modelMapper.map(ingredient, i);
        return modelMapper.map(saveIngredient(i.getRecipe().getId(), i), IngredientDTO.class);
    }

    @Override
    @Transactional
    public void saveRecipeIngredients(int recipeId, MultipleSaveRequest<MutateIngredient> saveRequest) {
        Recipe recipe = getRecipe(recipeId);
        List<Ingredient> ingredients = saveRequest.getItems().stream()
                .map(ingredient -> modelMapper.map(ingredient, Ingredient.class))
                .peek(ingredient -> ingredient.setRecipe(recipe))
                .collect(Collectors.toList());
        ingredientRepo.saveAll(ingredients);
    }

    @Override
    @Transactional
    public void saveRecipeIngredients(int recipeId, List<MutateIngredient> ingredients) {
        Recipe recipe = getRecipe(recipeId);
        List<Ingredient> ings = ingredients.stream()
                .map(mutateIngredient -> modelMapper.map(mutateIngredient, Ingredient.class))
                .peek(ingredient -> ingredient.setRecipe(recipe))
                .collect(Collectors.toList());
        ingredientRepo.saveAll(ings);
    }

    @Override
    @Transactional
    public void deleteRecipeIngredients(MultipleDeleteRequest deleteRequest) {
        ingredientRepo.deleteAllWithIds(deleteRequest.getIndices());
    }

    @Override
    @Transactional
    public void updateIngredients(int recipeId, MultipleSaveRequest<UpdateIngredient> ingredients) {
        Recipe recipe = getRecipe(recipeId);
        List<Ingredient> ings = ingredients.getItems().stream()
                .map(ingredient -> modelMapper.map(ingredient, Ingredient.class))
                .peek(ingredient -> ingredient.setRecipe(recipe))
                .collect(Collectors.toList());

        ingredientRepo.saveAll(ings);
    }

    @Override
    @Transactional
    public void updateIngredients(int recipeId, List<UpdateIngredient> ingredients) {
        Recipe recipe = getRecipe(recipeId);
        List<Ingredient> ings = ingredients.stream()
                .map(ing -> modelMapper.map(ing, Ingredient.class))
                .peek(ing -> ing.setRecipe(recipe))
                .collect(Collectors.toList());
        ingredientRepo.saveAll(ings);
    }

    public boolean isRecipeMine(int recipeId, Principal principal) {
        Recipe recipe = getRecipe(recipeId);
        return recipe.getUser().getEmail().equals(principal.getName());
    }

    public boolean areIngredientsMine(int recipeId, Collection<UpdateIngredient> ingredients, Principal principal) {
        Recipe recipe = getRecipe(recipeId);
        if (recipe.getUser().getEmail().equals(principal.getName())) {
            List<Integer> recipeIngredientsIndices = recipe.getIngredients().stream()
                    .map(Ingredient::getId)
                    .collect(Collectors.toList());

            return recipeIngredientsIndices.containsAll(ingredients.stream().map(UpdateIngredient::getId).collect(Collectors.toList()));
        }
        return false;
    }

    public boolean areIngredientsMine(int recipeId, int[] indices, Principal principal) {
        Recipe recipe = getRecipe(recipeId);
        if (recipe.getUser().getEmail().equals(principal.getName())) {
            List<Integer> recipeIngredients = recipe.getIngredients().stream()
                    .map(Ingredient::getId)
                    .collect(Collectors.toList());
            return recipeIngredients.containsAll(Arrays.stream(indices).boxed().collect(Collectors.toList()));
        }
        return false;
    }

    public boolean isIngredientMine(int ingredientId, Principal principal) {
        Ingredient ingredient = getIngredient(ingredientId);
        return ingredient.getRecipe().getUser().getEmail().equals(principal.getName());
    }

    public boolean isRecipeAccessible(int recipeId, Principal principal) {
        Recipe recipe = getRecipe(recipeId);
        log.error(principal + "");
        if (principal == null) {
            return recipe.isVisible();
        }
        return recipe.getUser().getEmail().equals(principal.getName());
    }

    public boolean isIngredientAccessible(int ingredientId, Principal principal) {
        Ingredient ingredient = getIngredient(ingredientId);
        if (principal == null) {
            return ingredient.getRecipe().isVisible();
        }

        return ingredient.getRecipe().getUser().getEmail().equals(principal.getName());
    }

    private void isMineRecipe(User user, Ingredient i) {
        if (!i.getRecipe().getUser().equals(user)) {
            throw new UnauthorizedOperationException("You dont have permission to perform this operation");
        }
    }

    private Ingredient saveIngredient(int recipeId, Ingredient ingredient) {
        Recipe recipe = getRecipe(recipeId);
        ingredient.setRecipe(recipe);
        return ingredientRepo.save(ingredient);
    }

    private Recipe getRecipe(int recipeId) {
        return recipeRepo.findById(recipeId)
                .orElseThrow(() -> new ResourceNotFoundException(Recipe.class.getSimpleName(), "ID", recipeId));
    }

    private Ingredient getIngredient(int ingredientId) {
        return ingredientRepo.findById(ingredientId)
                .orElseThrow(() -> new ResourceNotFoundException(Ingredient.class.getSimpleName(), "ID", ingredientId));
    }
}
