package com.mix.cb.cookbook.services.data;

import com.mix.cb.cookbook.models.entities.Privilege;

import java.util.Set;

public interface PrivilegeService {
    Privilege findByName(String name);
    Set<Privilege> findAllByRoleName(String roleName);
}
