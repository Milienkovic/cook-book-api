package com.mix.cb.cookbook.services.validation.validators;

import com.mix.cb.cookbook.services.validation.ValidDescription;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DescriptionValidator implements ConstraintValidator<ValidDescription, String> {

    @Override
    public boolean isValid(String desc, ConstraintValidatorContext context) {
        return desc.trim().length() > 0 && desc.trim().length() < 256;
    }
}
