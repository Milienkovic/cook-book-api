package com.mix.cb.cookbook.services.validation.validators;

import com.mix.cb.cookbook.services.validation.ValidPassword;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordValidator implements ConstraintValidator<ValidPassword, String> {
   private String message ="";
   private String regex ="";
   public void initialize(ValidPassword constraint) {
      this.message = constraint.message();
      this.regex = constraint.regexp();
   }

   public boolean isValid(String password, ConstraintValidatorContext context) {
      context.disableDefaultConstraintViolation();
      context.buildConstraintViolationWithTemplate(message).addConstraintViolation();
      return password.matches(regex);
   }
}
