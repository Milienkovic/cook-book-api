package com.mix.cb.cookbook.services.data.impl;

import com.mix.cb.cookbook.models.entities.Ban;
import com.mix.cb.cookbook.models.entities.Role;
import com.mix.cb.cookbook.models.entities.User;
import com.mix.cb.cookbook.models.entities.tokens.VerificationToken;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.*;
import com.mix.cb.cookbook.repos.UserRepo;
import com.mix.cb.cookbook.services.data.RoleService;
import com.mix.cb.cookbook.services.data.UserService;
import com.mix.cb.cookbook.services.data.tokens.impl.TokenService;
import com.mix.cb.cookbook.services.exceptions.ResourceExistsException;
import com.mix.cb.cookbook.services.exceptions.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserServiceImpl implements UserService {
    private final UserRepo userRepo;
    private final RoleService roleService;
    private final TokenService tokenService;
    private final PasswordEncoder passwordEncoder;
    private ModelMapper modelMapper;

    public UserServiceImpl(UserRepo userRepo, RoleService roleService, TokenService tokenService, PasswordEncoder passwordEncoder) {
        this.userRepo = userRepo;
        this.roleService = roleService;
        this.tokenService = tokenService;
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    public void setModelMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public CurrentUserPayload findByEmail(String email) {
        return modelMapper.map(getUser(email), CurrentUserPayload.class);
    }

    @Override
    public CurrentUserPayload findById(int userId) {
        return modelMapper.map(getUser(userId), CurrentUserPayload.class);
    }

    @Override
    public UserInfo findUserById(int userId) {
        return modelMapper.map(getUser(userId), UserInfo.class);
    }

    @Override
    public UserInfo findUserByEmail(String email) {
        return modelMapper.map(getUser(email), UserInfo.class);
    }

    @Override
    @Transactional
    public void deleteById(int id) {
        User user = getUser(id);
        tokenService.deleteTokens(user);
        //m-2-m
        userRepo.removeUserFromFavorites(id);
        userRepo.removeUserFromFComments(id);

        userRepo.deleteById(id);
    }

    @Override
    @Transactional
    public void deleteByEmail(String email) {
        User user = getUser(email);
        tokenService.deleteTokens(user);
        userRepo.removeUserFromFavorites(user.getId());
        userRepo.removeUserFromFComments(user.getId());
        userRepo.deleteByEmail(email);
    }

    @Override
    @Transactional
    public CurrentUserPayload createUser(UserRegistrationRequest userDTO) {
        if (userRepo.existsByEmail(userDTO.getEmail())) {
            throw new ResourceExistsException(User.class.getSimpleName(), "email", userDTO.getEmail());
        }
        Role role = roleService.findByRole("USER");
        User user = new User();
        Ban ban = new Ban();
        ban.setUser(user);
        user.setBan(ban);
        modelMapper.map(userDTO, user);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRole(role);
        User persistedUser = saveUser(user);
        log.error(persistedUser + "");
        return modelMapper.map(persistedUser, CurrentUserPayload.class);
    }

    @Override
    @Transactional
    public void updateUser(MutateUser user, int id) {
        User u = getUser(id);
        modelMapper.map(user, u);
        saveUser(u);
    }

    @Override
    public List<AdminUserPayload> findAll() {
        return userRepo.findAll().stream()
                .map(user -> modelMapper.map(user, AdminUserPayload.class))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void enableUser(VerificationToken verificationToken) {
        User user = verificationToken.getUser();
        Timestamp validatedAt = new Timestamp(System.currentTimeMillis());
        user.setValidatedAt(validatedAt);
        user.setEnabled(true);
        userRepo.save(user);
    }

    @Override
    @Transactional
    public void updatePassword(String email, ChangePasswordRequest passwordRequest) {
        User user = getUser(email);
        user.setPassword(passwordEncoder.encode(passwordRequest.getPassword()));
        userRepo.save(user);
    }

    @Override
    public void updateLastVisited(String email) {
        User user = getUser(email);
        user.setLastVisited(new Timestamp(System.currentTimeMillis()));
        userRepo.save(user);
    }

    private User saveUser(User user) {
        if (user.getAddress() != null) {
            user.getAddress().setUser(user);
            if (user.getAddress().getLocation() != null)
                user.getAddress().getLocation().setAddress(user.getAddress());
        }
        return userRepo.save(user);
    }

    private User getUser(int id) {
        return userRepo.findById(id).orElseThrow(() -> new ResourceNotFoundException(User.class.getSimpleName(), "ID", id));
    }

    private User getUser(String email) {
        return userRepo.findByEmail(email).orElseThrow(() -> new ResourceNotFoundException(User.class.getSimpleName(), "email", email));
    }

    public boolean isMe(int userId, String email) {
        User user = getUser(userId);
        return user.getEmail().equals(email);
    }

    public boolean isMe(String email, String authEmail) {
        return email.equals(authEmail);
    }
}
