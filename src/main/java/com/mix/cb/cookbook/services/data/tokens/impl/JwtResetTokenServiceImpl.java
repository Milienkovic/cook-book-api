package com.mix.cb.cookbook.services.data.tokens.impl;

import com.mix.cb.cookbook.models.entities.User;
import com.mix.cb.cookbook.models.entities.tokens.JwtResetToken;
import com.mix.cb.cookbook.models.entities.tokens.Token;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.CurrentUserPayload;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.UserDTO;
import com.mix.cb.cookbook.repos.UserRepo;
import com.mix.cb.cookbook.repos.tokens.JwtResetTokenRepo;
import com.mix.cb.cookbook.services.data.tokens.JwtResetTokenService;
import com.mix.cb.cookbook.services.exceptions.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;

@Slf4j
@Service
public class JwtResetTokenServiceImpl implements JwtResetTokenService {
    private final JwtResetTokenRepo jwtResetTokenRepo;
    private UserRepo userRepo;

    private ModelMapper mapper;

    public JwtResetTokenServiceImpl(JwtResetTokenRepo jwtResetTokenRepo) {
        this.jwtResetTokenRepo = jwtResetTokenRepo;
    }

    @Autowired
    public void setUserRepo(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Autowired
    public void setMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public JwtResetToken findByToken(String token) {
        return getToken(token);
    }

    @Override
    public JwtResetToken findByUser(User user) {
        return getToken(user);
    }


    @Override
    public JwtResetToken createToken(CurrentUserPayload user) {
        User u = mapUser(user);
        return saveResetToken(u);
    }

    @Override
    public JwtResetToken createToken(String email) {
        User user = userRepo.findByEmail(email).orElseThrow(() -> new ResourceNotFoundException(User.class, "email", email));
        return saveResetToken(user);
    }

    @Override
    public JwtResetToken createToken(User user) {
        return saveResetToken(user);
    }

    private JwtResetToken saveResetToken(User user) {
        String token = createTokenString();

        JwtResetToken resetToken;
        if (jwtResetTokenRepo.existsByUser(user)) {
            resetToken = getToken(user);
            resetToken.updateToken(token);
        } else {
            resetToken = new JwtResetToken();
            resetToken.setToken(token);
            resetToken.setUser(user);
        }
        return jwtResetTokenRepo.save(resetToken);
    }

    @Override
    public JwtResetToken generateNewJwtResetToken(String oldResetToken) {
        JwtResetToken resetToken = getToken(oldResetToken);
        String token = createTokenString();
        resetToken.updateToken(token);
        return jwtResetTokenRepo.save(resetToken);
    }

    @Override
    public boolean validateJwtResetToken(String refreshToken) {
        JwtResetToken jwtResetToken = getToken(refreshToken);
        User user = jwtResetToken.getUser();
        return user != null && !isJwtResetTokenExpired(jwtResetToken);
    }

    @Override
    public boolean validateJwtResetToken(JwtResetToken refreshToken) {
        return !isJwtResetTokenExpired(refreshToken) && refreshToken.getUser() != null;
    }

    @Override
    public JwtResetToken findByUser(UserDTO user) {
        User u = mapper.map(user, User.class);
        return getToken(u);
    }

    @Override
    public boolean isTokenExpired(String token) {
        JwtResetToken jwtResetToken = getToken(token);
        return jwtResetToken.getExpiryDate().before(new Date());
    }

    @Override
    public boolean existsByUserEmail(String email) {
        return jwtResetTokenRepo.existsByUserEmail(email);
    }

    @Override
    public boolean existsByToken(String token) {
        return jwtResetTokenRepo.existsByToken(token);
    }

    private boolean isJwtResetTokenExpired(JwtResetToken token) {
        return token.getExpiryDate().before(new Date());
    }

    @Override
    public void deleteByUser(User user) {
        jwtResetTokenRepo.deleteByUser(user);
    }

    @Override
    public void invalidateToken(Token token) {
        if (token instanceof JwtResetToken) {
            token.setExpiryDate(new Timestamp(System.currentTimeMillis() - 1000));
            jwtResetTokenRepo.save((JwtResetToken) token);
        } else {
            throw new UnsupportedOperationException("Operation is not supported");
        }
    }

    private JwtResetToken getToken(String token) {
        return jwtResetTokenRepo.findByToken(token)
                .orElseThrow(() -> new ResourceNotFoundException(JwtResetToken.class.getSimpleName(), "token", token));
    }

    private JwtResetToken getToken(User user) {
        return jwtResetTokenRepo.findByUser(user)
                .orElseThrow(() -> new ResourceNotFoundException("token not found"));
    }

    private User mapUser(CurrentUserPayload userDTO) {
        return mapper.map(userDTO, User.class);
    }
}
