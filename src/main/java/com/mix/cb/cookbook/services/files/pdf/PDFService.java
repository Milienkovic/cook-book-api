package com.mix.cb.cookbook.services.files.pdf;

import com.mix.cb.cookbook.models.payloads.files.UploadPdfFileResponse;
import org.springframework.core.io.Resource;

public interface PDFService {
    String createRecipePdf(int recipeId, String template);

    Resource loadPdfAsResource(String filename);

    UploadPdfFileResponse downloadRecipeAsPdf(int recipeId, String recipeTemplate,String downloadUrl);
}
