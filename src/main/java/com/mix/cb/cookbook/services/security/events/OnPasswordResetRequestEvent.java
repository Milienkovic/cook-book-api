package com.mix.cb.cookbook.services.security.events;

import com.mix.cb.cookbook.models.payloads.events.PasswordResetEventData;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;
@Getter
@Setter
public class OnPasswordResetRequestEvent extends ApplicationEvent {

    public OnPasswordResetRequestEvent(PasswordResetEventData data) {
        super(data);
    }
}
