package com.mix.cb.cookbook.services.data.impl;

import com.mix.cb.cookbook.models.entities.Recipe;
import com.mix.cb.cookbook.models.entities.User;
import com.mix.cb.cookbook.repos.RecipeRepo;
import com.mix.cb.cookbook.repos.UserRepo;
import com.mix.cb.cookbook.services.data.FavoritesService;
import com.mix.cb.cookbook.services.exceptions.ResourceNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class FavoritesServiceImpl implements FavoritesService {
    private enum Action {
        ADD, REMOVE
    }

    private final UserRepo userRepo;
    private final RecipeRepo recipeRepo;

    public FavoritesServiceImpl(UserRepo userRepo, RecipeRepo recipeRepo) {
        this.userRepo = userRepo;
        this.recipeRepo = recipeRepo;
    }

    @Override
    public void addFavoriteRecipe(String email, int recipeId) {
        handleFavorites(email, recipeId, Action.ADD);
    }

    @Override
    public void removeRecipe(String email, int recipeId) {
        handleFavorites(email, recipeId, Action.REMOVE);
    }

    private User getUser(String email) {
        return userRepo.findByEmail(email)
                .orElseThrow(() -> new ResourceNotFoundException(User.class.getSimpleName(), "email", email));
    }

    private Recipe getRecipe(int recipeId) {
        return recipeRepo.findById(recipeId)
                .orElseThrow(() -> new ResourceNotFoundException(Recipe.class.getSimpleName(), "ID", recipeId));
    }

    private void handleFavorites(String email, int recipeId, Action action) {
        User user = getUser(email);
        Recipe recipe = getRecipe(recipeId);
        switch (action) {
            case ADD:
                user.addFavoriteRecipe(recipe);
                userRepo.save(user);
                break;
            case REMOVE:
                user.removeFromFavorites(recipe);
                userRepo.save(user);
                break;
            default:
                throw new UnsupportedOperationException("Action is not supported");
        }
    }
}
