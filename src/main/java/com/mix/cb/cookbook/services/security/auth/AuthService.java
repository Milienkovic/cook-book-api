package com.mix.cb.cookbook.services.security.auth;

import com.mix.cb.cookbook.models.entities.tokens.JwtResetToken;
import com.mix.cb.cookbook.models.payloads.auth.data.AuthToken;
import com.mix.cb.cookbook.models.payloads.auth.data.LoginRequest;
import com.mix.cb.cookbook.models.payloads.common.ResponseMessage;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.ChangePasswordRequest;

import java.util.Locale;

public interface AuthService {
    AuthToken authUserWithoutPasswordOnChangePasswordRequest(int userId, String passwordToken);
    AuthToken authUserWithoutPassword(String verificationToken);
    AuthToken authUserOnRefreshedAuthToken(JwtResetToken refreshToken);
    AuthToken authUserOnRefreshedAuthToken(String refreshToken);

    AuthToken authWithResetTokenAndChangePasswordPrivilege(String resetToken);

    AuthToken authUserWithEmailAndPassword(LoginRequest loginRequest);
    ResponseMessage confirmRegistration(String token, Locale locale);
    ResponseMessage confirmPasswordUpdate(String email, ChangePasswordRequest passwordRequest, String token, Locale locale);

}
