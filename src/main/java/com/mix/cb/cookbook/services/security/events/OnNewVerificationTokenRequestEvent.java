package com.mix.cb.cookbook.services.security.events;


import com.mix.cb.cookbook.models.payloads.events.NewVerificationTokenEventData;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

@Getter
@Setter
public class OnNewVerificationTokenRequestEvent extends ApplicationEvent {

    public OnNewVerificationTokenRequestEvent(NewVerificationTokenEventData data) {
        super(data);
    }
}
