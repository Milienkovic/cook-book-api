package com.mix.cb.cookbook.services.data.tokens.impl;

import com.mix.cb.cookbook.models.entities.User;
import com.mix.cb.cookbook.models.entities.tokens.PasswordResetToken;
import com.mix.cb.cookbook.models.entities.tokens.Token;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.CurrentUserPayload;
import com.mix.cb.cookbook.repos.UserRepo;
import com.mix.cb.cookbook.repos.tokens.PasswordResetTokenRepo;
import com.mix.cb.cookbook.services.data.tokens.PasswordResetTokenService;
import com.mix.cb.cookbook.services.exceptions.InvalidTokenException;
import com.mix.cb.cookbook.services.exceptions.ResourceNotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
public class PasswordResetTokenServiceImpl implements PasswordResetTokenService {

    private final PasswordResetTokenRepo passwordTokenRepo;
    private UserRepo userRepo;
    private ModelMapper mapper;

    public PasswordResetTokenServiceImpl(PasswordResetTokenRepo passwordTokenRepo) {
        this.passwordTokenRepo = passwordTokenRepo;
    }

    @Autowired
    public void setUserRepo(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Autowired
    public void setMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public PasswordResetToken findByToken(String token) {
        return getPasswordResetToken(token);
    }

    @Override
    public PasswordResetToken findByUser(User user) {
        return passwordTokenRepo.findByUser(user).orElseThrow(() -> new ResourceNotFoundException("Token not found"));
    }

    @Override
    public PasswordResetToken createToken(CurrentUserPayload user) {
        return savePasswordToken(mapper.map(user, User.class));
    }

    @Override
    public PasswordResetToken createToken(String email) {
        User user = userRepo.findByEmail(email).orElseThrow(() -> new ResourceNotFoundException(User.class, "email", email));
        return savePasswordToken(user);
    }

    private PasswordResetToken savePasswordToken(User user) {
        String token = createTokenString();

        PasswordResetToken passwordResetToken;
        if (passwordTokenRepo.existsByUser(user)) {
            passwordResetToken = getPasswordResetToken(user);
            passwordResetToken.updateToken(token);
        } else {
            passwordResetToken = new PasswordResetToken();
            passwordResetToken.setToken(token);
            passwordResetToken.setUser(user);
        }
        return passwordTokenRepo.save(passwordResetToken);
    }


    @Override
    public boolean existsByUserEmail(String email) {
        return passwordTokenRepo.existsByUserEmail(email);
    }

    @Override
    public boolean existsByToken(String token) {
        return passwordTokenRepo.existsByToken(token);
    }

    @Override
    public PasswordResetToken validatePasswordResetToken(int userId, String token) {
        PasswordResetToken passwordResetToken = getPasswordResetToken(token);
        if (token == null || (userId != passwordResetToken.getUser().getId()) || isTokenExpired(token)) {
            throw new InvalidTokenException("Password token is not valid");
        }
        return passwordResetToken;
    }

    @Override
    public void invalidateToken(String token) {
        PasswordResetToken passwordResetToken = getPasswordResetToken(token);
        invalidatePasswordToken(token, passwordResetToken);
    }

    private void invalidatePasswordToken(String token, PasswordResetToken passwordResetToken) {
        if (!isTokenExpired(token)) {
            passwordResetToken.setExpiryDate(new Timestamp(System.currentTimeMillis() - 1000));
            passwordTokenRepo.save(passwordResetToken);
        } else {
            throw new InvalidTokenException("Password token is not valid");
        }
    }

    @Override
    public void deleteByUser(User user) {
        passwordTokenRepo.deleteByUser(user);
    }

    @Override
    public void invalidateToken(Token token) {
        if (token instanceof PasswordResetToken) {
            invalidatePasswordToken(token.getToken(), (PasswordResetToken) token);
        } else {
            throw new UnsupportedOperationException("Operation is not supported");
        }
    }


    private PasswordResetToken getPasswordResetToken(String token) {
        return passwordTokenRepo.findByToken(token)
                .orElseThrow(() -> new ResourceNotFoundException(PasswordResetToken.class.getSimpleName(), "token", token));
    }

    private PasswordResetToken getPasswordResetToken(User user) {
        return passwordTokenRepo.findByUser(user).orElseThrow(() -> new ResourceNotFoundException("Token not found"));
    }

}
