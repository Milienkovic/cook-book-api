package com.mix.cb.cookbook.services.data.impl;

import com.mix.cb.cookbook.models.entities.Category;
import com.mix.cb.cookbook.models.entities.Recipe;
import com.mix.cb.cookbook.models.entities.User;
import com.mix.cb.cookbook.models.enums.Complexity;
import com.mix.cb.cookbook.models.payloads.common.MultipleDeleteRequest;
import com.mix.cb.cookbook.models.payloads.mutate_entities.recipes.MutateRecipe;
import com.mix.cb.cookbook.models.payloads.mutate_entities.recipes.RateRecipeRequest;
import com.mix.cb.cookbook.models.payloads.mutate_entities.recipes.RecipeInfo;
import com.mix.cb.cookbook.repos.CategoryRepo;
import com.mix.cb.cookbook.repos.RecipeRepo;
import com.mix.cb.cookbook.repos.UserRepo;
import com.mix.cb.cookbook.services.data.RecipeService;
import com.mix.cb.cookbook.services.exceptions.ResourceNotFoundException;
import com.mix.cb.cookbook.services.exceptions.UnauthorizedAccessException;
import com.mix.cb.cookbook.services.exceptions.UnauthorizedOperationException;
import com.mix.cb.cookbook.services.files.FileStorageService;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static com.mix.cb.cookbook.services.files.impl.FileStorageServiceImpl.Action.RECIPES;

@Slf4j
@Service
public class RecipeServiceImpl implements RecipeService {

    private final RecipeRepo recipeRepo;
    private final CategoryRepo categoryRepo;
    private final UserRepo userRepo;
    private FileStorageService storageService;
    private ModelMapper modelMapper;

    public RecipeServiceImpl(RecipeRepo recipeRepo, CategoryRepo categoryRepo, UserRepo userRepo) {
        this.recipeRepo = recipeRepo;
        this.categoryRepo = categoryRepo;
        this.userRepo = userRepo;
    }

    @Autowired
    public void setModelMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Autowired
    public void setStorageService(FileStorageService storageService) {
        this.storageService = storageService;
    }

    @Override
    public List<RecipeInfo> findAllByCategoryId(int categoryId, String email) {
        return filterRecipesCollection(recipeRepo.findAllByCategoryId(categoryId), email);
    }

    @Override
    public List<RecipeInfo> findAll() {
        return mapRecipes(recipeRepo.findAll());
    }

    @Override
    public List<RecipeInfo> findAllByCategoryIdAndComplexity(int categoryId, Complexity complexity, String email) {
        return filterRecipesCollection(recipeRepo.findAllByCategoryIdAndComplexity(categoryId, complexity), email);
    }


    @Override
    public List<RecipeInfo> findAllByName(String recipeName, String email) {
        return filterRecipesCollection(recipeRepo.findAllByName(recipeName), email);
    }

    @Override
    public List<RecipeInfo> findAllByUser(String email) {
        User user = getUser(email);
        return user.getRecipes().stream()
                .map(recipe -> modelMapper.map(recipe, RecipeInfo.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<RecipeInfo> findAllByUserAndCategory(int categoryId, String email) {
        User user = getUser(email);
        return user.getRecipes()
                .stream()
                .filter(recipe -> recipe.getCategory().getId() == categoryId)
                .map(recipe -> modelMapper.map(recipe, RecipeInfo.class))
                .collect(Collectors.toList());
    }

    @Override
    public RecipeInfo findById(int recipeId, String email) {
        Recipe dbRecipe = getRecipe(recipeId);
        if (isIgnorableRecipe(dbRecipe, email)) {
            throw new UnauthorizedAccessException("You don't have right access privileges to access this resource");
        }
            return modelMapper.map(dbRecipe, RecipeInfo.class);
    }

    @Override
    public RecipeInfo findById(int recipeId) {
        return modelMapper.map(getRecipe(recipeId), RecipeInfo.class);
    }


    @Override
    @Transactional
    public void deleteById(int id, String email) {
        Recipe recipe = getRecipe(id);
        if (!isMineRecipe(recipe, email)) {
            throw new UnauthorizedOperationException("You dont have permission to execute this operation");
        }

        if (recipe.getImageUrl() != null && !recipe.getImageUrl().isEmpty()) {
            storageService.deleteFile(
                    storageService.resolveFilenameFromUrl(recipe.getImageUrl()),
                    RECIPES);
        }
        recipeRepo.deleteById(id);
    }

    @Override
    public void deleteAll(String email, MultipleDeleteRequest deleteRequest) {
        for (int index : deleteRequest.getIndices()) {
            deleteById(index, email);
        }
    }

    @Override
    @Transactional
    public RecipeInfo createRecipe(MutateRecipe recipe, int categoryId, String email) {
        Recipe r = new Recipe();
        modelMapper.map(recipe, r);
        return modelMapper.map(saveRecipe(r, categoryId, email), RecipeInfo.class);
    }

    @Override
    @Transactional
    public RecipeInfo updateRecipe(int recipeId, MutateRecipe recipe, String email) {
        Recipe r = getRecipe(recipeId);
        if (!isMineRecipe(r, email)) {
            throw new UnauthorizedOperationException("You dont have permission to execute this operation");
        }
        modelMapper.map(recipe, r);
        return modelMapper.map(saveRecipe(r, r.getCategory().getId(), r.getUser().getEmail()), RecipeInfo.class);
    }

    @Override
    public void rateRecipe(int recipeId, RateRecipeRequest rateRecipe) {
        Recipe recipe = getRecipe(recipeId);
        double rating = recipe.getRating();
        int votes = recipe.getVotes();
        rating = recalculateRating(rateRecipe, rating, votes);
        recipe.setVotes(recipe.getVotes() + 1);
        recipe.setRating(rating);
        recipeRepo.save(recipe);
    }

    @Override
    public void updateRecipeImageUrl(int entityId, String imageUrl) {
        Recipe recipe = getRecipe(entityId);
        recipe.setImageUrl(imageUrl);
        recipeRepo.save(recipe);
    }

    public boolean isMineRecipe(int recipeId, String email) {
        Recipe recipe = getRecipe(recipeId);
        return recipe.getUser().getEmail().equals(email);
    }

    public boolean areRecipesMine(int[] recipesIds, String email) {
        for (int recipeId : recipesIds) {
            if (!isMineRecipe(recipeId, email)) {
                return false;
            }
        }
        return true;
    }

    public boolean isRecipeAccessible(int recipeId, Principal principal) {
        Recipe recipe = getRecipe(recipeId);
        if (principal == null) {
            return recipe.isVisible();
        }
        return recipe.getUser().getEmail().equals(principal.getName());
    }

    private double recalculateRating(@NotNull RateRecipeRequest rateRecipe, double rating, int votes) {
        return (rating * votes + rateRecipe.getRated()) / (votes + 1);
    }

    private Recipe saveRecipe(@NotNull Recipe r, int categoryId, String email) {
        User user = getUser(email);
        Category category = getCategory(categoryId);
        r.setUser(user);
        r.setCategory(category);
        return recipeRepo.save(r);
    }

    private Category getCategory(int categoryId) {
        return categoryRepo.findById(categoryId)
                .orElseThrow(() -> new ResourceNotFoundException(Category.class.getSimpleName(), "ID", categoryId));
    }

    private Recipe getRecipe(int recipeId) {
        return recipeRepo.findById(recipeId)
                .orElseThrow(() -> new ResourceNotFoundException(Recipe.class.getSimpleName(), "ID", recipeId));
    }

    private User getUser(String email) {
        return userRepo.findByEmail(email)
                .orElseThrow(() -> new ResourceNotFoundException(User.class.getSimpleName(), "email", email));
    }

    private List<RecipeInfo> mapRecipes(@NotNull List<Recipe> recipes) {
        return recipes.stream()
                .map(recipe -> modelMapper.map(recipe, RecipeInfo.class))
                .collect(Collectors.toList());
    }

    /**
     * @param recipe persisted recipe.
     * @param email  users email.
     * @return should recipe be ignored -> false if recipe is public or owned by user, otherwise returns true
     */
    private boolean isIgnorableRecipe(@NotNull Recipe recipe, String email) {
        if (!recipe.isVisible()) {
            if (email != null && !email.isEmpty()) {
                return !recipe.getUser().getEmail().equals(email);
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    private boolean isMineRecipe(@NotNull Recipe recipe, String email) {
        return recipe.getUser().getEmail().equals(email);
    }

    private List<RecipeInfo> filterRecipesCollection(@NotNull Collection<Recipe> recipes, String email) {
        return recipes
                .stream()
                .filter(recipe -> !isIgnorableRecipe(recipe, email))
                .map(recipe -> modelMapper.map(recipe, RecipeInfo.class))
                .collect(Collectors.toList());
    }
}
