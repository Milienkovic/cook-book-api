package com.mix.cb.cookbook.services.validation.validators;

import com.mix.cb.cookbook.services.validation.FieldValuesMatch;
import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class FieldValuesMatchValidator implements ConstraintValidator<FieldValuesMatch, Object> {
    private String field;
    private String fieldMatch;

    public void initialize(FieldValuesMatch constraint) {
        this.field = constraint.field();
        this.fieldMatch = constraint.fieldMatch();
    }

    public boolean isValid(Object object, ConstraintValidatorContext context) {
        Object fieldValue = new BeanWrapperImpl(object).getPropertyValue(field);
        Object fieldMatchValue = new BeanWrapperImpl(object).getPropertyValue(fieldMatch);
        if (fieldValue != null) {
            return fieldValue.equals(fieldMatchValue);
        } else {
            return fieldMatchValue == null;
        }
    }
}
