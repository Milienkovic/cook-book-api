package com.mix.cb.cookbook.services.validation.validators;

import com.mix.cb.cookbook.models.enums.Coordinates;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.address.location.MutateLocation;
import com.mix.cb.cookbook.services.validation.ValidLocation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static com.mix.cb.cookbook.models.enums.Coordinates.*;


public class LocationValidator implements ConstraintValidator<ValidLocation, MutateLocation> {
    String defaultMessage = "";
    String message = "";


    public void initialize(ValidLocation constraint) {
        defaultMessage = constraint.message();
    }

    public boolean isValid(MutateLocation location, ConstraintValidatorContext context) {
        double latitude = location.getLat();
        double longitude = location.getLng();
        if (!isValidLat(latitude)) {
            formErrorMessage(LATITUDE);
            buildConstraintValidatorContext(context, message);
            return false;
        }
        if (!isValidLng(longitude)) {
            formErrorMessage(LONGITUDE);
            buildConstraintValidatorContext(context, message);
            return false;
        }
        return true;
    }

    private boolean isValidLat(double latitude) {
        return -90.0 < latitude && latitude < 90.0;
    }

    private boolean isValidLng(double longitude) {
        return longitude > -180.0 && longitude < 180.0;
    }

    private void formErrorMessage(Coordinates coordinate) {
        if (coordinate == LATITUDE){
            message = defaultMessage + LATITUDE + " is not valid; Must be in -90.0 - 90.0 deg range";
        }
        if (coordinate==LONGITUDE){
            message = defaultMessage + LONGITUDE + " is not valid; Must be in -180.0 - 180.0 deg range";
        }
    }

    private void buildConstraintValidatorContext(ConstraintValidatorContext context, String message) {
        context.buildConstraintViolationWithTemplate(message).addConstraintViolation();
    }
}
