package com.mix.cb.cookbook.services.data.impl;

import com.mix.cb.cookbook.models.entities.Category;
import com.mix.cb.cookbook.models.payloads.common.MultipleDeleteRequest;
import com.mix.cb.cookbook.models.payloads.mutate_entities.categories.CategoryDTO;
import com.mix.cb.cookbook.models.payloads.mutate_entities.categories.CategoryInfo;
import com.mix.cb.cookbook.models.payloads.mutate_entities.categories.MutateCategory;
import com.mix.cb.cookbook.repos.CategoryRepo;
import com.mix.cb.cookbook.services.data.CategoryService;
import com.mix.cb.cookbook.services.exceptions.ResourceExistsException;
import com.mix.cb.cookbook.services.exceptions.ResourceNotFoundException;
import com.mix.cb.cookbook.services.files.FileStorageService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

import static com.mix.cb.cookbook.services.files.impl.FileStorageServiceImpl.Action.CATEGORIES;

@Slf4j
@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepo categoryRepo;
    private FileStorageService storageService;
    private ModelMapper modelMapper;

    public CategoryServiceImpl(CategoryRepo categoryRepo) {
        this.categoryRepo = categoryRepo;
    }

    @Autowired
    public void setModelMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Autowired
    public void setStorageService(FileStorageService storageService) {
        this.storageService = storageService;
    }

    @Override
    public List<CategoryInfo> findAll(Principal principal) {
        List<Category> categories = categoryRepo.findAll();
        return categories.stream()
                .map(category -> {
                    CategoryInfo categoryInfo = modelMapper.map(category, CategoryInfo.class);
                    categoryInfo.setRecipesCount((int) category.getRecipes().stream()
                            .filter(recipe -> {
                                if (principal == null) {
                                    return recipe.isVisible();
                                }
                                return recipe.isVisible() || recipe.getUser().getEmail().equals(principal.getName());
                            }).count());
                    return categoryInfo;
                })
                .collect(Collectors.toList());
    }

    @Override
    public CategoryDTO findById(int id) {
        return modelMapper.map(getCategory(id), CategoryDTO.class);
    }

    @Override
    public CategoryDTO findByName(String categoryName) {
        return modelMapper.map(getCategory(categoryName), CategoryDTO.class);
    }

    @Override
    @Transactional
    public void deleteById(int id) {
        Category category = getCategory(id);
        if (category.getImageUrl() != null && !category.getImageUrl().isEmpty()) {
            storageService.deleteFile(
                    storageService.resolveFilenameFromUrl(category.getImageUrl()),
                    CATEGORIES);
        }
        categoryRepo.delete(category);
    }

    @Override
    public void deleteAll(MultipleDeleteRequest request) {
        for (int index : request.getIndices()) {
            deleteById(index);
        }
    }

    @Override
    @Transactional
    public CategoryDTO createCategory(MutateCategory category, String appUrl) {
        if (categoryRepo.existsByName(category.getName()))
            throw new ResourceExistsException(Category.class.getSimpleName(), "name", category.getName());
        Category c = modelMapper.map(category, Category.class);

        return modelMapper.map(saveCategory(c), CategoryDTO.class);
    }

    @Override
    @Transactional
    public CategoryDTO updateCategory(int categoryId, MutateCategory category) {
        Category c = getCategory(categoryId);
        if (categoryRepo.existsByName(category.getName())) {
            Category cat = getCategory(category.getName());
            if (cat.getId() != categoryId) {
                throw new ResourceExistsException(Category.class.getSimpleName(), "name", category.getName());
            }
        }
        modelMapper.map(category, c);
        return modelMapper.map(saveCategory(c), CategoryDTO.class);
    }

    @Override
    public CategoryDTO updateImageUrl(int categoryId, String imageUrl) {
        Category c = getCategory(categoryId);
        c.setImageUrl(imageUrl);
        return modelMapper.map(saveCategory(c), CategoryDTO.class);
    }

    private Category saveCategory(Category category) {
        return categoryRepo.save(category);
    }

    private Category getCategory(int categoryId) {
        return categoryRepo.findById(categoryId)
                .orElseThrow(() -> new ResourceNotFoundException(Category.class.getSimpleName(), "ID", categoryId));
    }

    private Category getCategory(String categoryName) {
        return categoryRepo.findByName(categoryName)
                .orElseThrow(() -> new ResourceNotFoundException(Category.class.getSimpleName(), "name", categoryName));
    }
}
