package com.mix.cb.cookbook.services.validation.validators;

import com.mix.cb.cookbook.services.validation.ValidRating;
import lombok.extern.slf4j.Slf4j;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Slf4j
public class ValidIntegerValidator implements ConstraintValidator<ValidRating, Integer> {
// todo not compleated

    public void initialize(ValidRating constraint) {
    }

    public boolean isValid(Integer rating, ConstraintValidatorContext context) {

        return rating > 0 && rating <= 5;
    }
}
