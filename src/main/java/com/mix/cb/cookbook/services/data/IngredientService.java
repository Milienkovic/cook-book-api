package com.mix.cb.cookbook.services.data;

import com.mix.cb.cookbook.models.payloads.common.MultipleDeleteRequest;
import com.mix.cb.cookbook.models.payloads.common.MultipleSaveRequest;
import com.mix.cb.cookbook.models.payloads.mutate_entities.ingredients.IngredientDTO;
import com.mix.cb.cookbook.models.payloads.mutate_entities.ingredients.MutateIngredient;
import com.mix.cb.cookbook.models.payloads.mutate_entities.ingredients.UpdateIngredient;

import java.util.List;

public interface IngredientService {

    List<IngredientDTO> findAllByRecipeId(int recipeId);
    IngredientDTO findById(int id);
    void deleteById(int id, String email);
    IngredientDTO createIngredient(int recipeId, MutateIngredient ingredient, String email);
    IngredientDTO updateIngredient(int ingredientId,  MutateIngredient ingredient, String email);

    void saveRecipeIngredients(int recipeId, MultipleSaveRequest<MutateIngredient> saveRequest);
    void saveRecipeIngredients(int recipeId, List<MutateIngredient> saveRequest);

    void deleteRecipeIngredients(MultipleDeleteRequest deleteRequest);

    void updateIngredients(int recipeId, MultipleSaveRequest<UpdateIngredient> ingredients);
    void updateIngredients(int recipeId, List<UpdateIngredient> ingredients);
}
