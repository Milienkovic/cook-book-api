package com.mix.cb.cookbook.services.validation.validators;

import com.mix.cb.cookbook.services.validation.ValidEmail;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ValidEmailValidator implements ConstraintValidator<ValidEmail, String> {
   private String regex ="";
   public void initialize(ValidEmail constraint) {
      this.regex = constraint.regex();
   }

   public boolean isValid(String email, ConstraintValidatorContext context) {
      return email.matches(regex);
   }
}
