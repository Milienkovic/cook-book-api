package com.mix.cb.cookbook.services.validation;

import com.mix.cb.cookbook.services.validation.validators.LocationValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = LocationValidator.class)
public @interface ValidLocation {
    String message() default "Location coordinates are not valid: ";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
