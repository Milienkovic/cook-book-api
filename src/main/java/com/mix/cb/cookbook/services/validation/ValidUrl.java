package com.mix.cb.cookbook.services.validation;

import com.mix.cb.cookbook.services.validation.validators.UrlValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target({ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = UrlValidator.class)
public @interface ValidUrl {
    String message() default "{url.invalid}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
