//package com.mix.cb.cookbook.services.security.auth;
//
//import com.mix.cb.cookbook.models.entities.tokens.JwtResetToken;
//import com.mix.cb.cookbook.models.payloads.auth.data.AuthToken;
//import com.mix.cb.cookbook.services.data.tokens.JwtResetTokenService;
//import com.mix.cb.cookbook.services.security.jwt.JwtTokenProvider;
//import com.mix.cb.cookbook.utils.Cookies;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpHeaders;
//import org.springframework.security.authentication.BadCredentialsException;
//import org.springframework.stereotype.Component;
//import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
//
//import javax.servlet.http.Cookie;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//@Slf4j
//@Component
//public class ExpiredAuthTokenInterceptor extends HandlerInterceptorAdapter {
//    private final JwtTokenProvider tokenProvider;
//    private final JwtResetTokenService jwtResetTokenService;
//
//    private AuthService authService;
//
//    public ExpiredAuthTokenInterceptor(JwtTokenProvider tokenProvider, JwtResetTokenService jwtResetTokenService) {
//        this.tokenProvider = tokenProvider;
//        this.jwtResetTokenService = jwtResetTokenService;
//    }
//@Autowired
//    public void setAuthService(AuthService authService) {
//        this.authService = authService;
//    }
//
//    @Override
//    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
//        handleExpiredAuthToken(request, response);
//        return true;
//    }
//
//    private void handleExpiredAuthToken(HttpServletRequest request, HttpServletResponse response) {
//        //TODO redirect
//        String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
//        String token = tokenProvider.resolveToken(authHeader);
//        if (token != null && !token.isEmpty() && !tokenProvider.validateToken(token)) {
//            Cookie refreshTokenCookie = Cookies.getCookie(request, "refresh-token").orElse(null);
//            if (refreshTokenCookie != null) {
//                String refreshToken = Cookies.decode(refreshTokenCookie, String.class);
//                JwtResetToken jwtResetToken = jwtResetTokenService.findByToken(refreshToken);
//                if (jwtResetTokenService.validateJwtResetToken(jwtResetToken)) {
//                  AuthToken authToken = authService.authUserOnRefreshedAuthToken(jwtResetToken);
//                  response.setHeader(HttpHeaders.AUTHORIZATION, authToken.getAuthToken());
//                } else {
//                    throw new BadCredentialsException("Bad Credentials");
//                }
//            }
//        }
//    }
//
//}