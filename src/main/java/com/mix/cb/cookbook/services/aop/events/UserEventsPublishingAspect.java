package com.mix.cb.cookbook.services.aop.events;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Slf4j
@Aspect
@Component
public class UserEventsPublishingAspect {

    private ApplicationEventPublisher eventPublisher;

    @Autowired
    public void setEventPublisher(ApplicationEventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    @Pointcut("within(com.mix..*) && execution(* com.mix.cb.cookbook.controllers.AuthController.registerUser(com.mix.cb.cookbook.models.payloads.mutate_entities.users.UserRegistrationRequest,..))")
    public void registerUser() {

    }

//    @AfterReturning(value = "registerUser()", returning = "userResponseEntity")
//    public void publishOnRegistrationCompleteEvent(JoinPoint joinPoint, Object userResponseEntity) throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
//        RegistrationEventData data = new RegistrationEventData();
//
//        if (userResponseEntity instanceof ResponseEntity) {
//            data.setUser((UserDTO) ((ResponseEntity) userResponseEntity).getBody());
//            if (((ResponseEntity) userResponseEntity).getBody() instanceof UserDTO) {
//                log.error(Arrays.toString(joinPoint.getArgs()));
//                for (Object arg : joinPoint.getArgs()) {
//                    if (arg instanceof HttpServletRequest) {
//                        HttpServletRequest request = (HttpServletRequest) arg;
//                        data.setLocale(request.getLocale());
//                        data.setAppUrl(composeAppUrl(request));
//                        break;
//                    }
//
//                }
//                eventPublisher.publishEvent(new OnRegistrationCompleteEvent(data));
//            }
//        }
//    }
}

