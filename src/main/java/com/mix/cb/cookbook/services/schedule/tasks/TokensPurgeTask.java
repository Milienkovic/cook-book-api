package com.mix.cb.cookbook.services.schedule.tasks;

import com.mix.cb.cookbook.repos.tokens.JwtResetTokenRepo;
import com.mix.cb.cookbook.repos.tokens.PasswordResetTokenRepo;
import com.mix.cb.cookbook.repos.tokens.VerificationTokenRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Calendar;
import java.util.Date;

@Slf4j
@Service
@Transactional
public class TokensPurgeTask {


    private final VerificationTokenRepo verificationTokenRepo;
    private final PasswordResetTokenRepo passwordResetTokenRepo;
    private final JwtResetTokenRepo jwtResetTokenRepo;

    public TokensPurgeTask(VerificationTokenRepo verificationTokenRepo, PasswordResetTokenRepo passwordResetTokenRepo, JwtResetTokenRepo jwtResetTokenRepo) {
        this.verificationTokenRepo = verificationTokenRepo;
        this.passwordResetTokenRepo = passwordResetTokenRepo;
        this.jwtResetTokenRepo = jwtResetTokenRepo;
    }

    @Scheduled(cron = "${purge.cron.expression}")
//    @Scheduled(fixedDelay = 5000)
    public void purgeTokens() {
        Date now = new Date();
        Date afterExpiration = addTime(now, 72);
        verificationTokenRepo.deleteAllByExpiryDateLessThan(afterExpiration);
        passwordResetTokenRepo.deleteAllByExpiryDateLessThan(afterExpiration);
        jwtResetTokenRepo.deleteAllByExpiryDateLessThan(afterExpiration);
        log.error("Tokens purged");
    }

    private Date addTime(Date now, int amount){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);
        calendar.add(Calendar.HOUR, amount);
        return calendar.getTime();
    }
}
