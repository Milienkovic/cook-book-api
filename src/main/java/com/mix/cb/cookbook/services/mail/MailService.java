package com.mix.cb.cookbook.services.mail;

import com.mix.cb.cookbook.services.security.events.OnNewVerificationTokenRequestEvent;
import com.mix.cb.cookbook.services.security.events.OnRegistrationCompleteEvent;
import com.mix.cb.cookbook.services.security.events.OnPasswordResetRequestEvent;


public interface MailService {
    void confirmRegistrationEmail(OnRegistrationCompleteEvent event);
    void resendVerificationTokenEmail(OnNewVerificationTokenRequestEvent onNewVerificationTokenRequestEvent);
    void resetPasswordEmail(OnPasswordResetRequestEvent event);
}
