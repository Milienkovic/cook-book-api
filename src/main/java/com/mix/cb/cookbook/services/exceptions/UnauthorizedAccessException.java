package com.mix.cb.cookbook.services.exceptions;

import lombok.Getter;

@Getter
public class UnauthorizedAccessException extends RuntimeException {
    private static final String message = "Unauthorized Access";
    private Object[] args;

    public UnauthorizedAccessException(String message) {
        super(message);
    }

    public UnauthorizedAccessException(Object[] args) {
        super(message);
        this.args = args;
    }
}
