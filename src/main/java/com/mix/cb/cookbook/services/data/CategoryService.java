package com.mix.cb.cookbook.services.data;

import com.mix.cb.cookbook.models.payloads.common.MultipleDeleteRequest;
import com.mix.cb.cookbook.models.payloads.mutate_entities.categories.CategoryDTO;
import com.mix.cb.cookbook.models.payloads.mutate_entities.categories.CategoryInfo;
import com.mix.cb.cookbook.models.payloads.mutate_entities.categories.MutateCategory;

import java.security.Principal;
import java.util.List;

public interface CategoryService {
    List<CategoryInfo> findAll(Principal principal);

    CategoryDTO findById(int id);

    CategoryDTO findByName(String categoryName);

    void deleteById(int id);

    void deleteAll(MultipleDeleteRequest request);

    CategoryDTO createCategory(MutateCategory category, String appUrl);

    CategoryDTO updateCategory(int categoryId, MutateCategory category);

    CategoryDTO updateImageUrl(int categoryId, String imageUrl);

}
