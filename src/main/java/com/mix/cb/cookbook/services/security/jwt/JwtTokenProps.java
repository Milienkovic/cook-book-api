package com.mix.cb.cookbook.services.security.jwt;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.annotation.PostConstruct;
import java.util.Base64;

@Data
@ConfigurationProperties(prefix = "security.jwt.token")
public class JwtTokenProps {
    private String secretKey;
    private long tokenExpirationMsec;

    @PostConstruct
    private void secretKey(){
        this.secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes());
    }
}
