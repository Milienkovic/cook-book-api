package com.mix.cb.cookbook.services.exceptions;

import lombok.Getter;

@Getter
public class UnauthorizedOperationException extends RuntimeException {
    private static final String message = "Unauthorized operation";
    private Object[] args;
    public UnauthorizedOperationException(String message) {
        super(message);
    }

    public UnauthorizedOperationException(Object... args) {
        super(message);
        this.args = args;
    }
}
