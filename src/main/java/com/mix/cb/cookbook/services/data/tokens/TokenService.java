package com.mix.cb.cookbook.services.data.tokens;

import com.mix.cb.cookbook.models.entities.User;
import com.mix.cb.cookbook.models.entities.tokens.Token;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.CurrentUserPayload;

import java.util.Date;
import java.util.UUID;

public interface TokenService<T extends Token> {
    T findByToken(String token);

    T findByUser(User user);

    default String createTokenString() {
        return UUID.randomUUID().toString();
    }

    T createToken(CurrentUserPayload user);
    T createToken(String email);

    default boolean isTokenExpired(String token){
        T t = findByToken(token);
        return t.getExpiryDate().before(new Date());
    }
    boolean existsByUserEmail(String email);
    boolean existsByToken(String token);
    void deleteByUser(User user);
    void invalidateToken(Token token);
}
