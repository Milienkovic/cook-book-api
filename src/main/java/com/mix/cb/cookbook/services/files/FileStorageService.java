package com.mix.cb.cookbook.services.files;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;

import static com.mix.cb.cookbook.services.files.impl.FileStorageServiceImpl.Action;

public interface FileStorageService {
    String storeFile(MultipartFile file);

    Resource loadFileAsResource(String fileName, Action action);

    void deleteFile(String fileName, Action action);

    Path createSubDirectory(Action action);

    String storeImage(MultipartFile file, int entityId, String appUrl, String email, Action action);

    String resolveFilenameFromUrl(String imageUrl);
}
