package com.mix.cb.cookbook.services.security.auth;

import com.mix.cb.cookbook.models.entities.Privilege;
import com.mix.cb.cookbook.models.entities.User;
import com.mix.cb.cookbook.models.entities.tokens.JwtResetToken;
import com.mix.cb.cookbook.models.entities.tokens.PasswordResetToken;
import com.mix.cb.cookbook.models.entities.tokens.Token;
import com.mix.cb.cookbook.models.entities.tokens.VerificationToken;
import com.mix.cb.cookbook.models.payloads.auth.data.AuthToken;
import com.mix.cb.cookbook.models.payloads.auth.data.LoginRequest;
import com.mix.cb.cookbook.models.payloads.common.ResponseMessage;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.ChangePasswordRequest;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.CurrentUserPayload;
import com.mix.cb.cookbook.services.data.UserService;
import com.mix.cb.cookbook.services.data.tokens.JwtResetTokenService;
import com.mix.cb.cookbook.services.data.tokens.PasswordResetTokenService;
import com.mix.cb.cookbook.services.data.tokens.VerificationTokenService;
import com.mix.cb.cookbook.services.exceptions.InvalidTokenException;
import com.mix.cb.cookbook.services.exceptions.ResourceNotFoundException;
import com.mix.cb.cookbook.services.security.jwt.JwtTokenProvider;
import com.mix.cb.cookbook.utils.Cookies;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import static com.mix.cb.cookbook.utils.Cookies.calculateMaxAgeSec;

@Slf4j
@Service
public class AuthServiceImpl implements AuthService {

    private final PasswordResetTokenService passwordResetTokenService;
    private final VerificationTokenService verificationTokenService;
    private final UserService userService;
    private final JwtResetTokenService jwtResetTokenService;

    private ModelMapper mapper;
    private MessageSource messageSource;
    private JwtTokenProvider tokenProvider;
    private AuthenticationManager authenticationManager;

    public AuthServiceImpl(PasswordResetTokenService passwordResetTokenService, VerificationTokenService verificationTokenService, UserService userService, JwtResetTokenService jwtResetTokenService) {
        this.passwordResetTokenService = passwordResetTokenService;
        this.verificationTokenService = verificationTokenService;
        this.userService = userService;
        this.jwtResetTokenService = jwtResetTokenService;
    }

    @Autowired
    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Autowired
    public void setTokenProvider(JwtTokenProvider tokenProvider) {
        this.tokenProvider = tokenProvider;
    }

    @Autowired
    public void setMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Override
    public AuthToken authUserWithoutPasswordOnChangePasswordRequest(int userId, String passwordToken) {
        PasswordResetToken resetToken = passwordResetTokenService.validatePasswordResetToken(userId, passwordToken);
        User user = getUser(resetToken);
        String[] privileges = user.getRole().getPrivileges().stream()
                .filter(privilege -> privilege.getPrivilege().equals("CHANGE_PASSWORD_PRIVILEGE"))
                .map(Privilege::getPrivilege)
                .toArray(String[]::new);
        AuthToken authToken = createAuthToken(user, privileges);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user,
                null,
                AuthorityUtils.createAuthorityList(privileges));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return authToken;
    }

    @Override
    public AuthToken authUserWithoutPassword(String verificationToken) {
        VerificationToken token = verificationTokenService.findByToken(verificationToken);
        User user = getUser(token);
        AuthToken authToken = createAuthToken(user, user.getRole().getRole());

        Authentication authentication = new UsernamePasswordAuthenticationToken(user, null,
                AuthorityUtils.commaSeparatedStringToAuthorityList(user.getRole().getRole()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return authToken;
    }

    @Override
    public AuthToken authUserOnRefreshedAuthToken(JwtResetToken jwtResetToken) {
        return authWithResetToken(jwtResetToken.getToken(), null);
    }

    @Override
    public AuthToken authUserOnRefreshedAuthToken(String refreshToken) {
        return authWithResetToken(refreshToken, null);
    }

    @Override
    public AuthToken authWithResetTokenAndChangePasswordPrivilege(String resetToken) {
        JwtResetToken jwtResetToken = jwtResetTokenService.findByToken(resetToken);
        if (jwtResetToken.getExpiryDate().before(new Timestamp(System.currentTimeMillis()))) {
            throw new InvalidTokenException("Token is not valid");
        }
        User user = jwtResetToken.getUser();
        List<String> privileges = user.getRole().getPrivileges().stream()
                .filter(privilege -> privilege.getPrivilege().equals("CHANGE_PASSWORD_PRIVILEGE"))
                .map(Privilege::getPrivilege)
                .collect(Collectors.toList());
        String bearerToken = tokenProvider.createBearerToken(user.getEmail(), privileges);
        Authentication authentication = tokenProvider.getAuthentication(tokenProvider.resolveToken(bearerToken));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return new AuthToken(user.getEmail(), bearerToken, null);
    }

    private AuthToken authWithResetToken(String resetToken, List<String> privileges) {
        JwtResetToken jwtResetToken = jwtResetTokenService.findByToken(resetToken);
        if (jwtResetToken.getExpiryDate().before(new Timestamp(System.currentTimeMillis()))) {
            throw new InvalidTokenException("Token is not valid");
        }
        String bearerToken;
        User user = jwtResetToken.getUser();
        if (privileges != null) {
            bearerToken = tokenProvider.createBearerToken(user.getEmail(), privileges);
        } else {
            bearerToken = tokenProvider.createBearerToken(user.getEmail(), Arrays.asList(user.getRole().getRole()));
        }
        Authentication authentication = tokenProvider.getAuthentication(tokenProvider.resolveToken(bearerToken));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return new AuthToken(user.getEmail(), bearerToken, null);

    }

    @Override
    public AuthToken authUserWithEmailAndPassword(LoginRequest loginRequest) {
        CurrentUserPayload userDTO = userService.findByEmail(loginRequest.getEmail());
        User user = mapper.map(userDTO, User.class);
        AuthToken authToken = createAuthToken(user, user.getRole().getRole());
        Authentication authentication = new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword());
        authenticationManager.authenticate(authentication);
        return authToken;
    }

    @Override
    public ResponseMessage confirmRegistration(String token, Locale locale) {
        ResponseMessage responseMessage;
        if (verificationTokenService.existsByToken(token)) {
            VerificationToken verificationToken = verificationTokenService.findByToken(token);
            if (verificationTokenService.isTokenExpired(token)) {
                throw new InvalidTokenException(messageSource.getMessage("auth.message.invalidToken", null, locale));
            } else {
                if (!verificationToken.getUser().isEnabled()) {
                    userService.enableUser(verificationToken);
                    verificationTokenService.invalidateToken(verificationToken);
                    responseMessage = createResponseMessage(messageSource.getMessage("auth.message.accountEnabled", null, locale), true);
                } else {
                    responseMessage = createResponseMessage(messageSource.getMessage("auth.message.accountAlreadyEnabled", null, locale), false);
                }
            }
        } else {
            throw new ResourceNotFoundException(VerificationToken.class.getSimpleName(), "token", token);
        }

        return responseMessage;
    }

    @Override
    public ResponseMessage confirmPasswordUpdate(String email, ChangePasswordRequest passwordRequest, String token, Locale locale) {
        ResponseMessage responseMessage;
        if (passwordResetTokenService.existsByToken(token)) {
            if (!passwordResetTokenService.isTokenExpired(token)) {
                userService.updatePassword(email, passwordRequest);
                passwordResetTokenService.invalidateToken(token);
                responseMessage = createResponseMessage(messageSource.getMessage("message.updatePasswordSuc", null, locale), true);
            } else {
                responseMessage = createResponseMessage(messageSource.getMessage("auth.message.tokenExpired", null, locale), false);
            }
        } else {
            throw new ResourceNotFoundException(PasswordResetToken.class.getSimpleName(), "token", token);
        }
        return responseMessage;
    }

    private Cookie createRefreshTokenCookie(JwtResetToken refreshToken) {
        int maxAge = calculateMaxAgeSec(refreshToken.getExpiryDate().getTime());
        String serializedToken = Cookies.encode(refreshToken.getToken());
        return Cookies.createCookie("refresh-token", serializedToken, maxAge, false);
    }

    private User getUser(Token token) {
        return token.getUser();
    }


    private AuthToken createAuthToken(User user, String... privileges) {
        JwtResetToken jwtResetToken = jwtResetTokenService.createToken(user);
        Cookie resetTokenCookie = createRefreshTokenCookie(jwtResetToken);
        String bearerToken = tokenProvider.createBearerToken(user.getEmail(), Arrays.asList(privileges));
        return new AuthToken(user.getEmail(), bearerToken, resetTokenCookie);
    }

    private ResponseMessage createResponseMessage(String message, boolean success) {
        return ResponseMessage.builder()
                .message(message)
                .isSuccess(success)
                .build();
    }
}