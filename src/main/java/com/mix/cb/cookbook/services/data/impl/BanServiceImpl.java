package com.mix.cb.cookbook.services.data.impl;

import com.mix.cb.cookbook.models.entities.Ban;
import com.mix.cb.cookbook.models.entities.Reason;
import com.mix.cb.cookbook.models.enums.BanReason;
import com.mix.cb.cookbook.models.enums.BanTime;
import com.mix.cb.cookbook.repos.BanRepo;
import com.mix.cb.cookbook.repos.ReasonRepo;
import com.mix.cb.cookbook.services.data.BanService;
import com.mix.cb.cookbook.services.exceptions.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

@Service
public class BanServiceImpl implements BanService {
    private final BanRepo banRepo;
    private final ReasonRepo reasonRepo;

    public BanServiceImpl(BanRepo banRepo, ReasonRepo reasonRepo) {
        this.banRepo = banRepo;
        this.reasonRepo = reasonRepo;
    }

    @Override
    public void banUserOnTooManyFailedLoginAttempts(String email, String userIP) {
        Ban ban = findByUserEmail(email);
        ban.setIP(userIP);
        handleTooManyFailedAttemptsReason(ban);
        ban.setBannedUntil(calculateTime(BanTime.HOUR));
        ban.setNob(ban.getNob() + 1);
        if (ban.getNob() >= 10) {
            ban.setPerma(true);
        }
        banRepo.save(ban);
    }

    private void handleTooManyFailedAttemptsReason(Ban ban) {
        Reason reason = new Reason();
        reason.setBan(ban);
        reason.setBanTimeInMinutes(BanTime.HOUR.time());
        reason.setReason(BanReason.TOO_MANY_FAILED_ATTEMPTS.reason());
        reasonRepo.save(reason);
    }

    @Override
    public Ban findByUserEmail(String email) {
        return banRepo.findByUserEmail(email)
                .orElseThrow(() -> new ResourceNotFoundException(Ban.class.getSimpleName(), "email", email));
    }

    @Override
    public boolean isBanExpired(Ban ban) {
        return ban.getBannedUntil().before(new Date());
    }

    private Timestamp calculateTime(BanTime banTime) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, banTime.time());
        return new Timestamp(calendar.getTimeInMillis());
    }
}
