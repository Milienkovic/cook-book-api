package com.mix.cb.cookbook.services.security;


import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class LoginAttemptService {
    private final int MAX_ATTEMPTS = 10;
    private LoadingCache<String, Integer> failAttempts;

    public LoginAttemptService() {
        failAttempts = CacheBuilder.newBuilder().expireAfterWrite(1, TimeUnit.DAYS)
                .build(new CacheLoader<>() {
                    @Override
                    public Integer load(String s) {
                        return 0;
                    }
                });
    }

    public void loginSucceeded(String key) {
        invalidateKey(key);
    }

    public void resetKey(String key) {
        invalidateKey(key);
    }

    public void loginFailed(String key) {
        int attempts;
        try {
            attempts = failAttempts.get(key);
        } catch (ExecutionException e) {
            attempts = 0;
        }
        attempts++;
        log.error("failed attempts " + attempts);
        failAttempts.put(key, attempts);
    }

    public boolean isBlocked(String key) {
        try {
            return failAttempts.get(key) >= MAX_ATTEMPTS;
        } catch (ExecutionException e) {
            return false;
        }
    }

    private void invalidateKey(String key) {
        failAttempts.invalidate(key);
    }
}
