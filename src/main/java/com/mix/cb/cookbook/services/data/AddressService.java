package com.mix.cb.cookbook.services.data;

import com.mix.cb.cookbook.models.payloads.mutate_entities.users.address.MutateAddress;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.address.AddressDTO;

public interface AddressService {
    AddressDTO findById(Integer id);

    void deleteAddress(String email);

    AddressDTO updateAddress(MutateAddress address, String email);

    AddressDTO createAddress(MutateAddress address, String email);
}
