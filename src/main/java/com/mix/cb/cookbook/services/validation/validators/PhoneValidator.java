package com.mix.cb.cookbook.services.validation.validators;

import com.mix.cb.cookbook.services.validation.ValidPhone;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PhoneValidator implements ConstraintValidator<ValidPhone, String> {
    private String regex = "";

    public void initialize(ValidPhone constraint) {
        this.regex = constraint.regexp();
    }

    public boolean isValid(String phone, ConstraintValidatorContext context) {
        return phone.matches(regex);
    }
}
