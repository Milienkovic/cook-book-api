package com.mix.cb.cookbook.services.data.impl;

import com.mix.cb.cookbook.models.entities.Privilege;
import com.mix.cb.cookbook.repos.PrivilegeRepo;
import com.mix.cb.cookbook.services.data.PrivilegeService;
import com.mix.cb.cookbook.services.exceptions.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class PrivilegeServiceImpl implements PrivilegeService {
    private final PrivilegeRepo privilegeRepo;

    public PrivilegeServiceImpl(PrivilegeRepo privilegeRepo) {
        this.privilegeRepo = privilegeRepo;
    }

    @Override
    public Privilege findByName(String name) {
        return getPrivilege(name);
    }

    @Override
    public Set<Privilege> findAllByRoleName(String role) {
        return new HashSet<>(privilegeRepo.findAllByRoleName(role));
    }

    private Privilege getPrivilege(String privilege) {
        return privilegeRepo.findByPrivilege(privilege)
                .orElseThrow(() -> new ResourceNotFoundException(Privilege.class.getSimpleName(), "privilege", privilege));
    }
}
