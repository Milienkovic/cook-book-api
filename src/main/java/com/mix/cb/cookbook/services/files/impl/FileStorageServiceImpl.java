package com.mix.cb.cookbook.services.files.impl;

import com.mix.cb.cookbook.models.payloads.mutate_entities.categories.CategoryDTO;
import com.mix.cb.cookbook.models.payloads.mutate_entities.recipes.RecipeInfo;
import com.mix.cb.cookbook.services.data.CategoryService;
import com.mix.cb.cookbook.services.data.RecipeService;
import com.mix.cb.cookbook.services.exceptions.FileStorageException;
import com.mix.cb.cookbook.services.exceptions.InvalidUrlFormatException;
import com.mix.cb.cookbook.services.files.FileStorageService;
import com.mix.cb.cookbook.services.files.FilesProps;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Slf4j
@Service
public class FileStorageServiceImpl implements FileStorageService {

    public enum Action {
        CATEGORIES, RECIPES, PDF, DEFAULT
    }

    private static final String CATEGORIES = "categories";
    private static final String RECIPES = "recipes";
    private static final String PDF = "pdf";

    private final Path fileStorageLocation;

    private CategoryService categoryService;
    private RecipeService recipeService;

    public FileStorageServiceImpl(FilesProps filesProps) {
        this.fileStorageLocation = Paths.get(filesProps.getUploadDir()).toAbsolutePath().normalize();
        log.error(fileStorageLocation.toString());
        try {

            Files.createDirectories(this.fileStorageLocation);
        } catch (IOException e) {
            throw new FileStorageException("Could not create the directory where uploaded files will be stored.", e);
        }
    }

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @Autowired
    public void setRecipeService(RecipeService recipeService) {
        this.recipeService = recipeService;
    }

    @Override
    public String storeFile(MultipartFile file) {
        return storeFile(file, fileStorageLocation, StringUtils.trimAllWhitespace(StringUtils.cleanPath(file.getOriginalFilename())));
    }

    @Override
    public String storeImage(MultipartFile image, int entityId, String appUrl, String email, Action action) {
        Path startLocation = createSubDirectory(action);
        String fileName = renameFile(image, entityId, action);
        String imageUrl = appUrl + "/download/" + fileName;

        switch (action) {
            case CATEGORIES:
                if (isImageValid(image, fileName)) {
                    imageUrl += "?category=" + entityId;
                    CategoryDTO category = categoryService.findById(entityId);
                    if (category.getImageUrl() != null && !category.getImageUrl().isEmpty()) {
                        deleteFile(
                                resolveFilenameFromUrl(category.getImageUrl()),
                                Action.CATEGORIES);
                    }
                    categoryService.updateImageUrl(entityId, imageUrl);
                }
                break;
            case RECIPES:
                if (isImageValid(image, fileName)) {
                    imageUrl += "?recipe=" + entityId;
                    RecipeInfo recipe = recipeService.findById(entityId, email);
                    if (recipe.getImageUrl() != null && !recipe.getImageUrl().isEmpty()) {
                        deleteFile(resolveFilenameFromUrl(recipe.getImageUrl()),
                                Action.RECIPES);
                    }
                    recipeService.updateRecipeImageUrl(entityId, imageUrl);
                }
                break;
            case DEFAULT:
                throw new IllegalStateException("Unexpected value: " + action);
        }
        return storeFile(image, startLocation, fileName);
    }

    @Override
    public String resolveFilenameFromUrl(String imageUrl) {
        try {
            return imageUrl.substring(imageUrl.lastIndexOf("/") + 1, imageUrl.lastIndexOf("?"));
        } catch (Exception e) {
            throw new InvalidUrlFormatException("Image url is not valid");
        }
    }

    @Override
    public Resource loadFileAsResource(String fileName, Action action) {
        Path filePath;
        try {
            switch (action) {

                case CATEGORIES:
                    filePath = fileStorageLocation.resolve(CATEGORIES).resolve(fileName).normalize();
                    break;
                case RECIPES:
                    filePath = fileStorageLocation.resolve(RECIPES).resolve(fileName).normalize();
                    break;
                case PDF:
                    filePath = fileStorageLocation.resolve(PDF).resolve(fileName).normalize();
                    break;
                case DEFAULT:
                    filePath = fileStorageLocation.resolve(fileName).normalize();
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + action);
            }
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new FileStorageException("File not found " + fileName);
            }
        } catch (MalformedURLException e) {
            throw new FileStorageException("File not found " + fileName, e);
        }
    }

    @Override
    public void deleteFile(String fileName, Action action) {
        try {
            switch (action) {

                case CATEGORIES:
                    Files.deleteIfExists(fileStorageLocation.resolve(CATEGORIES).resolve(fileName).normalize());
                    break;
                case RECIPES:
                    Files.deleteIfExists(fileStorageLocation.resolve(RECIPES).resolve(fileName).normalize());
                    break;
                case PDF:
                    Files.deleteIfExists(fileStorageLocation.resolve(PDF).resolve(fileName).normalize());
                    break;
                case DEFAULT:
                    Files.deleteIfExists(fileStorageLocation.resolve(fileName).normalize());
                    break;
            }
//            Files.delete(fileStorageLocation.resolve(fileName).normalize());
        } catch (IOException e) {
            throw new FileStorageException("File couldn't be deleted", e);
        }
    }

    @Override
    public Path createSubDirectory(Action action) {
        try {
            Path subdirectoryPath;
            switch (action) {
                case CATEGORIES:
                    subdirectoryPath = this.fileStorageLocation.resolve(CATEGORIES);
                    break;
                case RECIPES:
                    subdirectoryPath = this.fileStorageLocation.resolve(RECIPES);
                    break;
                case PDF:
                    subdirectoryPath = this.fileStorageLocation.resolve(PDF);
                    break;
                case DEFAULT:
                    subdirectoryPath = this.fileStorageLocation;
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + action);
            }
            if (!Files.exists(subdirectoryPath)) {
                Files.createDirectories(subdirectoryPath);
                log.error("subdirectory created " + subdirectoryPath.toString());
            }
            return subdirectoryPath;
        } catch (IOException e) {
            e.printStackTrace();
            throw new FileStorageException("Error while creating subdirectory " + action);
        }
    }

    private String renameFile(MultipartFile file, int entityId, Action action) {
        switch (action) {
            case CATEGORIES:
                return CATEGORIES + "_" + entityId + "_" + StringUtils.trimAllWhitespace(StringUtils.cleanPath(file.getOriginalFilename()));
            case RECIPES:
                return RECIPES + "_" + entityId + "_" + StringUtils.trimAllWhitespace(StringUtils.cleanPath(file.getOriginalFilename()));
            default:
                throw new UnsupportedOperationException("Operation is not supported");
        }
    }

    private String storeFile(MultipartFile file, Path startLocation, String fileName) {
        try {
            isImageValid(file, fileName);
            Path targetLocation = startLocation.resolve(fileName);
            log.error("target location " + targetLocation.toString());
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            return fileName;

        } catch (IOException e) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", e);
        }
    }

    private boolean isImageValid(MultipartFile file, String fileName) {
        String extension = FilenameUtils.getExtension(fileName);

        if (file.isEmpty()) {
            throw new FileStorageException("failed to store empty file");
        }

        if (fileName.contains("..")) {
            throw new FileStorageException("Filename contains invalid path sequence " + fileName);
        }

        if (!extension.equalsIgnoreCase("jpg") && !extension.equalsIgnoreCase("jpeg")) {
            throw new FileStorageException("Image format is not supported, only JPG(JPEG) is. " + extension + " provided");
        }

        return true;
    }
}
