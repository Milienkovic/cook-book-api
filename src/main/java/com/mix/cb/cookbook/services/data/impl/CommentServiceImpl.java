package com.mix.cb.cookbook.services.data.impl;

import com.mix.cb.cookbook.models.entities.Comment;
import com.mix.cb.cookbook.models.entities.Recipe;
import com.mix.cb.cookbook.models.entities.User;
import com.mix.cb.cookbook.models.payloads.mutate_entities.comments.LikeCommentRequest;
import com.mix.cb.cookbook.models.payloads.mutate_entities.comments.ReportCommentRequest;
import com.mix.cb.cookbook.models.payloads.mutate_entities.comments.MutateComment;
import com.mix.cb.cookbook.models.payloads.mutate_entities.comments.CommentDTO;
import com.mix.cb.cookbook.repos.CommentRepo;
import com.mix.cb.cookbook.repos.RecipeRepo;
import com.mix.cb.cookbook.repos.UserRepo;
import com.mix.cb.cookbook.services.data.CommentService;
import com.mix.cb.cookbook.services.exceptions.ResourceNotFoundException;
import com.mix.cb.cookbook.services.exceptions.UnauthorizedOperationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class CommentServiceImpl implements CommentService {

    private final CommentRepo commentRepo;
    private final UserRepo userRepo;
    private final RecipeRepo recipeRepo;
    private ModelMapper modelMapper;

    public CommentServiceImpl(CommentRepo commentRepo, UserRepo userRepo, RecipeRepo recipeRepo) {
        this.commentRepo = commentRepo;
        this.userRepo = userRepo;
        this.recipeRepo = recipeRepo;
    }

    @Autowired
    public void setModelMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public List<CommentDTO> findAllByReportedAndUserId(boolean reported, int userId) {
        return mapComments(commentRepo.findAllByReportedAndUserId(reported, userId));
    }

    @Override
    public List<CommentDTO> findAll() {
        return mapComments(commentRepo.findAll());
    }

    @Override
    public List<CommentDTO> findAllReported(boolean reported) {
        return mapComments(commentRepo.findAllByReported(reported));
    }

    @Override
    public List<CommentDTO> findAllByRecipeId(boolean reported, int recipeId) {
        return mapComments(commentRepo.findAllByReportedAndRecipeId(reported, recipeId));
    }

    @Override
    public List<CommentDTO> findAllReplies(boolean reported, int commentId) {
        return mapComments(commentRepo.findAllByReportedAndCommentId(reported, commentId));
    }

    @Override
    public CommentDTO findById(int commentId) {
        Comment comment = getComment(commentId);
        if (comment.isReported()) {
            throw new ResourceNotFoundException(Comment.class.getSimpleName(), "ID", commentId);
        }
        return modelMapper.map(comment, CommentDTO.class);
    }

    @Override
    public CommentDTO createComment(String email, int recipeId, MutateComment mutateComment) {
        Comment comment = submitComment(email, recipeId, mutateComment);
        return modelMapper.map(comment, CommentDTO.class);
    }

    @Override
    public CommentDTO createReply(String email, int commentId, MutateComment mutateComment) {
        Comment comment = submitReply(email, commentId, mutateComment);
        return modelMapper.map(comment, CommentDTO.class);
    }

    @Override
    public void deleteComment(int commentId, String email) {
        User user = getUser(email);
        Comment comment = getComment(commentId);
        if (!user.getComments().contains(comment)) {
            throw new UnauthorizedOperationException("You dont have permission to perform this operation");
        }
        commentRepo.deleteById(commentId);
    }

    @Override
    public void deleteAllReported() {
        commentRepo.deleteAllByReported(true);
    }

    @Override
    public CommentDTO updateComment(int commentId, MutateComment mutateComment, String email) {
        User user = getUser(email);
        Comment comment = getComment(commentId);
        if (!user.getComments().contains(comment)) {
            throw new UnauthorizedOperationException("You dont have permission to perform this operation");
        }
        modelMapper.map(mutateComment, comment);
        return modelMapper.map(commentRepo.save(comment), CommentDTO.class);
    }

    @Override
    public void likeComment(int commentId, LikeCommentRequest likeCommentRequest) {
        Comment comment = getComment(commentId);
        if (likeCommentRequest.isLike()) {
            comment.setLikes(comment.getLikes() + 1);
        } else {
            comment.setDislikes(comment.getDislikes() + 1);
        }
        commentRepo.save(comment);
    }

    @Override
    public void reportComment(int commentId, ReportCommentRequest reportCommentRequest) {
        Comment comment = getComment(commentId);
        comment.setReported(true);
        comment.setReason(reportCommentRequest.getReason().reason());
        commentRepo.save(comment);
    }

    private List<CommentDTO> mapComments(Collection<Comment> comments) {
        return comments.stream()
                .map(comment -> modelMapper.map(comment, CommentDTO.class))
                .collect(Collectors.toList());
    }

    private Comment getComment(int commentId) {
        return commentRepo.findById(commentId)
                .orElseThrow(() -> new ResourceNotFoundException(Comment.class.getSimpleName(), "ID", commentId));
    }

    private Comment submitComment(String email, int recipeId, MutateComment mutateComment) {
        Comment comment = modelMapper.map(mutateComment, Comment.class);
        User user = getUser(email);
        Recipe recipe = getRecipe(recipeId);
        comment.setUser(user);
        comment.setRecipe(recipe);
        return commentRepo.save(comment);
    }

    private Recipe getRecipe(int recipeId) {
        return recipeRepo.findById(recipeId)
                .orElseThrow(() -> new ResourceNotFoundException(Recipe.class.getSimpleName(), "ID", recipeId));
    }

    private User getUser(String email) {
        return userRepo.findByEmail(email)
                .orElseThrow(() -> new ResourceNotFoundException(User.class.getSimpleName(), "email", email));
    }

    private Comment submitReply(String email, int commentId, MutateComment mutateComment) {
        Comment comment = getComment(commentId);
        User user = getUser(email);
        Comment reply = modelMapper.map(mutateComment, Comment.class);
        reply.setUser(user);
        reply.setRecipe(comment.getRecipe());
        reply.setComment(comment);
        return commentRepo.save(reply);
    }

    private Comment saveComment(String email, int recipeId, Comment comment, MutateComment mutateComment) {
        User user = getUser(email);
        if (comment.getId() == 0) {
            comment.setUser(user);
            Recipe recipe = getRecipe(recipeId);
            comment.setRecipe(recipe);
            return commentRepo.save(comment);
        } else {
            Comment reply = modelMapper.map(mutateComment, Comment.class);
            reply.setUser(user);
            reply.setRecipe(comment.getRecipe());
            reply.setComment(comment);
            return commentRepo.save(reply);
        }
    }
}
