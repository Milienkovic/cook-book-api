package com.mix.cb.cookbook.controllers.exceptionHandlers;

import com.mix.cb.cookbook.models.payloads.errors.ErrorResponse;
import com.mix.cb.cookbook.services.exceptions.*;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;

@ControllerAdvice
@Slf4j
public class ResourceExceptionHandler {

    private HttpServletRequest request;
    private MessageSource messageSource;

    @Autowired
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    //400
    @ExceptionHandler({FileStorageException.class, InvalidTokenException.class, InvalidPasswordException.class, InvalidJwtAuthenticationException.class})
    public ResponseEntity<ErrorResponse> handleFileStorageException(RuntimeException e) {
        return composeErrorResponseEntity(HttpStatus.BAD_REQUEST, e.getMessage());
    }

    //404
    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleResourceNotFoundException(ResourceNotFoundException e) {
        log.error(e.getMessage());
        return composeErrorResponseEntity(HttpStatus.NOT_FOUND, messageSource.getMessage("message.error.resourceNotFound", e.getArgs(), request.getLocale()));
    }

    //405
    @ExceptionHandler({UnauthorizedOperationException.class})
    public ResponseEntity<ErrorResponse> handleUnauthorizedOperationException(UnauthorizedOperationException e) {
        log.error(e.getMessage());
        return composeErrorResponseEntity(HttpStatus.METHOD_NOT_ALLOWED,
                messageSource.getMessage("message.error.unauthorizedOperation", null, request.getLocale()));
    }

    //405
    @ExceptionHandler(UnauthorizedAccessException.class)
    public ResponseEntity<ErrorResponse> handleUnauthorizedAccessException(UnauthorizedAccessException e) {
        log.error(e.getMessage());
        return composeErrorResponseEntity(HttpStatus.METHOD_NOT_ALLOWED,
                messageSource.getMessage("message.error.unauthorizedOperation", null, request.getLocale()));
    }

    //409
    @ExceptionHandler({EmailExistsException.class})
    public ResponseEntity<ErrorResponse> handleEmailExistsException(EmailExistsException e) {
        return composeErrorResponseEntity(HttpStatus.CONFLICT, e.getMessage());
    }

    @ExceptionHandler({ResourceExistsException.class})
    public ResponseEntity<ErrorResponse> handleResourceExistsException(ResourceExistsException e) {
        return composeErrorResponseEntity(HttpStatus.CONFLICT, messageSource.getMessage("message.error.resourceExistsAlready", e.getArgs(), request.getLocale()));
    }

    private ResponseEntity<ErrorResponse> composeErrorResponseEntity(HttpStatus status, String message) {
        ErrorResponse response = composeErrorResponse(status, message);
        return ResponseEntity
                .status(status)
                .body(response);
    }

    @NotNull
    private ErrorResponse composeErrorResponse(HttpStatus status, String message) {
        ErrorResponse response = new ErrorResponse();
        response.setStatus(status.value());
        response.setError(status.getReasonPhrase());
        response.setMessage(message);
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
        response.setPath(request.getServletPath());
        return response;
    }
}


