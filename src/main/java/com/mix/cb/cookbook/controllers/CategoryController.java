package com.mix.cb.cookbook.controllers;

import com.mix.cb.cookbook.models.payloads.common.MultipleDeleteRequest;
import com.mix.cb.cookbook.models.payloads.common.ResponseMessage;
import com.mix.cb.cookbook.models.payloads.mutate_entities.categories.CategoryDTO;
import com.mix.cb.cookbook.models.payloads.mutate_entities.categories.CategoryInfo;
import com.mix.cb.cookbook.models.payloads.mutate_entities.categories.MutateCategory;
import com.mix.cb.cookbook.services.data.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URI;
import java.security.Principal;
import java.util.List;

import static com.mix.cb.cookbook.utils.URLs.composeAppUrl;

@Slf4j
@RestController
public class CategoryController {

    private final CategoryService categoryService;
    private MessageSource messageSource;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @GetMapping(value = "/categories")
    public ResponseEntity<List<CategoryInfo>> findAll(Principal principal) {
        return ResponseEntity.ok(categoryService.findAll(principal));
    }

    @GetMapping(value = "/categories", params = "id")
    public ResponseEntity<CategoryDTO> findById(@RequestParam("id") int categoryId) {
        return ResponseEntity.ok(categoryService.findById(categoryId));
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @DeleteMapping(value = "/categories", params = "id")
    public ResponseEntity<ResponseMessage> deleteById(@RequestParam("id") int categoryId, HttpServletRequest request) {
        categoryService.deleteById(categoryId);
        String message = messageSource.getMessage("message.categoryDeleted", null, request.getLocale());
        return ResponseEntity.ok(ResponseMessage.builder()
                .message(message)
                .isSuccess(true)
                .build());
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping(value = "/categories")
    public ResponseEntity<CategoryDTO> createCategory(@Valid @RequestBody MutateCategory category,
                                                      HttpServletRequest request) {
        CategoryDTO persistedCategory = categoryService.createCategory(category, composeAppUrl(request));

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequestUri()
                .queryParam("id", persistedCategory.getId())
                .buildAndExpand(persistedCategory.getId())
                .toUri();
        return ResponseEntity.created(location).body(persistedCategory);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PutMapping(value = "/categories", params = "id")
    public ResponseEntity<CategoryDTO> updateCategory(@RequestParam("id") int categoryId, @Valid @RequestBody MutateCategory category) {
        return ResponseEntity.ok(categoryService.updateCategory(categoryId, category));
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @DeleteMapping("/categories/all")
    public ResponseEntity<ResponseMessage> deleteAll(@RequestBody @Valid MultipleDeleteRequest multipleDeleteRequest, HttpServletRequest request) {

        String message = messageSource.getMessage("message.categoriesDeleted", null, request.getLocale());
        categoryService.deleteAll(multipleDeleteRequest);
        return ResponseEntity.ok(ResponseMessage.builder()
                .message(message)
                .isSuccess(true)
                .build());
    }
}
