package com.mix.cb.cookbook.controllers.user;

import com.mix.cb.cookbook.models.payloads.common.ResponseMessage;
import com.mix.cb.cookbook.services.data.FavoritesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@RestController
public class FavoritesController {

    private final FavoritesService favoritesService;

    private MessageSource messageSource;

    public FavoritesController(FavoritesService favoritesService) {
        this.favoritesService = favoritesService;
    }

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping(value = "/favorites/add", params = "recipe")
    public ResponseEntity<?> addFavoriteRecipe(Principal principal,
                                               @RequestParam("recipe") int recipeId,
                                               HttpServletRequest request) {
        favoritesService.addFavoriteRecipe(principal.getName(), recipeId);
        return ResponseEntity.ok(ResponseMessage.builder()
                .message(messageSource.getMessage("message.favorites.added", null, request.getLocale()))
                .isSuccess(true)
                .build());
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping(value = "/favorites/remove", params = "recipe")
    public ResponseEntity<?> removeRecipe(Principal principal,
                                          @RequestParam("recipe") int recipeId,
                                          HttpServletRequest request) {
        favoritesService.removeRecipe(principal.getName(), recipeId);
        return ResponseEntity.ok(ResponseMessage.builder()
                .message(messageSource.getMessage("message.favorites.removed", null, request.getLocale()))
                .isSuccess(true)
                .build());
    }
}
