package com.mix.cb.cookbook.controllers;

import com.mix.cb.cookbook.models.payloads.common.ResponseMessage;
import com.mix.cb.cookbook.models.payloads.mutate_entities.comments.LikeCommentRequest;
import com.mix.cb.cookbook.models.payloads.mutate_entities.comments.ReportCommentRequest;
import com.mix.cb.cookbook.models.payloads.mutate_entities.comments.MutateComment;
import com.mix.cb.cookbook.models.payloads.mutate_entities.comments.CommentDTO;
import com.mix.cb.cookbook.services.data.CommentService;
import com.mix.cb.cookbook.utils.URLs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;

@RestController
public class CommentController {

    private final CommentService commentService;

    private MessageSource messageSource;

    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @GetMapping("/comments")
    public ResponseEntity<?> getComments() {
        return ResponseEntity.ok(commentService.findAll());
    }

    @GetMapping(value = "/comments/replies", params = "comment")
    public ResponseEntity<?> getCommentReplies(@RequestParam("comment") int commentId) {
        return ResponseEntity.ok(commentService.findAllReplies(false, commentId));
    }

    @GetMapping(value = "/comments/recipe", params = "recipe")
    public ResponseEntity<?> getRecipeComments(@RequestParam("recipe") int recipeId) {
        return ResponseEntity.ok(commentService.findAllByRecipeId(false, recipeId));
    }

    @GetMapping(value = "/comments", params = "id")
    public ResponseEntity<?> getComment(@RequestParam int id) {
        return ResponseEntity.ok(commentService.findById(id));
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping(value = "/comments", params = {"recipe"})
    public ResponseEntity<?> commentRecipe(Principal principal,
                                           @RequestParam("recipe") int recipeId,
                                           @RequestBody @Valid MutateComment mutateComment) {
        CommentDTO persistedComment = commentService.createComment(principal.getName(), recipeId, mutateComment);
        return ResponseEntity.created(ServletUriComponentsBuilder
                .fromCurrentRequestUri()
                .queryParam("id", persistedComment.getId())
                .buildAndExpand(persistedComment.getId())
                .toUri()).body(persistedComment);
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping(value = "/replies", params = {"comment"})
    public ResponseEntity<?> replyToComment(Principal principal,
                                            @RequestParam("comment") int commentId,
                                            @RequestBody @Valid MutateComment mutateComment,
                                            HttpServletRequest request) {
        CommentDTO persistedReply = commentService.createReply(principal.getName(), commentId, mutateComment);
        return ResponseEntity.created(URLs.composeReplyLocationUri(persistedReply, request)).body(persistedReply);
    }

    @PreAuthorize("isAuthenticated()")
    @PutMapping(value = "/comments", params = "id")
    public ResponseEntity<?> updateComment(@RequestParam("id") int commentId,
                                           @RequestBody @Valid MutateComment mutateComment,
                                           Principal principal) {
        CommentDTO persistedComment = commentService.updateComment(commentId, mutateComment, principal.getName());
        return ResponseEntity.ok(persistedComment);
    }

    @PreAuthorize("isAuthenticated()")
    @DeleteMapping(value = "/comments", params = "id")
    public ResponseEntity<?> deleteComment(@RequestParam int id,
                                           HttpServletRequest request,
                                           Principal principal) {
        commentService.deleteComment(id, principal.getName());
        return ResponseEntity.ok(ResponseMessage.builder()
                .message(messageSource.getMessage("message.commentDeleted", null, request.getLocale()))
                .isSuccess(true)
                .build());
    }

    @PreAuthorize("isAuthenticated()")
    @PutMapping(value = "/comments/like", params = "comment")
    public ResponseEntity<?> likeComment(@RequestParam("comment") int commentId,
                                         @RequestBody @Valid LikeCommentRequest likeCommentRequest,
                                         HttpServletRequest request) {

        String message = likeCommentRequest.isLike() ?
                messageSource.getMessage("message.commentLiked", null, request.getLocale()) :
                messageSource.getMessage("message.commentDisliked", null, request.getLocale());

        commentService.likeComment(commentId, likeCommentRequest);
        return ResponseEntity.ok(ResponseMessage.builder()
                .message(message)
                .isSuccess(true)
                .build());
    }

    @PreAuthorize("isAuthenticated()")
    @PutMapping(value = "/comments/report", params = "comment")
    public ResponseEntity<?> reportComment(@RequestParam("comment") int commentId,
                                           @RequestBody @Valid ReportCommentRequest reportCommentRequest,
                                           HttpServletRequest request) {
        commentService.reportComment(commentId, reportCommentRequest);
        return ResponseEntity.ok(ResponseMessage.builder()
                .message(messageSource.getMessage("message.commentReported", null, request.getLocale()))
                .isSuccess(true)
                .build());
    }
}
