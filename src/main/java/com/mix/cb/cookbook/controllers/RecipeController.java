package com.mix.cb.cookbook.controllers;

import com.mix.cb.cookbook.models.enums.Complexity;
import com.mix.cb.cookbook.models.payloads.common.MultipleDeleteRequest;
import com.mix.cb.cookbook.models.payloads.common.ResponseMessage;
import com.mix.cb.cookbook.models.payloads.mutate_entities.recipes.*;
import com.mix.cb.cookbook.services.data.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URI;
import java.security.Principal;
import java.util.List;

@RestController
public class RecipeController {

    private final RecipeService recipeService;
    private MessageSource messageSource;

    public RecipeController(RecipeService recipeService) {
        this.recipeService = recipeService;
    }

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @GetMapping(value = "/recipes", params = "id")
    @PreAuthorize("@recipeServiceImpl.isRecipeAccessible(#recipeId, #principal)")
    public ResponseEntity<?> findById(@RequestParam("id") int recipeId,
                                      Principal principal) {
        return ResponseEntity.ok(recipeService.findById(recipeId, principal == null ? null : principal.getName()));
    }

    @GetMapping(value = "/recipes")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<List<RecipeInfo>> findAll() {
        return ResponseEntity.ok(recipeService.findAll());
    }

    @GetMapping(value = "/recipes/mine")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<List<RecipeInfo>> findAllByUser(Principal principal) {
        return ResponseEntity.ok(recipeService.findAllByUser(principal.getName()));
    }

    @GetMapping(value = "/recipes/mine", params = "category")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<List<RecipeInfo>> findAllByUserAndCategory(@RequestParam("category") int categoryId,
                                                                    Principal principal) {
        return ResponseEntity.ok(recipeService.findAllByUserAndCategory(categoryId, principal.getName()));
    }

    @GetMapping(value = "/recipes", params = "category")
    public ResponseEntity<List<RecipeInfo>> findAllByCategoryId(@RequestParam("category") int categoryId,
                                                               Principal principal) {
        return ResponseEntity.ok(recipeService.findAllByCategoryId(categoryId, principal == null ? null : principal.getName()));
    }

    @GetMapping(value = "/recipes", params = {"category", "complexity"})
    public ResponseEntity<List<RecipeInfo>> findAllInCategoryByComplexity(@RequestParam("category") int categoryId,
                                                                         @RequestParam Complexity complexity,
                                                                         Principal principal) {
        return ResponseEntity.ok(recipeService.findAllByCategoryIdAndComplexity(categoryId, complexity, principal == null ? null : principal.getName()));
    }

    @DeleteMapping(value = "/recipes", params = "id")
    @PreAuthorize("isAuthenticated() and @recipeServiceImpl.isMineRecipe(#recipeId, #principal.name)")
    public ResponseEntity<?> deleteById(Principal principal,
                                        @RequestParam("id") int recipeId,
                                        HttpServletRequest request) {
        recipeService.deleteById(recipeId, principal.getName());
        String message = messageSource.getMessage("message.recipeDeleted", null, request.getLocale());
        return ResponseEntity.ok(ResponseMessage.builder()
                .message(message)
                .isSuccess(true)
                .build());
    }

    @DeleteMapping("/recipes/all")
    @PreAuthorize("isAuthenticated() and @recipeServiceImpl.areRecipesMine(#deleteRequest.indices, #principal.name)")
    public ResponseEntity<?> deleteAll(Principal principal,
                                       @RequestBody @Valid MultipleDeleteRequest deleteRequest,
                                       HttpServletRequest request) {
        recipeService.deleteAll(principal.getName(), deleteRequest);
        String message = messageSource.getMessage("message.recipesDeleted", null, request.getLocale());
        return ResponseEntity.ok(ResponseMessage.builder()
                .message(message)
                .isSuccess(true)
                .build());
    }

    @PostMapping(value = "/recipes", params = {"category"})
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<RecipeInfo> createRecipe(Principal principal,
                                                  @RequestParam("category") int categoryId,
                                                  @RequestBody @Valid MutateRecipe recipe) {
        RecipeInfo persistedRecipe = recipeService.createRecipe(recipe, categoryId, principal.getName());
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequestUri()
                .queryParam("id", persistedRecipe.getId())
                .buildAndExpand(persistedRecipe.getId())
                .toUri();
        return ResponseEntity.created(location).body(persistedRecipe);
    }

    @PutMapping(value = "/recipes", params = "id")
    @PreAuthorize("isAuthenticated() and @recipeServiceImpl.isMineRecipe(#recipeId, #principal.name)")
    public ResponseEntity<RecipeInfo> updateRecipe(@RequestParam("id") int recipeId,
                                                  @RequestBody @Valid MutateRecipe recipe,
                                                  Principal principal) {
        return ResponseEntity.ok(recipeService.updateRecipe(recipeId, recipe, principal.getName()));
    }

    @PutMapping(value = "/recipes/rate", params = "id")
    public ResponseEntity<?> rateRecipe(@RequestParam("id") int recipeId,
                                        @RequestBody @Valid RateRecipeRequest rateRecipeRequest,
                                        HttpServletRequest request) {
        String message = messageSource.getMessage("message.recipeRated", null, request.getLocale());
        recipeService.rateRecipe(recipeId, rateRecipeRequest);
        return ResponseEntity.ok(ResponseMessage.builder()
                .message(message)
                .isSuccess(true)
                .build());
    }
}
