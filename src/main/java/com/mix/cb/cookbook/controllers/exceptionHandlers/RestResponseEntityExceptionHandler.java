package com.mix.cb.cookbook.controllers.exceptionHandlers;

import com.mix.cb.cookbook.models.payloads.errors.ErrorResponse;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ElementKind;
import java.sql.Timestamp;
import java.util.*;

@Slf4j
@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    private MessageSource messageSource;

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @NotNull
    @Override
    protected ResponseEntity<Object> handleBindException(BindException ex, @NotNull HttpHeaders headers, HttpStatus status, WebRequest request) {
        ErrorResponse response = createErrorsResponse(ex, request, status);

        return handleExceptionInternal(ex, response, headers, status, request);
    }

    @NotNull
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ErrorResponse response = createErrorsResponse(ex.getBindingResult(), request, status);
        return handleExceptionInternal(ex, response, headers, status, request);
    }

    private ErrorResponse createErrorsResponse(BindingResult result, WebRequest request, HttpStatus status) {
        ErrorResponse response = new ErrorResponse();
        Map<String, String> fieldErrors = new HashMap<>();
        for (FieldError error : result.getFieldErrors()) {
            fieldErrors.put(error.getField(), error.getDefaultMessage());
        }

        for (ObjectError error : result.getGlobalErrors()) {
            fieldErrors.put(error.getObjectName(), error.getDefaultMessage());
        }

        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
        response.setErrors(fieldErrors);
        response.setPath(((ServletWebRequest) request).getRequest().getServletPath());
        response.setStatus(status.value());
        response.setError(status.name());
        if (result.hasGlobalErrors()) {
            response.setMessage(result.getGlobalError().getDefaultMessage());
        }
        return response;
    }


    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> handle(ConstraintViolationException constraintViolationException, WebRequest request) {
        ErrorResponse errorResponse = composeConstraintViolationErrorResponse(constraintViolationException, request);
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    private ErrorResponse composeConstraintViolationErrorResponse(
            ConstraintViolationException constraintViolationException,
            WebRequest request) {

        Set<ConstraintViolation<?>> violations = constraintViolationException.getConstraintViolations();
        Map<Integer, List<Message>> fieldErrors = new HashMap<>();

        if (!violations.isEmpty()) {
            for (ConstraintViolation<?> constraintViolation : violations) {
                constraintViolation.getPropertyPath().forEach(node -> {
                            if (node.getKind().equals(ElementKind.PROPERTY)) {
                                if (fieldErrors.containsKey(node.getIndex())) {
                                    fieldErrors.get(node.getIndex()).add(new Message(node.getName(), constraintViolation.getMessage()));
                                } else {
                                    List<Message> m = new ArrayList<>();
                                    m.add(new Message(node.getName(), constraintViolation.getMessage()));
                                    fieldErrors.put(node.getIndex(), m);
                                }
                            }
                        }
                );
            }
            log.error(fieldErrors.toString());
        }

        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setPath(((ServletWebRequest) request).getRequest().getServletPath());
        errorResponse.setStatus(HttpStatus.BAD_REQUEST.value());
        errorResponse.setError(HttpStatus.BAD_REQUEST.getReasonPhrase());
        errorResponse.setMessage(messageSource.getMessage("constraint.violation", null, request.getLocale()));
        errorResponse.setTimestamp(new Timestamp(System.currentTimeMillis()));
        errorResponse.setErrors(fieldErrors);
        return errorResponse;
    }

    @Data
    private static class Message {
        private final String field;
        private final String message;

        public Message(String field, String message) {
            this.field = field;
            this.message = message;
        }

        @Override
        public String toString() {
            return
                    field + ':' +
                            message;
        }
    }
}

