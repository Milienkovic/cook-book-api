package com.mix.cb.cookbook.controllers;

import com.mix.cb.cookbook.models.payloads.auth.data.AuthToken;
import com.mix.cb.cookbook.models.payloads.auth.data.LoginRequest;
import com.mix.cb.cookbook.models.payloads.common.ResponseMessage;
import com.mix.cb.cookbook.models.payloads.events.NewVerificationTokenEventData;
import com.mix.cb.cookbook.models.payloads.events.PasswordResetEventData;
import com.mix.cb.cookbook.models.payloads.events.RegistrationEventData;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.ChangePasswordRequest;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.CurrentUserPayload;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.UserRegistrationRequest;
import com.mix.cb.cookbook.services.data.UserService;
import com.mix.cb.cookbook.services.security.auth.AuthService;
import com.mix.cb.cookbook.services.security.events.OnNewVerificationTokenRequestEvent;
import com.mix.cb.cookbook.services.security.events.OnPasswordResetRequestEvent;
import com.mix.cb.cookbook.services.security.events.OnRegistrationCompleteEvent;
import com.mix.cb.cookbook.utils.UrlProps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.net.URI;

import static com.mix.cb.cookbook.utils.URLs.composeUserLocationUri;

@Slf4j
@RestController
public class AuthController {
    private final UserService userService;
    private final ApplicationEventPublisher eventPublisher;
    private final AuthService authService;
    private MessageSource messageSource;
    private final String serverUrl;

    public AuthController(UserService userService, ApplicationEventPublisher eventPublisher, AuthService authService, UrlProps urlProps) {
        this.userService = userService;
        this.eventPublisher = eventPublisher;
        this.authService = authService;
        this.serverUrl = urlProps.getUrl();
    }

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @PostMapping(value = "/registration")
    @PreAuthorize("isAnonymous()")
    //aop mail sending, args order important!
    //aop not working atm
    public ResponseEntity<?> registerUser(@RequestBody @Valid UserRegistrationRequest user,
                                          HttpServletRequest request) {
        CurrentUserPayload persistedUser = userService.createUser(user);
        eventPublisher.publishEvent(new OnRegistrationCompleteEvent(new RegistrationEventData(persistedUser, serverUrl, request.getLocale())));
        URI location = composeUserLocationUri(persistedUser, request);
        String message = messageSource.getMessage("message.regSuccess", new String[]{persistedUser.getEmail()}, request.getLocale());
        return ResponseEntity.created(location)
                .body(ResponseMessage.builder()
                        .message(message)
                        .isSuccess(true)
                        .build());
    }

    @PostMapping(value = "/registration/confirm", params = "token")
//    @PreAuthorize("isAnonymous()")
    public ResponseEntity<?> confirmRegistration(@RequestParam String token,
                                                 HttpServletRequest request) {
        ResponseMessage responseMessage = authService.confirmRegistration(token, request.getLocale());
        return ResponseEntity.ok(responseMessage);
    }

    @PostMapping(value = "/registration/resendRegistrationToken", params = {"email"})
//    @PreAuthorize("isAnonymous()")
    public ResponseEntity<?> resendRegistrationToken(@RequestParam String email,
                                                     HttpServletRequest request) {
        NewVerificationTokenEventData data = new NewVerificationTokenEventData(request.getLocale(), serverUrl, email);
        eventPublisher.publishEvent(new OnNewVerificationTokenRequestEvent(data));
        return ResponseEntity.ok(ResponseMessage.builder()
                .message(messageSource.getMessage("message.resendRegistrationToken", null, request.getLocale()))
                .isSuccess(true)
                .build());
    }


    @PostMapping(value = "/resetPassword", params = "email")
    public ResponseEntity<?> resetPassword(@RequestParam String email, HttpServletRequest request) {
//        String appUrl = composeAppUrl(request);
        CurrentUserPayload user = userService.findByEmail(email);
        PasswordResetEventData data = new PasswordResetEventData(request.getLocale(), serverUrl, user);
        eventPublisher.publishEvent(new OnPasswordResetRequestEvent(data));
        String message = messageSource.getMessage("message.resetPasswordEmail", null, request.getLocale());
        return ResponseEntity.ok(ResponseMessage.builder()
                .isSuccess(true)
                .message(message)
                .build());
    }

    @PostMapping(value = "/changePassword", params = {"id", "token"})
    public ResponseEntity<?> grantChangePasswordPermission(@RequestParam("id") int userId,
                                                           @RequestParam String token,
                                                           HttpServletRequest request,
                                                           HttpServletResponse response) {
        AuthToken authToken = authService.authUserWithoutPasswordOnChangePasswordRequest(userId, token);
        HttpHeaders headers = setAuthHeader(authToken);
        response.addCookie(authToken.getRefreshTokenCookie());
        String message = messageSource.getMessage("message.changePasswordPrivilegesSuc", null, request.getLocale());
        return ResponseEntity.status(HttpStatus.OK)
                .headers(headers)
                .body(ResponseMessage.builder()
                        .message(message)
                        .isSuccess(true)
                        .build());
    }

    @PostMapping(value = "/updatePassword", params = "token")
    @PreAuthorize("hasAuthority('CHANGE_PASSWORD_PRIVILEGE')")
    public ResponseEntity<?> updatePassword(@RequestBody @Valid ChangePasswordRequest passwordRequest,
                                            @RequestParam String token,
                                            HttpServletRequest request) {
        log.error(String.valueOf(request.getHeaderNames()));
        log.error(request.getHeader(HttpHeaders.AUTHORIZATION));
        ResponseMessage message = authService.confirmPasswordUpdate(SecurityContextHolder.getContext().getAuthentication().getName(),
                passwordRequest, token, request.getLocale());
        return ResponseEntity.ok(message);
    }

    @PostMapping("/login")
    @PreAuthorize("isAnonymous()")
    public ResponseEntity<?> login(@RequestBody @Valid LoginRequest loginRequest,
                                   HttpServletRequest request,
                                   HttpServletResponse response) {
        AuthToken authToken = authService.authUserWithEmailAndPassword(loginRequest);
        HttpHeaders headers = setAuthHeader(authToken);
        response.addCookie(authToken.getRefreshTokenCookie());
        String message = messageSource.getMessage("message.login.notification", null, request.getLocale());
        return ResponseEntity.status(HttpStatus.OK)
                .headers(headers)
                .body(ResponseMessage.builder()
                        .message(message)
                        .isSuccess(true)
                        .build());
    }

    @PostMapping(value = "/autoLogin", params = "token")
    @PreAuthorize("isAnonymous()")
    public ResponseEntity<?> autoLogin(@RequestParam String token,
                                       HttpServletRequest request,
                                       HttpServletResponse response) {
        AuthToken authToken = authService.authUserWithoutPassword(token);
        HttpHeaders headers = setAuthHeader(authToken);
        response.addCookie(authToken.getRefreshTokenCookie());

        String message = messageSource.getMessage("message.autoLogin.notification", null, request.getLocale());
        return ResponseEntity.status(HttpStatus.OK)
                .headers(headers)
                .body(ResponseMessage.builder()
                        .message(message)
                        .isSuccess(true)
                        .build());
    }

    @PostMapping(value = "/exchangeToken", params = {"token"})
    public ResponseEntity<ResponseMessage> extendLogin(
            @RequestParam("token") String refreshToken,
            HttpServletRequest request,
            HttpServletResponse response) {
        AuthToken authToken = authService.authUserOnRefreshedAuthToken(refreshToken);
        HttpHeaders headers = setAuthHeader(authToken);
        if (authToken.getRefreshTokenCookie() != null) {
            response.addCookie(authToken.getRefreshTokenCookie());
        }
        String message = messageSource.getMessage("message.auth.notification.newToken", null, request.getLocale());
        return ResponseEntity
                .status(HttpStatus.OK)
                .headers(headers)
                .body(ResponseMessage
                        .builder()
                        .message(message)
                        .isSuccess(true)
                        .build());
    }

    @PostMapping(value = "/exchangePrivilegeToken", params = "token")
    public ResponseEntity<ResponseMessage> extendLoginWithPasswordPrivilege(@RequestParam String token,
                                                                            HttpServletRequest request,
                                                                            HttpServletResponse response) {
        AuthToken authToken = authService.authWithResetTokenAndChangePasswordPrivilege(token);
        HttpHeaders headers = setAuthHeader(authToken);
        if (authToken.getRefreshTokenCookie() != null) {
            response.addCookie(authToken.getRefreshTokenCookie());
        }
        return ResponseEntity
                .status(HttpStatus.OK)
                .headers(headers)
                .body(ResponseMessage.builder()
                        .isSuccess(true)
                        .message(messageSource.getMessage("message.auth.notification.newToken", null, request.getLocale()))
                        .build());

    }

    private HttpHeaders setAuthHeader(AuthToken authToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, authToken.getAuthToken());
        return headers;
    }
}
