package com.mix.cb.cookbook.controllers;

import com.mix.cb.cookbook.models.payloads.common.MultipleDeleteRequest;
import com.mix.cb.cookbook.models.payloads.common.ResponseMessage;
import com.mix.cb.cookbook.models.payloads.mutate_entities.ingredients.IngredientDTO;
import com.mix.cb.cookbook.models.payloads.mutate_entities.ingredients.MutateIngredient;
import com.mix.cb.cookbook.models.payloads.mutate_entities.ingredients.UpdateIngredient;
import com.mix.cb.cookbook.services.data.IngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.net.URI;
import java.security.Principal;
import java.util.List;

@Validated
@RestController
public class IngredientController {

    private final IngredientService ingredientService;
    private MessageSource messageSource;

    public IngredientController(IngredientService ingredientService) {
        this.ingredientService = ingredientService;
    }

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @GetMapping(value = "/ingredients", params = "recipe")
    @PreAuthorize("@ingredientServiceImpl.isRecipeAccessible(#recipeId, #principal)")
    public ResponseEntity<List<IngredientDTO>> findAllByRecipeId(@RequestParam("recipe") int recipeId,
                                                                 Principal principal) {
        return ResponseEntity.ok(ingredientService.findAllByRecipeId(recipeId));
    }

    @GetMapping(value = "/ingredients", params = "id")
    @PreAuthorize("@ingredientServiceImpl.isIngredientAccessible(#ingredientId, #principal)")
    public ResponseEntity<IngredientDTO> findById(@RequestParam("id") int ingredientId,
                                                  Principal principal) {
        return ResponseEntity.ok(ingredientService.findById(ingredientId));
    }

    @DeleteMapping(value = "/ingredients", params = "id")
    @PreAuthorize("isAuthenticated() and @ingredientServiceImpl.isIngredientMine(#ingredientId, #principal)")
    public ResponseEntity<?> deleteById(@RequestParam("id") int ingredientId,
                                        Principal principal,
                                        HttpServletRequest request) {
        ingredientService.deleteById(ingredientId, principal.getName());
        return ResponseEntity.ok(ResponseMessage.builder()
                .message(messageSource.getMessage("message.ingredientDeleted", null, request.getLocale()))
                .isSuccess(true)
                .build());
    }

    @PostMapping(value = "/ingredients", params = "recipe")
    @PreAuthorize("isAuthenticated() and @ingredientServiceImpl.isRecipeAccessible(#recipeId, #principal)")
    public ResponseEntity<IngredientDTO> createIngredient(@RequestParam("recipe") int recipeId,
                                                          @Valid @RequestBody MutateIngredient ingredient,
                                                          Principal principal) {
        IngredientDTO persistedIngredient = ingredientService.createIngredient(recipeId, ingredient, principal.getName());
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequestUri()
                .queryParam("id", persistedIngredient.getId())
                .buildAndExpand(persistedIngredient.getId())
                .toUri();
        return ResponseEntity.created(location).body(persistedIngredient);
    }

    @PutMapping(value = "/ingredients", params = "id")
    @PreAuthorize("isAuthenticated() and @ingredientServiceImpl.isIngredientMine(#ingredientId, #principal)")
    public ResponseEntity<IngredientDTO> updateIngredient(@RequestParam("id") int ingredientId,
                                                          @Valid @RequestBody MutateIngredient ingredient,
                                                          Principal principal) {
        return ResponseEntity.ok(ingredientService.updateIngredient(ingredientId, ingredient, principal.getName()));
    }

    @PutMapping(value = "/ingredients/all", params = "recipe")
    @PreAuthorize("isAuthenticated() and @ingredientServiceImpl.areIngredientsMine(#recipeId, #ingredients, #principal)")
    public ResponseEntity<ResponseMessage> updateIngredients(@RequestParam("recipe") int recipeId,
                                                             @Valid @RequestBody
                                                             @NotEmpty List<@Valid UpdateIngredient> ingredients,
                                                             Principal principal,
                                                             HttpServletRequest request) {
        ingredientService.updateIngredients(recipeId, ingredients);
        return ResponseEntity.ok(ResponseMessage.builder()
                .message(messageSource.getMessage("message.ingredientsUpdated", null, request.getLocale()))
                .isSuccess(true)
                .build());
    }

    @PostMapping(value = "/ingredients/all", params = "recipe")
    @PreAuthorize("isAuthenticated() and @ingredientServiceImpl.isRecipeMine(#recipeId, #principal)")
    public ResponseEntity<ResponseMessage> saveIngredients(@RequestParam("recipe") int recipeId,
                                                           @Valid
                                                           @RequestBody
                                                           @NotEmpty
                                                                   List<@Valid MutateIngredient> ingredients,
                                                           Principal principal,
                                                           HttpServletRequest request) {
        ingredientService.saveRecipeIngredients(recipeId, ingredients);
        return ResponseEntity.ok(ResponseMessage.builder()
                .message(messageSource.getMessage("message.ingredientsSaved", null, request.getLocale()))
                .isSuccess(true)
                .build());
    }

    @DeleteMapping(value = "/ingredients/all", params = "recipe")
    @PreAuthorize("isAuthenticated() and @ingredientServiceImpl.areIngredientsMine(#recipeId, #deleteRequest.indices, #principal)")
    public ResponseEntity<ResponseMessage> deleteIngredients(@RequestParam("recipe") int recipeId,
                                                             @Valid @RequestBody MultipleDeleteRequest deleteRequest,
                                                             Principal principal,
                                                             HttpServletRequest request) {
        ingredientService.deleteRecipeIngredients(deleteRequest);
        return ResponseEntity.ok(ResponseMessage.builder()
                .message(messageSource.getMessage("message.ingredientsDeleted", null, request.getLocale()))
                .isSuccess(true)
                .build());
    }
}
