package com.mix.cb.cookbook.controllers;

import com.mix.cb.cookbook.models.payloads.files.UploadFileResponse;
import com.mix.cb.cookbook.services.files.FileStorageService;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.security.Principal;

import static com.mix.cb.cookbook.services.files.impl.FileStorageServiceImpl.Action.*;

@RestController
public class FileManagementController {
    // todo refactor
    private final FileStorageService fileStorageService;

    public FileManagementController(FileStorageService fileStorageService) {
        this.fileStorageService = fileStorageService;
    }


    @PreAuthorize("isAuthenticated()")
    @PostMapping(value = "/upload")
    public ResponseEntity<UploadFileResponse> uploadFile(@RequestParam("file") MultipartFile file,
                                                         @RequestParam(value = "category", required = false) Integer categoryId,
                                                         @RequestParam(value = "recipe", required = false) Integer recipeId,
                                                         Principal principal) {

        String appUrl = ServletUriComponentsBuilder.fromCurrentContextPath().toUriString();
        String fileName = "";
        String query = null;
        if (categoryId == null && recipeId == null) {
            fileName = fileStorageService.storeFile(file);

        }
        if (categoryId != null && recipeId == null) {
            fileName = fileStorageService.storeImage(file, categoryId, appUrl, principal.getName(), CATEGORIES);
            query = "category=" + categoryId;
        }
        if (categoryId == null && recipeId != null) {
            query = "recipe=" + recipeId;
            fileName = fileStorageService.storeImage(file, recipeId, appUrl, principal.getName(), RECIPES);
        }

        String fileDownloadUri = ServletUriComponentsBuilder
                .fromCurrentContextPath()
                .path("/download/")
                .path(fileName)
                .query(query)
                .toUriString();

        return ResponseEntity.ok()
                .header(HttpHeaders.LOCATION, fileDownloadUri)
                .body(UploadFileResponse.builder()
                        .fileName(fileName)
                        .downloadUrl(fileDownloadUri)
                        .fileType(file.getContentType())
                        .fileSize(file.getSize())
                        .build());
    }

    @GetMapping("/download/{fileName:.+}")
    public ResponseEntity<?> downloadFile(@PathVariable String fileName,
                                          @RequestParam(value = "category", required = false) Integer categoryId,
                                          @RequestParam(value = "recipe", required = false) Integer recipeId,
                                          HttpServletRequest request) {
        Resource resource = null;

        if (categoryId == null && recipeId == null) {
            resource = fileStorageService.loadFileAsResource(fileName, DEFAULT);

        }
        if (categoryId != null && recipeId == null) {
            resource = fileStorageService.loadFileAsResource(fileName, CATEGORIES);

        }
        if (categoryId == null && recipeId != null) {
            resource = fileStorageService.loadFileAsResource(fileName, RECIPES);
        }

        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (contentType == null) {
            contentType = MediaType.APPLICATION_OCTET_STREAM.getType();
        }
        return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
}
