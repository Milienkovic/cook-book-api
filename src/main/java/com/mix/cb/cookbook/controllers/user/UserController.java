package com.mix.cb.cookbook.controllers.user;

import com.mix.cb.cookbook.models.payloads.common.ResponseMessage;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.*;
import com.mix.cb.cookbook.services.data.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@Slf4j
@RestController
public class UserController {
    private final UserService userService;
    private MessageSource messageSource;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @PostMapping("/users")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<List<AdminUserPayload>> getUsers() {
        return ResponseEntity.ok(userService.findAll());
    }

    @PostMapping(value = "/users/me")
    @PreAuthorize("(hasAuthority('USER') or hasAuthority('ADMIN')) and @userServiceImpl.isMe(#userEmail.email,#principal.name)")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<UserInfo> getUser(@RequestBody @Valid UserEmail userEmail, Principal principal) {
        return ResponseEntity.ok(userService.findUserByEmail(userEmail.getEmail()));
    }

    @PostMapping(value = "/users/me", params = {"id"})
    @PreAuthorize("(hasAuthority('USER') or hasAuthority('ADMIN')) and @userServiceImpl.isMe(#userId,#principal.name)")
    public ResponseEntity<UserInfo> getUser(@RequestParam("id") int userId, Principal principal) {
        return ResponseEntity.ok(userService.findUserById(userId));
    }

    @DeleteMapping(value = "/users", params = "id")
    @PreAuthorize("(hasAuthority('USER') and @userServiceImpl.isMe(#userId, #principal.name)) or hasAuthority('ADMIN')")
    public ResponseEntity<?> deleteById(@RequestParam("id") int userId,
                                        Principal principal,
                                        HttpServletRequest request) {
        userService.deleteById(userId);
        return ResponseEntity.ok(ResponseMessage.builder()
                .message(messageSource.getMessage("message.userDeleted", null, request.getLocale()))
                .isSuccess(true)
                .build());
    }

    @DeleteMapping(value = "/users")
    @PreAuthorize("(hasAuthority('USER') and @userServiceImpl.isMe(#userEmail.email, #principal.name)) or hasAuthority('ADMIN')")
    public ResponseEntity<?> deleteByEmail(@Valid @RequestBody UserEmail userEmail,
                                           Principal principal,
                                           HttpServletRequest request) {
        userService.deleteByEmail(principal.getName());
        return ResponseEntity.ok(ResponseMessage.builder()
                .message(messageSource.getMessage("message.userDeleted", null, request.getLocale()))
                .isSuccess(true)
                .build());
    }

    @PutMapping(value = "/users", params = "id")
    @PreAuthorize("((hasAuthority('USER') or hasAuthority('ADMIN'))) and @userServiceImpl.isMe(#userId, #principal.name)")
    public ResponseEntity<ResponseMessage> updateUser(@RequestBody @Valid MutateUser user,
                                                      @RequestParam("id") int userId,
                                                      Principal principal,
                                                      HttpServletRequest request) {
        userService.updateUser(user, userId);
        return ResponseEntity.ok(ResponseMessage.builder()
                .message(messageSource.getMessage("message.userUpdated", null, request.getLocale()))
                .isSuccess(true)
                .build());
    }
}
