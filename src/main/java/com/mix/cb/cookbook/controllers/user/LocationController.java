package com.mix.cb.cookbook.controllers.user;

import com.mix.cb.cookbook.models.payloads.common.ResponseMessage;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.address.location.LocationDTO;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.address.location.MutateLocation;
import com.mix.cb.cookbook.services.data.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URI;
import java.security.Principal;

@RestController
public class LocationController {

    private final LocationService locationService;
    private MessageSource messageSource;

    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @GetMapping(value = "/users/location", params = "id")
    @PreAuthorize("hasAuthority('USER') and @userServiceImpl.isMe(#userId, #principal.name)")
    public ResponseEntity<LocationDTO> findById(@RequestParam("id") int userId, Principal principal) {
        return ResponseEntity.ok(locationService.findById(userId));
    }

    @GetMapping(value = "/users/location", params = {"lat", "lng"})
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<LocationDTO> findByLatitudeAndLongitude(@RequestParam double lat, @RequestParam double lng) {
        // todo only owner can access it
        return ResponseEntity.ok(locationService.findByLatitudeAndLongitude(lat, lng));
    }

    @DeleteMapping(value = "users/location")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<?> deleteLocation(Principal principal, HttpServletRequest request) {
        locationService.deleteLocation(principal.getName());
        return ResponseEntity.ok(ResponseMessage.builder()
                .message(messageSource.getMessage("message.locationDeleted", null, request.getLocale()))
                .isSuccess(true)
                .build());
    }

    @PostMapping(value = "/users/location")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<LocationDTO> createLocation(@RequestBody @Valid MutateLocation location,
                                                      Principal principal) {
        LocationDTO persistedLocation = locationService.createLocation(location, principal.getName());
        URI locationHeader = ServletUriComponentsBuilder
                .fromCurrentRequestUri()
                .queryParam("id", persistedLocation.getId())
                .buildAndExpand(persistedLocation.getId())
                .toUri();
        return ResponseEntity.created(locationHeader).body(persistedLocation);
    }

    @PutMapping(value = "/users/location")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<LocationDTO> updateLocation(@RequestBody @Valid MutateLocation location,
                                                      Principal principal) {
        return ResponseEntity.ok(locationService.updateLocation(location, principal.getName()));
    }
}
