package com.mix.cb.cookbook.controllers.user;

import com.mix.cb.cookbook.models.payloads.common.ResponseMessage;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.address.AddressDTO;
import com.mix.cb.cookbook.models.payloads.mutate_entities.users.address.MutateAddress;
import com.mix.cb.cookbook.services.data.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URI;
import java.security.Principal;


@RestController
public class AddressController {

    private final AddressService addressService;
    private MessageSource messageSource;

    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @GetMapping(value = "/users/address", params = "id")
    @PreAuthorize("hasAuthority('USER') and @userServiceImpl.isMe(#userId, #principal.name)")
    public ResponseEntity<AddressDTO> getAddress(@RequestParam("id") int userId, Principal principal) {
        return ResponseEntity.ok(addressService.findById(userId));
    }

    @DeleteMapping(value = "/users/address")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<?> deleteByEmail(Principal principal, HttpServletRequest request) {
        addressService.deleteAddress(principal.getName());
        return ResponseEntity.ok(ResponseMessage.builder()
                .message(messageSource.getMessage("message.addressDeleted", null, request.getLocale()))
                .isSuccess(true)
                .build());
    }

    @PostMapping(value = "/users/address")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<AddressDTO> createAddress(@RequestBody @Valid MutateAddress address,
                                                    Principal principal) {
        AddressDTO persistedAddress = addressService.createAddress(address, principal.getName());
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequestUri()
                .queryParam("id", persistedAddress.getId())
                .buildAndExpand(persistedAddress.getId())
                .toUri();
        return ResponseEntity.created(location).body(persistedAddress);
    }

    @PutMapping(value = "/users/address")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<AddressDTO> updateAddress(@RequestBody @Valid MutateAddress address,
                                                    Principal principal) {
        return ResponseEntity.ok(addressService.updateAddress(address, principal.getName()));
    }
}
