package com.mix.cb.cookbook.controllers;

import com.mix.cb.cookbook.models.payloads.files.UploadPdfFileResponse;
import com.mix.cb.cookbook.services.data.RecipeService;
import com.mix.cb.cookbook.services.files.pdf.PDFService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

import static com.mix.cb.cookbook.utils.URLs.composeRecipePdfDownloadUrl;

@Controller
public class PDFController {
    public static final String RECIPE_TEMPLATE = "recipe";
    private final PDFService pdfService;

    private RecipeService recipeService;

    public PDFController(PDFService pdfService) {
        this.pdfService = pdfService;
    }

    @Autowired
    public void setRecipeService(RecipeService recipeService) {
        this.recipeService = recipeService;
    }

    @GetMapping(value = "/recipes/preview", params = "recipe")
    public ModelAndView createPDFRecipe(@RequestParam("recipe") int recipeId,
                                        ModelAndView model) {

        model.addObject("recipe", recipeService.findById(recipeId));
        model.setViewName(RECIPE_TEMPLATE);
        return model;
    }

    @GetMapping(value = "/recipes/pdf", params = "recipe")
    public ResponseEntity<Resource> downloadPDFRecipe(@RequestParam("recipe") int recipeId,
                                                      HttpServletRequest request) {
        UploadPdfFileResponse pdfResponse = pdfService.downloadRecipeAsPdf(recipeId, RECIPE_TEMPLATE, composeRecipePdfDownloadUrl(recipeId, request));
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_OCTET_STREAM)
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + pdfResponse.getFileName() + "\"")
                .body(pdfResponse.getResource());
    }
}
