package com.mix.cb.cookbook.controllers;

import com.mix.cb.cookbook.models.payloads.common.MultipleDeleteRequest;
import com.mix.cb.cookbook.models.payloads.common.ResponseMessage;
import com.mix.cb.cookbook.models.payloads.mutate_entities.steps.MutateStep;
import com.mix.cb.cookbook.models.payloads.mutate_entities.steps.StepDTO;
import com.mix.cb.cookbook.models.payloads.mutate_entities.steps.UpdateStep;
import com.mix.cb.cookbook.services.data.StepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.net.URI;
import java.security.Principal;
import java.util.List;

@Validated
@RestController
public class StepController {
    private final StepService stepService;
    private MessageSource messageSource;

    public StepController(StepService stepService) {
        this.stepService = stepService;
    }

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @GetMapping(value = "/steps", params = "recipe")
    @PreAuthorize("@stepServiceImpl.isRecipeAccessible(#recipeId,#principal)")
    public ResponseEntity<List<StepDTO>> findAllByRecipeId(@RequestParam("recipe") int recipeId,
                                                           Principal principal) {
        return ResponseEntity.ok(stepService.findAllByRecipeId(recipeId));
    }

    @GetMapping(value = "/steps", params = "id")
    @PreAuthorize("@stepServiceImpl.isStepMine(#stepId, #principal)")
    public ResponseEntity<StepDTO> findById(@RequestParam("id") int stepId,
                                            Principal principal) {
        return ResponseEntity.ok(stepService.findById(stepId));
    }

    @DeleteMapping(value = "/steps", params = "id")
    @PreAuthorize("isAuthenticated() and @stepServiceImpl.isStepMine(#stepId, #principal)")
    public ResponseEntity<?> deleteById(@RequestParam("id") int stepId,
                                        HttpServletRequest request,
                                        Principal principal) {
        stepService.deleteById(stepId, principal.getName());
        String message = messageSource.getMessage("message.stepDeleted", null, request.getLocale());
        return ResponseEntity.ok(ResponseMessage.builder()
                .message(message)
                .isSuccess(true)
                .build());
    }


    @PostMapping(value = "/steps", params = "recipe")
    @PreAuthorize("isAuthenticated() and @stepServiceImpl.isRecipeAccessible(#recipeId, #principal)")
    public ResponseEntity<StepDTO> createStep(@RequestParam("recipe") int recipeId,
                                              @RequestBody @Valid MutateStep step,
                                              Principal principal) {
        StepDTO persistedStep = stepService.createStep(recipeId, step);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequestUri()
                .queryParam("id", persistedStep.getId())
                .buildAndExpand(persistedStep.getId())
                .toUri();
        return ResponseEntity.created(location).body(persistedStep);
    }

    @PutMapping(value = "/steps/all", params = "recipe")
    @PreAuthorize("isAuthenticated() and @stepServiceImpl.areStepsMine(#recipeId, #steps, #principal)")
    public ResponseEntity<ResponseMessage> updateSteps(@RequestParam("recipe") int recipeId,
                                                       @Valid @RequestBody @NotEmpty List<@Valid UpdateStep> steps,
                                                       Principal principal,
                                                       HttpServletRequest request) {
        stepService.updateSteps(recipeId, steps);
        return ResponseEntity.ok(ResponseMessage.builder()
                .message(messageSource.getMessage("message.stepsUpdated", null, request.getLocale()))
                .isSuccess(true)
                .build());
    }

    @PutMapping(value = "/steps", params = "id")
    @PreAuthorize("isAuthenticated() and @stepServiceImpl.isStepMine(#stepId, #principal)")
    public ResponseEntity<StepDTO> updateStep(@RequestParam("id") int stepId,
                                              @RequestBody @Valid MutateStep step,
                                              Principal principal) {
        return ResponseEntity.ok(stepService.updateStep(stepId, step));
    }

    @PostMapping(value = "/steps/all", params = "recipe")
    @PreAuthorize("isAuthenticated() and @stepServiceImpl.isRecipeMine(#recipeId, #principal)")
    public ResponseEntity<ResponseMessage> saveSteps(@RequestParam("recipe") int recipeId,
                                                     @Valid @RequestBody @NotEmpty List<@Valid MutateStep> saveRequest,
                                                     Principal principal,
                                                     HttpServletRequest request) {
        stepService.saveRecipeSteps(recipeId, saveRequest);
        return ResponseEntity.ok(ResponseMessage.builder()
                .message(messageSource.getMessage("message.stepsSaved", null, request.getLocale()))
                .isSuccess(true)
                .build());
    }

    @DeleteMapping(value = "/steps/all", params = "recipe")
    @PreAuthorize("isAuthenticated() and @stepServiceImpl.areStepsMine(#recipeId, #deleteRequest.indices, #principal)")
    public ResponseEntity<ResponseMessage> deleteSteps(@RequestParam("recipe") int recipeId,
                                                       @Valid @RequestBody MultipleDeleteRequest deleteRequest,
                                                       Principal principal,
                                                       HttpServletRequest request) {
        stepService.deleteRecipeSteps(deleteRequest);
        return ResponseEntity.ok(ResponseMessage.builder()
                .message(messageSource.getMessage("message.stepsDeleted", null, request.getLocale()))
                .isSuccess(true)
                .build());

    }
}
