package com.mix.cb.cookbook.repos;

import com.mix.cb.cookbook.models.entities.Ingredient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface IngredientRepo extends JpaRepository<Ingredient, Integer> {
    Collection<Ingredient> findAllByRecipeId(int id);
    boolean existsByRecipeIdAndName(int recipeId, String name);

    @Modifying
    @Query(value="delete from ingredient where id in :indices", nativeQuery=true)
    void deleteAllWithIds(@Param("indices") int[] indices);
}
