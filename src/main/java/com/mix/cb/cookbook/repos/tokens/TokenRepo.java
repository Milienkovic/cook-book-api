package com.mix.cb.cookbook.repos.tokens;

import com.mix.cb.cookbook.models.entities.User;
import com.mix.cb.cookbook.models.entities.tokens.Token;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Date;
import java.util.Optional;

@NoRepositoryBean
public interface TokenRepo<T extends Token> extends JpaRepository<T,Integer> {
    Optional<T> findByToken(String token);
    Optional<T> findByUser(User user);
    void deleteByUser(User user);
    void deleteAllByExpiryDateLessThan(Date now);
    boolean existsByUserEmail(String email);
    boolean existsByToken(String token);
}
