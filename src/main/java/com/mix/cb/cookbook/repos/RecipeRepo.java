package com.mix.cb.cookbook.repos;

import com.mix.cb.cookbook.models.entities.Recipe;
import com.mix.cb.cookbook.models.enums.Complexity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface RecipeRepo extends JpaRepository<Recipe, Integer> {
    Collection<Recipe> findAllByCategoryId(int id);

    Collection<Recipe> findAllByCategoryIdAndComplexity(int categoryId, Complexity complexity);

    Collection<Recipe> findAllByName(String recipeName);
    void deleteAllByUserId(int userId);

    @Modifying(clearAutomatically = true,flushAutomatically = true)
    @Query(value = "delete from favorite where favorite.recipe_id = :recipeId", nativeQuery = true)
    void clearRecipeFromFavorites(@Param("recipeId") int recipeId);

}
