package com.mix.cb.cookbook.repos.tokens;

import com.mix.cb.cookbook.models.entities.tokens.VerificationToken;

import java.util.Optional;

public interface VerificationTokenRepo extends TokenRepo<VerificationToken> {
    Optional<VerificationToken> findByUserEmail(String email);
}
