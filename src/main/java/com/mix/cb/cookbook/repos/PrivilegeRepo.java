package com.mix.cb.cookbook.repos;

import com.mix.cb.cookbook.models.entities.Privilege;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface PrivilegeRepo extends JpaRepository<Privilege, Integer> {
    Optional<Privilege> findByPrivilege(String privilege);

    @Query(nativeQuery = true, value = "select * from privilege p inner join role_privilege rp on p.id = rp.privilege_id inner join role r on rp.role_id = r.id where r.role= :roleName")
    Collection<Privilege> findAllByRoleName(@Param("roleName") String roleName);
}
