package com.mix.cb.cookbook.repos;

import com.mix.cb.cookbook.models.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepo extends JpaRepository<User, Integer> {
    Optional<User> findByEmail(String email);

    void deleteByEmail(String email);

    boolean existsByEmail(String email);

    @Modifying(flushAutomatically = true, clearAutomatically = true)
    @Query(value = "delete from favorite where favorite.user_id = :userId", nativeQuery = true)
    void removeUserFromFavorites(@Param("userId") int userId);

    @Modifying(flushAutomatically = true, clearAutomatically = true)
    @Query(value = "delete from `comment` where comment.user_id = :userId", nativeQuery = true)
    void removeUserFromFComments(@Param("userId") int userId);
}
