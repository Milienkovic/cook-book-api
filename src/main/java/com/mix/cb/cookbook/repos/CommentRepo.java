package com.mix.cb.cookbook.repos;

import com.mix.cb.cookbook.models.entities.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

public interface CommentRepo extends JpaRepository<Comment, Integer> {
    Collection<Comment> findAllByReportedAndUserId(boolean reported, int userId);

    Collection<Comment> findAllByReportedAndCommentId(boolean reported, int commentId);

    Collection<Comment> findAllByReportedAndRecipeId(boolean reported, int recipeId);

    Collection<Comment> findAllByReported(boolean reported);

    void deleteAllByReported(boolean reported);
}
