package com.mix.cb.cookbook.repos.tokens;

import com.mix.cb.cookbook.models.entities.User;
import com.mix.cb.cookbook.models.entities.tokens.JwtResetToken;

public interface JwtResetTokenRepo extends TokenRepo<JwtResetToken> {
    boolean existsByUser(User user);

}
