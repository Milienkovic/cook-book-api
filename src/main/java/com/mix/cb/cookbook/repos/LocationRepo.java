package com.mix.cb.cookbook.repos;

import com.mix.cb.cookbook.models.entities.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LocationRepo extends JpaRepository<Location,Integer> {
    Optional<Location> findByLatAndLng(double lat, double lng);
}
