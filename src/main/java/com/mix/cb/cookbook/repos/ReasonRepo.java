package com.mix.cb.cookbook.repos;

import com.mix.cb.cookbook.models.entities.Reason;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReasonRepo extends JpaRepository<Reason,Integer> {
}
