package com.mix.cb.cookbook.repos;

import com.mix.cb.cookbook.models.entities.Ban;
import com.mix.cb.cookbook.models.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;

public interface BanRepo extends JpaRepository<Ban,Integer> {
    Optional<Ban> findByUser(User user);
    Collection<Ban> findAllByBannedUntilBefore(Date now);
    Optional<Ban> findByUserEmail(String email);
}
