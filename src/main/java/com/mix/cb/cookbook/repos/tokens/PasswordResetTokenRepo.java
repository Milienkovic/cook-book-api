package com.mix.cb.cookbook.repos.tokens;

import com.mix.cb.cookbook.models.entities.User;
import com.mix.cb.cookbook.models.entities.tokens.PasswordResetToken;
import org.springframework.stereotype.Repository;

@Repository
public interface PasswordResetTokenRepo extends TokenRepo<PasswordResetToken> {
    boolean existsByUser(User user);

}
