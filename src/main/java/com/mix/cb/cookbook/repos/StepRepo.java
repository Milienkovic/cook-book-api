package com.mix.cb.cookbook.repos;

import com.mix.cb.cookbook.models.entities.Step;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface StepRepo extends JpaRepository<Step,Integer> {
    Collection<Step> findAllByRecipeId(int recipeId);
    void deleteByRecipeIdAndStepNo(int recipeIdm, String stepNumber);
    boolean existsByRecipeIdAndStepNo(int recipeId, String stepNumber);
    Optional<Step> findById(int id);
    Optional<Step> findByRecipeIdAndStepNo(int recipeId, String stepNumber);

    @Modifying
    @Query(value="delete from step where id in :indices", nativeQuery=true)
    void deleteAllWithIds(@Param("indices") int[] indices);
}
