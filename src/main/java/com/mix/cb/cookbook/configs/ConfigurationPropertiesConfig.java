package com.mix.cb.cookbook.configs;

import com.mix.cb.cookbook.services.files.FilesProps;
import com.mix.cb.cookbook.services.security.jwt.JwtTokenProps;
import com.mix.cb.cookbook.utils.UrlProps;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties({JwtTokenProps.class, FilesProps.class, UrlProps.class})
public class ConfigurationPropertiesConfig {
}
