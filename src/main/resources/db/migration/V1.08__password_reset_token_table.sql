CREATE TABLE `PasswordResetToken` (
                                      `id` INT NOT NULL AUTO_INCREMENT,
                                      `token` varchar(255) NOT NULL,
                                      `expiry_date` TIMESTAMP,
                                      `user_id` INT NOT NULL,
                                      PRIMARY KEY (`id`)
);

ALTER TABLE `PasswordResetToken` ADD CONSTRAINT `PasswordResetToken_fk0` FOREIGN KEY (`user_id`) REFERENCES `User`(`id`);

