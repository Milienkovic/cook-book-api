CREATE TABLE `jwt_reset_token` (
                                      `id` INT NOT NULL AUTO_INCREMENT,
                                      `token` varchar(255) NOT NULL,
                                      `expiry_date` TIMESTAMP,
                                      `user_id` INT NOT NULL,
                                      PRIMARY KEY (`id`)
);

ALTER TABLE `jwt_reset_token` ADD CONSTRAINT `jwt_reset_token_fk0` FOREIGN KEY (`user_id`) REFERENCES `User`(`id`);