Alter table `Category` drop foreign key `Category_fk0`;
alter table `Category` drop COLUMN `user_id`;
alter table `Category` add `type` varchar(50) null;


alter table `Recipe` add `user_id` int not null;
alter table `Recipe` add `visible` boolean;
alter table `Recipe` add constraint `Recipe_fk1` foreign key (`user_id`) references `User`(`id`);

