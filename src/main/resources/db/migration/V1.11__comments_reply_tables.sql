
CREATE TABLE `Comment` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`text` TEXT NOT NULL,
	`likes` INT DEFAULT 0,
	`dislikes` INT DEFAULT 0,
	`last_modified` TIMESTAMP NULL,
	`comment_id` INT NULL DEFAULT null,
	`created_at` TIMESTAMP NULL,
	`reported` BOOLEAN,
	`user_id` INT NOT NULL,
	`recipe_id` INT NOT NULL,
	PRIMARY KEY (`id`)
);


ALTER TABLE `Comment` ADD CONSTRAINT `Comment_fk0` FOREIGN KEY (`user_id`) REFERENCES `User`(`id`);
ALTER TABLE `Comment` ADD CONSTRAINT `Comment_fk1` FOREIGN KEY (`recipe_id`) REFERENCES `Recipe`(`id`);
ALTER TABLE `Comment` ADD CONSTRAINT `Comment_fk2` FOREIGN KEY (`comment_id`) REFERENCES `Comment`(`id`);