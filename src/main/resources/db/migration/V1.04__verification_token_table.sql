CREATE TABLE `VerificationToken` (
                                     `id` INT NOT NULL AUTO_INCREMENT,
                                     `token` varchar(255) NOT NULL,
                                     `expiry_date` DATE NOT NULL,
                                     `user_id` INT NOT NULL,
                                     PRIMARY KEY (`id`)
);

ALTER TABLE `VerificationToken` ADD CONSTRAINT `VerificationToken_fk0` FOREIGN KEY (`user_id`) REFERENCES `User`(`id`);

