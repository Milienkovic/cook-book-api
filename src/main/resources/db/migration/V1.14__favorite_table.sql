CREATE TABLE `Favorite` (
                            `user_id` INT NOT NULL,
                            `recipe_id` INT NOT NULL
);

ALTER TABLE `Favorite` ADD CONSTRAINT `Favorite_fk0` FOREIGN KEY (`user_id`) REFERENCES `User`(`id`);

ALTER TABLE `Favorite` ADD CONSTRAINT `Favorite_fk1` FOREIGN KEY (`recipe_id`) REFERENCES `Recipe`(`id`);