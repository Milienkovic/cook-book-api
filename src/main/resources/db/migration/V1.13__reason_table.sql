CREATE TABLE `Reason` (
                          `id` INT NOT NULL AUTO_INCREMENT,
                          `reason` TEXT NOT NULL,
                          `created_at` TIMESTAMP,
                          `ban_time_in_minutes` INT NOT NULL,
                          `ban_id` INT NOT NULL,
                          PRIMARY KEY (`id`)
);

ALTER  TABLE `Ban` DROP COLUMN `reason`;

ALTER TABLE `Reason` ADD CONSTRAINT `Reason_fk0` FOREIGN KEY (`ban_id`) REFERENCES `Ban`(`user_id`);
