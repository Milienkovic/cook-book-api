CREATE TABLE `Ban`(
    `reason`        TEXT         NULL,
    `nob`           INT          NOT NULL DEFAULT 0,
    `perma`         BOOLEAN,
    `banned_until` TIMESTAMP     NULL,
    `created_at`    TIMESTAMP    NULL,
    `last_modified` TIMESTAMP    NULL,
    `ip`            varchar(255) NULL,
    `user_id`       INT          NOT NULL,
    PRIMARY KEY (`user_id`)
);



ALTER TABLE `Ban` ADD CONSTRAINT `Ban_fk0` FOREIGN KEY (`user_id`) REFERENCES `User` (`id`);