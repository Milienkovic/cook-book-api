CREATE TABLE `role_privilege`(
    `role_id` int not null,
    `privilege_id` int not null
);

ALTER TABLE `role_privilege` ADD CONSTRAINT `role_privilege_fk0` FOREIGN KEY (`role_id`) REFERENCES `Role`(`id`);
ALTER TABLE `role_privilege` ADD CONSTRAINT `role_privilege_fk1` FOREIGN KEY (`privilege_id`) REFERENCES `Privilege`(`id`);