CREATE TABLE `Category` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NOT NULL,
	`image_url` VARCHAR(255),
	`created_at` DATE,
	`user_id` INT NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Recipe` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NOT NULL,
	`image_url` VARCHAR(255),
	`created_at` DATE,
	`last_modified` DATE,
	`complexity` INT,
	`rating` FLOAT,
	`category_id` INT NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Ingredient` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NOT NULL,
	`uom` VARCHAR(25),
	`amount` INT,
	`image_url` VARCHAR(255),
	`recipe_id` INT NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Role` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`role` VARCHAR(25) NOT NULL UNIQUE,
	PRIMARY KEY (`id`)
);

CREATE TABLE `User` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`first_name` VARCHAR(50) NOT NULL,
	`last_name` VARCHAR(50),
	`email` VARCHAR(100) NOT NULL UNIQUE,
	`password` VARCHAR(100) NOT NULL,
	`phone` VARCHAR(100),
    `last_visited` TIMESTAMP null,
	`created_at` DATE,
	`validated_at` DATE,
	`role_id` INT NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Address` (
	`street` VARCHAR(100),
	`city` VARCHAR(100),
	`country` VARCHAR(50),
	`user_id` INT NOT NULL,
	PRIMARY KEY (`user_id`)
);

CREATE TABLE `Location` (
	`lat` FLOAT,
	`lng` FLOAT,
	`address_id` INT NOT NULL,
	PRIMARY KEY (`address_id`)
);

CREATE TABLE `Step` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`step_no` VARCHAR(50),
	`description` VARCHAR(255),
	`created_at` DATE,
	`last_modified` DATE,
	`mandatory` BOOLEAN,
	`recipe_id` INT NOT NULL,
	PRIMARY KEY (`id`)
);

ALTER TABLE `Category` ADD CONSTRAINT `Category_fk0` FOREIGN KEY (`user_id`) REFERENCES `User`(`id`);

ALTER TABLE `Recipe` ADD CONSTRAINT `Recipe_fk0` FOREIGN KEY (`category_id`) REFERENCES `Category`(`id`);

ALTER TABLE `Ingredient` ADD CONSTRAINT `Ingredient_fk0` FOREIGN KEY (`recipe_id`) REFERENCES `Recipe`(`id`);

ALTER TABLE `User` ADD CONSTRAINT `User_fk0` FOREIGN KEY (`role_id`) REFERENCES `Role`(`id`);

ALTER TABLE `Address` ADD CONSTRAINT `Address_fk0` FOREIGN KEY (`user_id`) REFERENCES `User`(`id`);

ALTER TABLE `Location` ADD CONSTRAINT `Location_fk0` FOREIGN KEY (`address_id`) REFERENCES `Address`(`user_id`);

ALTER TABLE `Step` ADD CONSTRAINT `Step_fk0` FOREIGN KEY (`recipe_id`) REFERENCES `Recipe`(`id`);

